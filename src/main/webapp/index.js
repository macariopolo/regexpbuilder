function ViewModel() {
	var self = this;
	self.projectName = ko.observable("");

	self.createProject = function() {
		window.location.href = "createProject.html";
	}
	
	self.openProject = function() {
		sessionStorage.projectName = self.projectName();
		window.location.href = "project.html";
	}
	
	self.regularExpressionGenerator = function() {
		window.location.href = "regexp.html";
	}
}

var vm = new ViewModel();
ko.applyBindings(vm);