function ViewModel() {
	var self = this;
	self.projectName = ko.observable("");

	self.uploadFile = function() {
		$.ajax({
			url: 'uploadFile?projectName=' + self.projectName(),
			type: 'POST',
			data: new FormData($('form')[0]),
			cache: false,
			contentType: false,
			processData: false,
			success : function() {
				sessionStorage.projectName = self.projectName();
				window.location.href="project.html"
			},			
			xhr: function () {
				var myXhr = $.ajaxSettings.xhr();
				if (myXhr.upload) {
					myXhr.upload.addEventListener('#progress', function (e) {
						if (e.lengthComputable) {
							$('#progress').attr({
								value: e.loaded,
								max: e.total,
							});
						}
					}, false);
				}
				return myXhr;
			}				
		});
	}
}

var vm = new ViewModel();
ko.applyBindings(vm);