function ViewModel() {
	var ws;
	var self = this;
	self.projectName = ko.observable(sessionStorage.projectName);
	self.classes = ko.observableArray([]);
	self.command = ko.observable("/Users/macario.polo/bacterioWebFiles/apache-maven-3.5.0/bin/mvn -Dtest=edu.uclm.esi.facil.AppTest test");
	self.lines = ko.observableArray([]);
	self.instrumentAttributes = ko.observable(true);
	self.instrumentMethods = ko.observable(true);
	self.instrumentationKind = ko.observable("inserta")
	
	function loadWebSocket() {
		ws = new WebSocket("ws://" + window.location.host + "/wsServerProjects");
		ws.onmessage = function(e) {
			var data = JSON.parse(e.data);
			var type = data.type;
			if (type == "project") {
				var classes = data.project;
				var clazz;
				for (var i=0; i<classes.length; i++) {
					clazz = new Clazz(classes[i]);
					self.classes.push(clazz);
				}
			} else if (type == "line") {
				self.lines.push(data.line);
			}
		}
		
		ws.onopen = function(event) {
			var msg = {
				type : "openProject",
				projectName : self.projectName()
			};
			ws.send(JSON.stringify(msg));
		}
		ws.onerror = function(event) {
			alert("Error");
		}
	}
	
	self.instrument = function() {
		var classes = ko.toJSON(self.classes);
		classes = JSON.parse(classes);
		var msg = {
			type : "instrument",
			instrumentAttributes : self.instrumentAttributes(),
			instrumentMethods : self.instrumentMethods(),
			classes : classes,
			projectName : self.projectName(),
			instrumentationKind : self.instrumentationKind()
		};
		ws.send(JSON.stringify(msg));
	}
	
	self.executeOriginal = function() {
		$("#console").html();
		var msg = {
			type : "executeOriginal",
			command : self.command(),
			projectName : self.projectName()
		};
		ws.send(JSON.stringify(msg));
	}
	
	self.executeInstrumented = function() {
		$("#console").html();
		var msg = {
			type : "executeInstrumented",
			command : self.command(),
			projectName : self.projectName()
		};
		ws.send(JSON.stringify(msg));
	}
	
	loadWebSocket();
}

class Clazz {
	constructor(clazz) {
		this.name = ko.observable(clazz.name);
		this.checked = ko.observable(false);
		
		this.constructors = ko.observableArray([]);
		//for (var i=0; i<clazz.constructors.length; i++) 
		//	this.constructors.push(new Constructor(clazz.constructors[i]));
		
		this.methods = ko.observableArray([]);
		for (var i=0; i<clazz.methods.length; i++)
			this.methods.push(new Method(clazz.methods[i]));
		
		this.attributes = ko.observableArray([]);
		for (var i=0; i<clazz.attributes.length; i++)
			this.attributes.push(new Attribute(clazz.attributes[i]));
	}
	
	select() {
		for (var i=0; i<this.constructors().length; i++) {
			this.constructors()[i].checked(this.checked());
		}
		for (var i=0; i<this.methods().length; i++) {
			this.methods()[i].checked(this.checked());
		}
		for (var i=0; i<this.attributes().length; i++) {
			this.attributes()[i].checked(this.checked());
		}
	}
}

class Constructor {
	constructor(constructor) {
		this.visibility = ko.observable(constructor.visibility);
		this.name = ko.observable(constructor.name);
		this.checked = ko.observable(false);
	}
}

class Method {
	constructor(method) {
		this.visibility = ko.observable(method.visibility);
		this.name = ko.observable(method.name);
		this.checked = ko.observable(false);
	}
}

class Attribute {
	constructor(attribute) {
		this.visibility = ko.observable(attribute.visibility);
		this.name = ko.observable(attribute.name);
		this.checked = ko.observable(false);
	}
}

var vm = new ViewModel();
ko.applyBindings(vm);