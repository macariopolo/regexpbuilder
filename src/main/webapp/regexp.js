function ViewModel() {
	var self = this;
	self.ws = null;
	self.disconnected = ko.observable(true);

	self.sequences = ko.observable("");
	self.frequencies = ko.observableArray([]);
	self.separated = ko.observable(false);
	self.regularExpression = ko.observable("");
	self.subprogress = ko.observable("");
	self.mainThread = ko.observable("");
	self.numberOfThreads = ko.observable(8);
	self.times = ko.observableArray([]);
	
	self.numberOfFinalStates = ko.observable("one");
	self.order = ko.observable("shortest");
	self.algorithm = ko.observable("equations");
	self.minimize = ko.observable(false);
	
	self.cargarEjemploSinEspacios = function() {
		self.separated(false);
		var jsa = example();
		var r = "";
		for (var i=0; i<jsa.length; i++)
			r = r + jsa[i] + "\n";
		this.sequences(r);
	}
	
	self.setNumberOfThreads = function() {
		var msg = {
			type : "setNumberOfThreads",
			numberOfThreads : self.numberOfThreads()
		};
		self.ws.send(JSON.stringify(msg));
	}
	
	self.cargarEjemploConEspacios = function() {
		self.separated(true);
		var jsa = example();
		var r = "";
		for (var i=0; i<jsa.length; i++) {
			var line = jsa[i];
			for (var j=0; j<line.length; j++) {
				r = r + line[j] + " ";
			}
			r = r+ "\n";
		}
		this.sequences(r);
	}
	
	self.getRegularExpression = function() {
		self.mainThread("Uploading sequences...");
		var msg = {
			type : "getRegularExpression",
			separated : self.separated(),
			sequences : self.sequences(),
			minimize : self.minimize(),
			numberOfFinalStates : self.numberOfFinalStates(),
			numberOfThreads : self.numberOfThreads(),
			order : self.order(),
			algorithm : self.algorithm()
		};
		self.ws.send(JSON.stringify(msg));
	}
	
	self.getEquations = function() {
		var msg = {
			type : "getEquations",
			separated : self.separated(),
			sequences : self.sequences()
		};
		self.ws.send(JSON.stringify(msg));
	}
	
	self.sortByStart = function() {
		var msg = {
			type : "sortByStart",
			separated : self.separated(),
			sequences : self.sequences()
		};
		self.ws.send(JSON.stringify(msg));
	}
	
	self.sortByShortest = function() {
		sortByLength(true);
	}
	
	self.sortByLongest = function() {
		sortByLength(false);
	}
	
	function sortByLength(shortestFirst) {
		var msg = {
			type : "sortByLength",
			shortestFirst : shortestFirst,
			separated : self.separated(),
			sequences : self.sequences()
		};
		self.ws.send(JSON.stringify(msg));
	}
	
	self.sortByEnd = function() {
		var msg = {
			type : "sortByEnd",
			separated : self.separated(),
			sequences : self.sequences()
		};
		self.ws.send(JSON.stringify(msg));
	}
	
	self.getFrequencies = function() {
		var msg = {
			type : "getFrequencies",
			separated : self.separated(),
			sequences : self.sequences()
		};
		self.ws.send(JSON.stringify(msg));
	}
	
	onmessage = function(event) {
		var jso = event.data;
		jso = JSON.parse(jso);
		if (jso.type == "time") {
			var found = false;
			for (var i=0; i<self.times().length; i++) {
				if (self.times()[i].startsWith(jso.task)) {
					self.times.splice(i, 1);
					if (jso.time!=0)
						self.times.push(jso.task + ": " + (jso.time/1000) + " seconds");
					else
						self.times.push(jso.task + ": --- <i>(executing)</i>");
					found = true;
					break;
				}
			}
			if (!found) {
				self.times.push(jso.task + ": " + (jso.time/1000) + " seconds");
			}
		} else if (jso.type == "threads") {
			self.numberOfThreads(jso.threads);
		} else if (jso.type == "subprogress") {
			self.subprogress("Thread " + jso.threadId + "-> " + jso.message);
		} else if (jso.type == "progress") {
			self.mainThread(jso.message);
		} else if (jso.type=="automatas") {
			var originalAutomata = jso.originalAutomata;
			var minimizedAutomata = jso.minimizedAutomata;
			printTables(originalAutomata.pairTables);
			sessionStorage.originalAutomata = JSON.stringify(originalAutomata);
			sessionStorage.minimizedAutomata = JSON.stringify(minimizedAutomata);
			window.open("automatas.html", "automata");
		} else if (jso.type=="automata") {
			var automata = jso.automata;
			sessionStorage.automata = JSON.stringify(automata);
			sessionStorage.title = jso.title;
			var w = window.open("automata.html", "_blank");
		} else if (jso.type == "sequences") {
			var r ="";
			for (var i=0; i<jso.sequences.length; i++)
				r = r + jso.sequences[i] + "\n";
			self.sequences(r);
		} else if (jso.type == "frequencies") {
			var r ="";
			for (var i=0; i<jso.sortedSequences.length; i++)
				r = r + jso.sortedSequences[i] + "\n";
			self.sequences(r);
			self.frequencies(jso.frequencies.frequencies);				
		} else if (jso.type == "regularExpression") {
			self.regularExpression(jso.regularExpression);
			//drawDFA(originalAutomata);
		} else if (jso.type == "equations") {
			self.regularExpression(jso.equations);
		}
	}
	
	function printTables(tables) {
		var area = document.getElementById("pairTables");
		area.innerHTML="";
		for (var i=0; i<tables.length; i++) {
			var pt = tables[i];
			var currentRow, currentCell;
			if (i%9 == 0) {
				currentRow = document.createElement("tr"); area.appendChild(currentRow);
				currentCell = document.createElement("td"); currentRow.appendChild(currentCell);
			} else {
				currentCell = document.createElement("td"); currentRow.appendChild(currentCell);
			}
			
			var table = document.createElement("table"); table.setAttribute("border", "1"); currentCell.appendChild(table);
			var tr = document.createElement("tr"); table.appendChild(tr);
			var td = document.createElement("th"); tr.appendChild(td);
			td.setAttribute("colspan", 5); td.innerHTML = pt.state;
			tr = document.createElement("tr"); table.appendChild(tr);
			td = document.createElement("th"); tr.appendChild(td); td.innerHTML = "From";
			td = document.createElement("th"); tr.appendChild(td); td.innerHTML = "With";
			td = document.createElement("th"); tr.appendChild(td); td.innerHTML = "To";
			td = document.createElement("th"); tr.appendChild(td); td.innerHTML = "With";
			td = document.createElement("th"); tr.appendChild(td); td.innerHTML = "Visits";
			var pairs = pt.pairs;
			for (var j=0; j<pairs.length; j++) {
				var pair = pairs[j];
				tr = document.createElement("tr"); table.appendChild(tr);
				td = document.createElement("td"); tr.appendChild(td); td.innerHTML = pair.inputState;
				td = document.createElement("td"); tr.appendChild(td); td.innerHTML = pair.inputSymbol;
				td = document.createElement("td"); tr.appendChild(td); td.innerHTML = pair.outputState;
				td = document.createElement("td"); tr.appendChild(td); td.innerHTML = pair.outputSymbol;
				td = document.createElement("td"); tr.appendChild(td); td.innerHTML = pair.visits;
			}
		}
	}
	
	function example() {
		var jsa = [];
		jsa.push("BECEFBECEF");
		jsa.push("BECEF");
		jsa.push("BECEFBECEFBECEF");
		jsa.push("BECEFBECEFBEF");
		jsa.push("BECEFBECEFBEFBEF");
		jsa.push("BECEFBECETEFEF");
		jsa.push("BECEFBEF");
		jsa.push("BECEFBEFBECEF");
		jsa.push("BECEFBEFBECEFBEF");
		jsa.push("BECEFBEFBEF");
		jsa.push("BECEFBEFBEFBECEF");
		jsa.push("BECEFBEFBEFBEF");
		jsa.push("BECETEFEF");
		jsa.push("BECETEFEFBECEF");
		jsa.push("BECETEFEFBEF");
		jsa.push("BECETEFEFBEFBEF");
		jsa.push("BEF");
		jsa.push("BEFBECEF");
		jsa.push("BEFBECEFBECEF");
		jsa.push("BEFBECEFBECEFBEF");
		jsa.push("BEFBECEFBEF");
		jsa.push("BEFBECEFBEFBECEF");
		jsa.push("BEFBECEFBEFBEF");
		jsa.push("BEFBECETEFEF");
		jsa.push("BEFBECETEFEFBEF");
		jsa.push("BEFBEF");
		jsa.push("BEFBEFBECEF");
		jsa.push("BEFBEFBECEFBECEF");
		jsa.push("BEFBEFBECEFBEF");
		jsa.push("BEFBEFBECETEFEF");
		jsa.push("BEFBEFBEF");
		jsa.push("BEFBEFBEFBECEF");
		jsa.push("BEFBEFBEFBEF");
		jsa.push("BEFBEFBEFBEFBEF");
		return jsa;
	}
	
	self.connectToServer = function() {
		self.ws = new WebSocket("ws://" + window.location.host + "/wsServerRegExp");
		self.ws.onmessage = onmessage;
		self.ws.onopen = function(event) {
			self.disconnected(false);
			var msg = {
				type : "getThreads"
			};
			self.ws.send(JSON.stringify(msg));
		}
		self.ws.onerror = function(event) {
			alert("Error");
		}
		self.ws.onclose = function() {
			self.disconnected(true);
		}
	}
	
	self.connectToServer();
}

var vm = new ViewModel();
ko.applyBindings(vm);