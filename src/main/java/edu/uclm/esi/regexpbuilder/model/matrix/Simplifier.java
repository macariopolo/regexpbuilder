package edu.uclm.esi.regexpbuilder.model.matrix;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class Simplifier {

	public String simplify(String sre) {
		ArrayList<Pattern> patterns = loadPatterns(sre);
		return sre;
	}

	private ArrayList<Pattern> loadPatterns(String sre) {
		HashMap<String, Pattern> patternsMap = new HashMap<>();
		int length = sre.length();
		int tokenLength = 3;
		
		int i = 0, start, end;
		String token;
		while (i<length-tokenLength+1) {
			start = i;
			end = i+3;
			token = sre.substring(start, end);
			if (!((token.indexOf('(')!=-1 && token.indexOf(')')==-1) || (token.indexOf('(')==-1 && token.indexOf(')')!=-1)))
				put(patternsMap, token);
			i++;
		}
		Collection<Pattern> cPatterns = patternsMap.values();
		ArrayList<Pattern> patterns = new ArrayList<>();
		for (Pattern pattern : cPatterns)
			patterns.add(pattern);
		Collections.sort(patterns, new Comparator<Pattern>() {
			@Override
			public int compare(Pattern p1, Pattern p2) {
				return p2.getN()-p1.getN();
			}			
		});
		return patterns;
	}

	private void put(HashMap<String, Pattern> patternsMap, String token) {
		Pattern pattern = patternsMap.get(token);
		if (pattern==null) {
			pattern = new Pattern(token);
			patternsMap.put(token, pattern);
		}
		pattern.inc();
	}

}
