package edu.uclm.esi.regexpbuilder.model.matrix;

import edu.uclm.esi.regexpbuilder.model.Automata;
import edu.uclm.esi.regexpbuilder.model.Reloj;
import edu.uclm.esi.regexpbuilder.model.SessionManager;
import edu.uclm.esi.regexpbuilder.model.Solver;

public abstract class MatrixSolver extends Solver {
	protected Matrix matrix;

	public MatrixSolver(Automata automata, SessionManager sessionManager, Reloj reloj) {
		super(automata, sessionManager, reloj);
	}
}
