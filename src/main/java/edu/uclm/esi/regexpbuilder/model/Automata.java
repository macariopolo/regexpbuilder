package edu.uclm.esi.regexpbuilder.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import edu.uclm.esi.regexpbuilder.model.equations.Equation;
import edu.uclm.esi.regexpbuilder.model.equations.EquationSystem;
import edu.uclm.esi.regexpbuilder.model.matrix.TransitiveClosureMatrixSolver;
import edu.uclm.esi.regexpbuilder.model.minimizer.AutomataMinimizer;
import edu.uclm.esi.regexpbuilder.model.re.Concat;
import edu.uclm.esi.regexpbuilder.model.re.ILabel;
import edu.uclm.esi.regexpbuilder.model.re.RightMember;
import edu.uclm.esi.regexpbuilder.model.re.State;
import edu.uclm.esi.regexpbuilder.model.re.Symbol;
import edu.uclm.esi.regexpbuilder.model.statemachines.IOPairTable;
import edu.uclm.esi.regexpbuilder.model.statemachines.TransitionsPairsSolver;

public abstract class Automata implements IThreadManager {
	protected State initial;
	protected Map<Integer, State> states;
	protected Map<String, Sequence> finalSequences;
	protected int counter;
	protected boolean separated;
	private EquationSystem equationSystem;
	protected Reloj reloj;
	protected int currentIndex;
	protected SessionManager sessionManager;
	
	private Automata(SessionManager sessionManager, Reloj reloj) {
		this.sessionManager = sessionManager;
		this.states = new ConcurrentHashMap<>();
		this.initial = newState();
		this.initial.setInitial(true);
		this.reloj = reloj;
	}
	
	public Automata(boolean separated, SessionManager sessionManager, Reloj reloj) {
		this(sessionManager, reloj);
		this.finalSequences = new HashMap<>();
		this.separated = separated;
	}
	
	public void add(int threadId, Sequence sequence, int index, int end) {
		Symbol symbol;
		Sequence subsequence;
		State[] currentState = { this.initial };
		State nextState = null;
		List<Symbol> symbols = sequence.getSymbols();
		int size = symbols.size();
		for (int i=0; i<size; i++) {
			symbol = symbols.get(i);
			subsequence = this.finalSequences.get(sequence.getSubsequence(i).toString());
			sessionManager.sendSubprogress(threadId, "Adding to automaton: sequence " + index + "/" + end + ", " + (i+1) + " of " + size + " symbols", 0.1);
			if (subsequence!=null && !currentState[0].hasOutputTransitionWith(symbol)) {
				State secondState = subsequence.getSecondState();
				State finalState = subsequence.getFinalState();
				currentState[0].addOutputTransition(symbol, secondState);
				this.addAllSubsequences(sequence, finalState);
				nextState=null;
				break;
			} else {
				nextState = getNextState(currentState, symbol, i==size-1);
			}
		}
		endOfSequence(sequence, nextState, currentState, symbols);
	}
	
	public void zipTransitions() {
		reloj.start(Reloj.ZIPPING_TRANSITIONS);
		sessionManager.sendProgress("Zipping transitions");
		ManagedThread[] zippers = sessionManager.createThreads(ManagedTransitionZipper.class, this);
		Thread[] threads = new Thread[zippers.length]; 
		for (int i=0; i<zippers.length; i++) {
			ManagedThread zipper = (ManagedThread) zippers[i];
			zippers[i].setSession(this.sessionManager.getSession());
			threads[i] = new Thread(zipper);
			threads[i].start();
		}
		for (int i=0; i<zippers.length; i++)
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		reloj.stop(Reloj.ZIPPING_TRANSITIONS);		
	}	

	public void groupTransitions() {
		reloj.start(Reloj.GROUPING_TRANSITIONS);
		int nos = this.getNumberOfStates();
		int groupings = 0;
		sessionManager.sendProgress("Grouping transitions: " + nos + " states");
		List<Transition> inputs, outputs;
		Transition input, output;
		State state, source, target;
		Integer[] statesIds = states.keySet().toArray(new Integer[0]);

		int cont = 0;
		int size = statesIds.length;
		int quantum = size/1000;
		if (quantum==0)
			quantum=1;
		for (Integer stateId : statesIds) {
			state = states.get(stateId);
			if (cont++%quantum==0)
				sessionManager.sendProgress("Grouping states: " + cont + "/" + size + " (S" + stateId + "): " + groupings + " removable states");
			inputs = state.getInputTransitionsList();
			outputs = state.getOutputTransitionsList();
			if (inputs.size()==1 && outputs.size()==1 && !state.isFinal()) {
				input = inputs.get(0);
				source = input.getSource();
				output = outputs.get(0);
				target = output.getTarget();
				if (target.getId()>state.getId()) {
					++groupings;
					state.removeAllOutputTransitions();
					source.removeOutputTransition(input);
					ILabel symbols = createTransition(input.getSymbol(), output.getSymbol());
					source.addOutputTransition(symbols, target);
					state.markToRemove();
				}
			}
		}
		reloj.stop(Reloj.GROUPING_TRANSITIONS);
		
		reloj.start(Reloj.REMOVING_UNSEFUL_STATES);
		ManagedThread[] removers = sessionManager.createThreads(ManagedRemover.class, this);
		Thread[] threads = new Thread[removers.length]; 
		for (int i=0; i<removers.length; i++) {
			ManagedThread remover = (ManagedThread) removers[i];
			threads[i] = new Thread(remover);
			threads[i].start();
		}
		
		for (int i=0; i<removers.length; i++)
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		reloj.stop(Reloj.REMOVING_UNSEFUL_STATES);
		
		reloj.start(Reloj.REENUMERATING_STATES);
		this.reset();
		sessionManager.sendProgress("Reenumerating states");
		cont = 0;
		for (int i=0; i<this.statesList.length; i++)
			this.statesList[i].setId(cont++);		
		this.states=new ConcurrentHashMap<>();
		for (int i=0; i<this.statesList.length; i++) {
			state = this.statesList[i];
			this.states.put(i, state);
		}

		reloj.stop(Reloj.REENUMERATING_STATES);
	}
	
	private ILabel createTransition(ILabel left, ILabel right) {
		RightMember rmLeft = (RightMember) left;
		RightMember rmRight = (RightMember) right;
		return new Concat(rmLeft, rmRight);
	}

	protected abstract State getNextState(State[] currentState, Symbol symbol, boolean lastSymbol);
	protected abstract void endOfSequence(Sequence sequence, State nextState, State[] currentState, List<Symbol> symbols);
	
	protected abstract Automata newAutomata();
	
	public Automata copy() {
		reloj.start(Reloj.COPYING_AUTOMATON);
		sessionManager.sendProgress("Making a copy of the automata");
		Automata result = newAutomata();
		result.states.remove(0);
		for (State state : states.values()) {
			sessionManager.sendProgress("Copying state S" + state.getId());
			State sc = (State) state.copy();
			result.add(sc);
		}
		int cont = 1, nos = states.size();
		for (State state : states.values()) {
			State sc = result.states.get(state.getId());
			sessionManager.sendProgress("Copying transitions of the state S" + state.getId() + " (" + cont++ + "/" + nos + ")");
			for (Transition t : state.getOutputTransitionsList()) {
				State target = result.states.get(t.getTarget().getId());
				sc.addOutputTransition((ILabel) t.getSymbol().copy(), target);
			}
		}
		reloj.stop(Reloj.COPYING_AUTOMATON);
		return result;
	}
	
	public Automata minimize() {
		return AutomataMinimizer.minimize(this, sessionManager, reloj);
	}
	
	public void remove(State stateToRemove, State stateToPreserve) {
		replaceTransitions(stateToPreserve, stateToRemove);
		removeOutputTransitions(stateToRemove);
		remove(stateToRemove);
	}
	
	public void remove(State state) {
		states.remove(state.getId());
		state.removeAllOutputTransitions();
	}

	private void removeOutputTransitions(State state) {
		List<Transition> transitions = state.getOutputTransitionsList();
		for (int i = transitions.size()-1; i>=0; i--) {
			Transition t = transitions.get(i);
			state.removeOutputTransition(t);
		}
	}

	private void replaceTransitions(State stateToPreserve, State stateToRemove) {
		List<Transition> inputs = stateToRemove.getInputTransitionsList();
		for (int i= inputs.size()-1; i>=0; i--) {
			Transition input = inputs.get(i);
			State source = input.getSource();
			State target = input.getTarget();
			source.removeOutputTransition(input);
			if (source==target)
				stateToPreserve.addOutputTransition(input.getSymbol(), stateToPreserve);
			else
				source.addOutputTransition(input.getSymbol(), stateToPreserve);	
		}
	}
	
	public int getNumberOfStates() {
		return this.states.size();
	}

	public State getState(int key) {
		return this.states.get((Integer) key);
	}

	public List<Transition> getTransitions(State s0, State s1) {
		List<Transition> result = new ArrayList<>();
		List<Transition> transitions = s0.getOutputTransitionsList();
		for (Transition transition : transitions)
			if (transition.getTarget()==s1)
				result.add(transition);
		return result;
	}
	
	protected abstract State newState();
	
	protected void addAllSubsequences(Sequence sequence, State finalState) {
		Sequence subsequence;
		int n = this.getNumberOfStates();
		int start = (int) (n*0);
		for (int i=start; i<sequence.length(); i++) {
			subsequence = sequence.getSubsequence(i);
			State initialState = calculateSecondState(sequence, i);
			subsequence.setSecondState(initialState);
			subsequence.setFinalState(finalState);
			this.finalSequences.put(subsequence.toString(), subsequence);
		}		
	}

	protected State calculateSecondState(Sequence sequence, int start) {
		int i=0;
		State currentState = this.initial;
		Symbol symbol;
		while (i<=start) {
			symbol = sequence.getSymbols().get(i++);
			currentState = currentState.nextState(symbol);
		}
		return currentState;
	}

	protected void printStates() {
		Collection<State> eStates = states.values();
		for (State state : eStates) {
			System.out.println("S" + state.getId());
			List<Transition> eTransitions = state.getOutputTransitionsList();
			for (Transition t : eTransitions)
				System.out.println("\t" + t.getSymbol() + "-> S" + t.getTarget().getId());
		}
	}

	public String getRegularExpression(String algorithm) {
		if (algorithm.equals("matrices")) {
			TransitiveClosureMatrixSolver solver = new TransitiveClosureMatrixSolver(this, sessionManager, reloj);
			solver.solve();

			return solver.getRegularExpression();
		} else {
			Collection<Equation> equations = getEquations();
			StringBuilder sb = new StringBuilder();
			int i=0;
			Iterator<Equation> iEquations = equations.iterator();
			while (iEquations.hasNext()) {
				Equation eq = iEquations.next();
				if (eq.isFinal()) {
					sb.append(eq.toString());
					if (i<equations.size())
						sb.append("|");
				}
				i++;
			}
			String result = sb.toString();
			if (result.endsWith("|"))
				result=result.substring(0, result.length()-1);
			return result;
		}
	}
	
	public Collection<Equation> getEquations() {
		return getEquations();
	}

	public void buildEquations() {
		Collection<State> states = this.states.values();
		this.equationSystem = new EquationSystem();
		Equation eq;
		for (State state : states) {
			eq = new Equation(this.equationSystem, this.separated);
			eq.setLeftMember(state);
			sessionManager.sendProgress("Calculating equation for state " + state);
			List<Transition> inputTransitions = state.getInputTransitionsList();
			for (Transition inputTransition : inputTransitions) {
				State source = (State) inputTransition.getSource().copy();
				RightMember symbols = (RightMember) inputTransition.getSymbol();
				eq.addRightMember(new Concat(source, symbols));
				eq.addStateUsed(source.getId());
			}
			this.equationSystem.add(eq);
		}
	}
	
	public EquationSystem getEquationSystem() {
		return equationSystem;
	}

	public Map<Integer, State> getStates() {
		return this.states;
	}

	public void add(State state) {
		this.states.put(state.getId(), state);
	}
	
	public JSONObject toJSON() throws JSONException {
		JSONObject jso = new JSONObject();
		jso.put("initialState", this.getState(0).toString());
		JSONArray finalStates = new JSONArray();
		JSONArray transitions = new JSONArray();
		for (State state : this.states.values()) {
			if (state.isFinal())
				finalStates.put(state.toString());
			for (Transition t : state.getOutputTransitionsList()) {
				JSONObject jsoT = new JSONObject();
				jsoT.put("sourceState", state.toString());
				jsoT.put("symbol", t.getSymbol().toString());
				jsoT.put("targetState", t.getTarget().toString());
				transitions.put(jsoT);
			}
		}
		jso.put("finalStates", finalStates);
		jso.put("transitions", transitions);
		TransitionsPairsSolver tps = new TransitionsPairsSolver(this, sessionManager, reloj);

		List<IOPairTable> tables = tps.buildTables();
		jso.put("pairTables", getJSON(tables));
		return jso;
	}

	private JSONArray getJSON(List<IOPairTable> tables) throws JSONException {
		JSONArray jsa = new JSONArray();
		for (int i = 0; i<tables.size(); i++)
			jsa.put(tables.get(i).toJSON());
		return jsa;
	}

	private State[] statesList;
	@Override
	public synchronized void reset() {
		this.currentIndex=0;
		this.statesList = this.states.values().toArray(new State[this.states.size()]);
	}

	@Override
	public synchronized State giveMeNext() {
		if (this.currentIndex<this.statesList.length) {
			State state = this.statesList[currentIndex++];
			state.setIndex(currentIndex);
			return state;
		}
		return null;
	}
	
	@Override
	public int size() {
		return this.states.size();
	}
	
	public State getInitial() {
		return initial;
	}

	public void setInitialState(State state) {
		if (this.initial!=null)
			this.initial.setInitial(false);
		this.initial = state;
	}

	public List<State> getFinalStates() {
		Vector<State> finalStates = new Vector<>();
		for (State state : this.states.values())
			if (state.isFinal())
				finalStates.add(state);
		return finalStates;
	}
	
	public SessionManager getSessionManager() {
		return sessionManager;
	}
}
