package edu.uclm.esi.regexpbuilder.model.matrix;

import edu.uclm.esi.regexpbuilder.model.Automata;
import edu.uclm.esi.regexpbuilder.model.Reloj;
import edu.uclm.esi.regexpbuilder.model.SessionManager;
import edu.uclm.esi.regexpbuilder.model.re.RightMember;

public class HeuristicMatrixSolver extends MatrixSolver {
	public HeuristicMatrixSolver(Automata automata, SessionManager sessionManager, Reloj reloj) {
		super(automata, sessionManager, reloj);
	}

	@Override
	public void solve() {
		this.matrix = new Matrix(automata);
		Matrix original = new Matrix(automata);
		this.matrix= original.by(this.matrix);
	}
	
	public String getRegularExpression() {
		RightMember regexp = matrix.getRegularExpression();
		regexp = regexp.simplify();
		String sre = regexp.toString();
		return sre;
	}
}
