package edu.uclm.esi.regexpbuilder.model;

import org.springframework.web.socket.WebSocketSession;

public abstract class REThread extends Thread {
	protected int id;
	protected int start;
	protected int end;
	protected Object[] objectsToProcess;
	protected WebSocketSession session;
	
	public REThread(int id, int start, int end, Object[] objectsToProcess) {
		this.id = id;
		this.start = start;
		this.end = end;
		this.objectsToProcess = objectsToProcess;
	}

	public void setSession(WebSocketSession session) {
		this.session = session;
	}
	
}
