package edu.uclm.esi.regexpbuilder.model;

import edu.uclm.esi.regexpbuilder.model.re.State;

public abstract class ZippingStrategy {
	protected int repeticiones;
	protected ManagedTransitionZipper zipper;
	
	public ZippingStrategy(ManagedTransitionZipper zipper, int repeticiones) {
		this.zipper = zipper;
		this.repeticiones = repeticiones;
	}
	
	public abstract CharSequence zip(State state, CharSequence expresion, int depthLevel);
}
