package edu.uclm.esi.regexpbuilder.model;

import org.springframework.web.socket.WebSocketSession;

public abstract class Solver {
	protected Automata automata;
	protected Reloj reloj;
	protected SessionManager sessionManager;
	
	public Solver(Automata automata, SessionManager sessionManager, Reloj reloj) {
		this.automata = automata;
		this.sessionManager = sessionManager;
		this.reloj = reloj;
	}
	
	public WebSocketSession getSession() {
		return sessionManager.getSession();
	}
	
	public abstract void solve();
	public abstract String getRegularExpression();
}
