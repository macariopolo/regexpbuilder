package edu.uclm.esi.regexpbuilder.model;

import edu.uclm.esi.regexpbuilder.web.ws.WSServerRegExp;

public class StatesRemover extends REThread {

	private Automata automata;

	public StatesRemover(int id, int start, int end, Object[] objectsToProcess) {
		super(id, start, end, objectsToProcess);
	}
	
	public void setAutomata(Automata automata) {
		this.automata = automata;
	}

	@Override
	public synchronized void start() {
		for (int i=start; i<end; i++) {
			edu.uclm.esi.regexpbuilder.model.re.State state = (edu.uclm.esi.regexpbuilder.model.re.State) objectsToProcess[i];
			if (state.isRemovable()) {
				WSServerRegExp.sendSubprogress(session, id, "Removing S" + state, 0.25);
				automata.remove(state);
			}
		}
		WSServerRegExp.sendSubprogress(session, id, "idle", 1);
	}
}
