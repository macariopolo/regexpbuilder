package edu.uclm.esi.regexpbuilder.model.statemachines;

import org.json.JSONException;
import org.json.JSONObject;

import edu.uclm.esi.regexpbuilder.model.Transition;
import edu.uclm.esi.regexpbuilder.model.re.State;

public class IOPair {
	private IOPairTable table;
	private Transition input;
	private Transition output;
	private int visits;
	
	public IOPair(IOPairTable table, Transition input, Transition output) {
		this.table = table;
		this.input = input;
		this.output = output;
		this.visits = 0;
	}
	
	@Override
	public String toString() {
		int verboseLevel = 1;
		if (verboseLevel==2)
			return this.input.getSource() + " + " + this.input.getSymbol() + " -> " + this.output.getSymbol() + " + " + 
				this.output.getTarget() + " / "  + this.visits + " visits";
		if (verboseLevel==1)
			return "" + this.input.getSource() + this.input.getSymbol() + ", " + this.output.getTarget() + this.output.getSymbol() + " (" + visits + ")";
		return "" + this.input.getSource() + this.input.getSymbol() + ", " + this.output.getTarget() + this.output.getSymbol();
	}

	public int getVisits() {
		return this.visits;
	}
	
	public IOPairTable getTable() {
		return table;
	}

	public Transition getInput() {
		return this.input;
	}

	public void visit() {
		this.visits++;
	}

	public Transition getOutput() {
		return this.output;
	}
	
	public State getState() {
		return this.table.getState();
	}

	public JSONObject toJSON() throws JSONException {
		JSONObject jso = new JSONObject();
		jso.put("inputState", this.input.getSource().toString());
		jso.put("inputSymbol", this.input.getSymbol().toString());
		jso.put("outputState", this.output.getTarget().toString());
		jso.put("outputSymbol", this.output.getSymbol().toString());
		jso.put("visits", this.visits);
		return jso;
	}
}
