package edu.uclm.esi.regexpbuilder.model.minimizer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import edu.uclm.esi.regexpbuilder.model.Automata;
import edu.uclm.esi.regexpbuilder.model.REThread;
import edu.uclm.esi.regexpbuilder.model.Reloj;
import edu.uclm.esi.regexpbuilder.model.SessionManager;
import edu.uclm.esi.regexpbuilder.model.Transition;
import edu.uclm.esi.regexpbuilder.model.re.State;

public class AutomataMinimizer {

	public static Automata minimize(Automata automata, SessionManager sessionManager, Reloj reloj) {
		reloj.start(Reloj.MINIMIZE_AUTOMATA);
		Automata result = automata.copy();
		
		List<String> keysWithEquivalent;
		Map<String, List<State>> equivalentStates = new ConcurrentHashMap<>();
		int iter = 1;
		int nos = automata.getNumberOfStates();
		long timeIni, timeFin;
		do {
			equivalentStates=new ConcurrentHashMap<>();
			timeIni = System.currentTimeMillis();
			keysWithEquivalent=loadEquivalentStatesInParallel(nos, iter, result, equivalentStates, sessionManager);
			timeFin = System.currentTimeMillis();
			joinStates(iter++, nos, result, keysWithEquivalent, equivalentStates, sessionManager);			
		} while (!keysWithEquivalent.isEmpty());
		sessionManager.sendProgress("Minimized automata has " + result.getNumberOfStates() + " states instead of " + nos);
		reloj.stop(Reloj.MINIMIZE_AUTOMATA);
		return result;
	}

	private static void joinStates(int iter, int nos, Automata automata, List<String> keysWithEquivalent, Map<String, List<State>> equivalentStates, SessionManager sessionManager) {
		List<State> states;
		String key;
		for (int i=0; i<keysWithEquivalent.size(); i++) {
			key=keysWithEquivalent.get(i);
			states = equivalentStates.get(key);
			joinStates(iter, nos, automata, states, sessionManager);
		}		
	}

	private static void joinStates(int iter, int nos, Automata automata, List<State> states, SessionManager sessionManager) {
		State stateToPreserve = states.get(0);
		sessionManager.sendProgress("Iteration " + iter + ": joining " + (states.size()-1) + " states into S" + stateToPreserve.getId());
		State stateToRemove;
		int size = states.size();
		for (int i=1; i<size; i++) {
			stateToRemove = states.get(i);
			List<Transition> inputs = stateToRemove.getInputTransitionsList();
			for (int j=inputs.size()-1; j>=0; j--) {
				Transition input = inputs.get(j);
				State source = input.getSource();
				State target = input.getTarget();
				source.removeOutputTransition(input);
				if (source==target)
					stateToPreserve.addOutputTransition(input.getSymbol(), stateToPreserve);
				else
					source.addOutputTransition(input.getSymbol(), stateToPreserve);	
			}
		}
		for (int i=states.size()-1; i>=1; i--) 
			automata.remove(states.get(i));
		sessionManager.sendProgress("New automata has " + automata.getNumberOfStates() + " states (the original had " + nos + ")");
	}
	
	private static ArrayList<String> loadEquivalentStatesInParallel(int nos, int iter, Automata automata, Map<String, List<State>> equivalentStates, SessionManager sessionManager) {
		REThread[] searchers = sessionManager.createThreads(Searcher.class, automata.getStates().values().toArray());
		for (int i=0; i<searchers.length; i++) {
			Searcher searcher = (Searcher) searchers[i];
			searcher.findIn(equivalentStates);
			searcher.start();
		}
		
		ArrayList<String> keysWithEquivalent = new ArrayList<>();
		List<String> partialKeys;
		String key;
		for (int i=0; i<searchers.length; i++) {
			Searcher searcher = (Searcher) searchers[i];
			try {
				searcher.join();
				partialKeys=searcher.getKeysWithEquivalent();
				for (int j=0; j<partialKeys.size(); j++) {
					key = partialKeys.get(j);
					if (!keysWithEquivalent.contains(key))
						keysWithEquivalent.add(key);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return keysWithEquivalent;
	}
}
