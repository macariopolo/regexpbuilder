package edu.uclm.esi.regexpbuilder.model.re;

import java.lang.reflect.Method;

@SuppressWarnings("unused")
public class Or {
	public static RightMember or(RightMember a, RightMember b) {
		RightMember result = null;
		String aName = a.getClass().getSimpleName();
		String bName = b.getClass().getSimpleName();
		String methodName = "or" + aName + bName;
		try {
			Method method = Or.class.getDeclaredMethod(methodName, RightMember.class, RightMember.class);
			result = (RightMember) method.invoke(null, a, b);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	private static RightMember orLambdaLambda(RightMember a, RightMember b) {
		return a.copy();
	}
	
	private static RightMember orLambdaSymbol(RightMember a, RightMember b) {
		return b.copy();
	}
	
	private static RightMember orLambdaState(RightMember a, RightMember b) {
		return b.copy();
	}
	
	private static RightMember orLambdaConcat(RightMember a, RightMember b) {
		return b.copy();
	}
	
	private static RightMember orLambdaClosure(RightMember a, RightMember b) {
		return b.copy();
	}
	
	private static RightMember orLambdaRightMemberList(RightMember a, RightMember b) {
		return b.copy();
	}
	
	private static RightMember orSymbolLambda(RightMember a, RightMember b) {
		return a.copy();
	}
	
	private static RightMember orSymbolSymbol(RightMember a, RightMember b) {
		if (a.equals(b))
			return a.copy();
		return new RightMemberList(a.copy(), b.copy());
	}
	
	private static RightMember orSymbolState(RightMember a, RightMember b) {
		return new RightMemberList(a.copy(), b.copy());
	}
	
	private static RightMember orSymbolConcat(RightMember a, RightMember b) {
		return new RightMemberList(a.copy(), b.copy());
	}
	
	private static RightMember orSymbolClosure(RightMember a, RightMember b) {
		if (b.completelyContains(a))
			return b.copy();
		return new RightMemberList(a.copy(), b.copy());
	}
	
	private static RightMember orSymbolRightMemberList(RightMember a, RightMember b) {
		if (b.completelyContains(a))
			return b.copy();
		return new RightMemberList(a.copy(), b.copy());
	}
	
	private static RightMember orStateLambda(RightMember a, RightMember b) {
		return orLambdaState(b, a);
	}
	
	private static RightMember orStateSymbol(RightMember a, RightMember b) {
		return orSymbolState(b, a);
	}
	
	private static RightMember orStateState(RightMember a, RightMember b) {
		return orSymbolSymbol(a, b);
	}
	
	private static RightMember orStateConcat(RightMember a, RightMember b) {
		return new RightMemberList(a.copy(), b.copy());
	}
	
	private static RightMember orStateClosure(RightMember a, RightMember b) {
		return orSymbolClosure(a, b);
	}
	
	private static RightMember orStateRightMemberList(RightMember a, RightMember b) {
		return orSymbolRightMemberList(a, b);
	}
	
	private static RightMember orConcatLambda(RightMember a, RightMember b) {
		return orLambdaConcat(b, a);
	}
	
	private static RightMember orConcatSymbol(RightMember a, RightMember b) {
		return orSymbolConcat(b, a);
	}
	
	private static RightMember orConcatState(RightMember a, RightMember b) {
		return orStateConcat(b, a);
	}
	
	private static RightMember orConcatConcat(RightMember a, RightMember b) {
		if (a.equals(b))
			return a.copy();
		return new RightMemberList(a, b);
	}
	
	private static RightMember orConcatClosure(RightMember a, RightMember b) {
		if (b.completelyContains(a))
			return b.copy();
		if (a.completelyContains(b)) {
			Closure result = (Closure) b;
			result = (Closure) result.copy();
			result.setOperator('+');
			return result;
		}
		return new RightMemberList(a, b);
	}
	
	private static RightMember orConcatRightMemberList(RightMember a, RightMember b) {
		return orConcatClosure(a, b);
	}
	
	private static RightMember orClosureLambda(RightMember a, RightMember b) {
		return orLambdaClosure(b, a);
	}
	
	private static RightMember orClosureSymbol(RightMember a, RightMember b) {
		return orSymbolClosure(b, a);
	}
	
	private static RightMember orClosureState(RightMember a, RightMember b) {
		return orStateClosure(b, a);
	}
	
	private static RightMember orClosureConcat(RightMember a, RightMember b) {
		return orConcatClosure(b, a);
	}
	
	private static RightMember orClosureClosure(RightMember a, RightMember b) {
		if (a.completelyContains(b))
			return a.copy();
		if (b.completelyContains(a))
			return b.copy();
		return new RightMemberList(a, b);
	}
	
	private static RightMember orClosureRightMemberList(RightMember a, RightMember b) {
		if (b.completelyContains(a))
			return b.copy();
		if (a.completelyContains(b)) 
			return a.copy();
		return new RightMemberList(a, b);
	}
	
	private static RightMember orRightMemberListRightMemberList(RightMember a, RightMember b) {
		if (a.completelyContains(b))
			return a.copy();
		if (b.completelyContains(a))
			return b.copy();
		return new RightMemberList(a, b);
	}
	
	private static RightMember orRightMemberListLambda(RightMember a, RightMember b) {
		return orLambdaRightMemberList(b, a);
	}
	
	private static RightMember orRightMemberListSymbol(RightMember a, RightMember b) {
		return orSymbolRightMemberList(b, a);
	}
	
	private static RightMember orRightMemberListState(RightMember a, RightMember b) {
		return orStateRightMemberList(b, a);
	}
	
	private static RightMember orRightMemberListConcat(RightMember a, RightMember b) {
		return orConcatRightMemberList(b, a);
	}
	
	private static RightMember orRightMemberListClosure(RightMember a, RightMember b) {
		if (b.completelyContains(a)) {
			Closure closure = (Closure) b;
			closure = (Closure) closure.copy();
			closure.setOperator('+');
			return closure;
		}
		return new RightMemberList(a, b);
	}
}
