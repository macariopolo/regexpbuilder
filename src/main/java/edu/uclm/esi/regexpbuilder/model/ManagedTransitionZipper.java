package edu.uclm.esi.regexpbuilder.model;

import java.util.List;

import edu.uclm.esi.regexpbuilder.model.re.Closure;
import edu.uclm.esi.regexpbuilder.model.re.Concat;
import edu.uclm.esi.regexpbuilder.model.re.ILabel;
import edu.uclm.esi.regexpbuilder.model.re.RightMember;
import edu.uclm.esi.regexpbuilder.model.re.State;
import edu.uclm.esi.regexpbuilder.model.re.Symbol;
import edu.uclm.esi.regexpbuilder.web.ws.WSServerRegExp;

public class ManagedTransitionZipper extends ManagedThread {
	private int repeticiones = 3;
	private ZippingStrategy zippingStrategy;
	
	public ManagedTransitionZipper() {
		this.zippingStrategy = new LemusZippingStrategy(this);
	}
	
	@Override
	public void run() {
		List<Transition> outputs;
		IProcessable processable;
		while ((processable=this.manager.giveMeNext())!=null) {
			State state = (State) processable.getObject();
			WSServerRegExp.sendSubprogress(session, id, "Zipping transitions of state S" + state.getId() + ": " + state.getIndex() + "/" + this.size, 0.25);
			outputs = state.getOutputTransitionsList();
			for (int j=0; j<outputs.size(); j++) {
				Transition output = outputs.get(j);
				String vieja = output.getSymbol().toString();
				CharSequence nueva = this.zippingStrategy.zip(state, vieja, 1);
				if (!nueva.equals(vieja)) {
					ILabel symbol = this.buildTransition(nueva.toString());
					output.setSymbol(symbol);
				}
			}
		}
		WSServerRegExp.sendSubprogress(session, id, "idle", 1);
	}
	
	public CharSequence zip(State state, CharSequence expresion) {
		return this.zippingStrategy.zip(state, expresion, 1);
	}
	
	public static void main(String[] args) {
		long timeIni = System.currentTimeMillis();
		StringBuilder sb = new StringBuilder();
		StringBuilder pattern = new StringBuilder("floostuuuuuumrnstuuuuuumrnstuuuuuumrnstuuuuuumrn");
		ManagedTransitionZipper zipper = new ManagedTransitionZipper();
		for (int i=3; i<=3; i++) {
			sb.append(pattern);
			long timeIni0 = System.currentTimeMillis();
			CharSequence shortest = zipper.zip(null, sb);
			long timeFin0 = System.currentTimeMillis();
			ILabel label = zipper.buildTransition(shortest.toString());
			System.out.println(label);
			System.out.println(i + ") Length: " + sb.length() + "; shortest:" + shortest + "; time: " + (timeFin0-timeIni0)/10 + " centésimas");
		}
		long timeFin = System.currentTimeMillis();
		System.out.println("Total time: " + (timeFin-timeIni)/1000  + " seconds");
	}
		
	public ILabel buildTransition(String s) {
		int length = s.length();
		if (length==1)
			return new Symbol(s);
	
		ILabel root = null;
		for (int i=0; i<length; i++) {
			char c = s.charAt(i);
			if (Character.isLetter(c)) {
				if (root==null)
					root = new Symbol("" + c);
				else if (root instanceof Symbol || root instanceof Concat || root instanceof Closure) {
					root = new Concat((RightMember) root, new Symbol("" + c));
				} 
			} else if (c=='{') {
				//int posDcho = s.indexOf('}', i+1);
				int posDcho = getPosParCierre(s, i+1);
				
				String closured = s.substring(i+1, posDcho);
				i=posDcho;
				if (root!=null)
					root = new Concat((RightMember) root, new Closure(buildTransition(closured), '+'));
				else
					root = new Closure(buildTransition(closured), '+');
			}
		}
		return root;
	}

	private int getPosParCierre(String s, int start) {
		int contParsIzdos = 1, contParDchos = 0;
		char c;
		int i;
		for (i=start; i<s.length(); i++) {
			c = s.charAt(i);
			if (c=='{')
				contParsIzdos++;
			else if (c=='}')
				contParDchos++;
			if (contParsIzdos == contParDchos)
				break;
		} 
		return i;
	}

	public int getRepeticiones() {
		return this.repeticiones;
	}
}
