package edu.uclm.esi.regexpbuilder.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import edu.uclm.esi.regexpbuilder.model.re.State;

public class LemusZippingStrategy extends ZippingStrategy {

	public LemusZippingStrategy(ManagedTransitionZipper zipper) {
		super(zipper, zipper.getRepeticiones());
	}

	@Override
	public StringBuilder zip(State state, CharSequence s, int depthLevel) {
		String expresion = s.toString();
		int length = expresion.length();
		int maxLength = length/repeticiones;
		ArrayList<Trio> posiciones = new ArrayList<>();
		ArrayList<Integer> setIndexes = new ArrayList<>();
		
		for (int currentLength = maxLength; currentLength>=1; currentLength--) {
			int offset = 0;
			do {
				String lastTrozo = null;
				int inicioCoincidencia = 0, finCoincidencia = 0;
				int currentPos = offset;
				do {
					int end = currentPos + currentLength;
					if (end>length || visitado(setIndexes, currentPos, end))
						break;
					String trozo = expresion.substring(currentPos, end);
					if (currentPos>0) {
						if (trozo.equals(lastTrozo)) {
							if (finCoincidencia-inicioCoincidencia==0) {
								inicioCoincidencia = currentPos-currentLength;
								finCoincidencia = end - 1;
							} else {
								finCoincidencia = end - 1 ;
							}
						} else {
							updatePositions(posiciones, setIndexes, currentLength, inicioCoincidencia, finCoincidencia);
							inicioCoincidencia = 0;
							finCoincidencia = 0;
						}
					}
					lastTrozo = trozo;
					currentPos = currentPos + currentLength;
					int visitedPos = Collections.binarySearch(setIndexes, currentPos); 
					if (visitedPos>=0) {
						updatePositions(posiciones, setIndexes, currentLength, inicioCoincidencia, finCoincidencia);
						inicioCoincidencia = 0;
						finCoincidencia = 0;
						for (int i=visitedPos; i<setIndexes.size(); i++) {
							if (i<setIndexes.size()-1) {
								int siguiente =  setIndexes.get(i+1);
								int este = setIndexes.get(i);
								if (siguiente-este>currentLength) {
									currentPos = este + 1;
									break;
								}
							} else {
								currentPos = setIndexes.get(i) + 1;
								break;
							}
						}
						lastTrozo = null;
					} 
				} while (currentPos<length); 
				updatePositions(posiciones, setIndexes, currentLength, inicioCoincidencia, finCoincidencia);
				offset++;
			} while (offset<currentLength);
		}
		//System.out.println(posiciones);
		
		Collections.sort(posiciones, new Comparator<Trio>() {
			@Override
			public int compare(Trio o1, Trio o2) {
				//int longitudes = o2.longitud - o1.longitud;
				//if (longitudes==0) {
					return o1.inicio-o2.inicio;
				//}
				//return longitudes;
			}			
		});
		//System.out.println(posiciones);
		
		if (posiciones.isEmpty())
			return new StringBuilder(expresion);
		int inicio, fin, longitud;
		Trio trio = posiciones.get(0);
		String token;
		String izquierda = expresion.substring(0, trio.inicio);
		izquierda = this.zip(state, izquierda, depthLevel).toString();
		String interior="";
		for (int i=0; i<posiciones.size(); i++) {
			trio = posiciones.get(i);
			inicio = trio.inicio;
			fin = trio.fin;
			longitud = trio.longitud;
			token = expresion.substring(inicio, inicio + longitud);
			token = this.zip(state, token, 1).toString();
			interior = interior + "{" + token + "}";
			if (i<posiciones.size()-1) {
				Trio siguienteTrio = posiciones.get(i+1);
				interior = interior + expresion.substring(fin+1, siguienteTrio.inicio);
			}
		}
		String derecha = ""; 
		if (expresion.length()>trio.fin) {
			derecha = derecha + expresion.substring(trio.fin+1);
			derecha = this.zip(state, derecha, 1).toString();
		}
		String nueva = izquierda + interior + derecha;
		return new StringBuilder(nueva);
	}

	private boolean visitado(ArrayList<Integer> setIndexes, int currentPos, int end) {
		for (int i=currentPos; i<end; i++)
			if (Collections.binarySearch(setIndexes, i)>=0)
				return true;
		return false;
	}

	private void updatePositions(ArrayList<Trio> posiciones, ArrayList<Integer> setIndexes,
			int currentLength, int inicioCoincidencia, int finCoincidencia) {
		if (finCoincidencia-inicioCoincidencia +1 >= currentLength * repeticiones) {
			Trio trio = new Trio(inicioCoincidencia, finCoincidencia, currentLength);
			posiciones.add(trio);
			for (int i=inicioCoincidencia; i<=finCoincidencia; i++)
				setIndexes.add(i);
			Collections.sort(setIndexes);
		}
	}
}
