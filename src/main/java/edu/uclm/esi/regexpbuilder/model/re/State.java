package edu.uclm.esi.regexpbuilder.model.re;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import edu.uclm.esi.regexpbuilder.model.IProcessable;
import edu.uclm.esi.regexpbuilder.model.Transition;
import edu.uclm.esi.regexpbuilder.model.equations.Equation;
import edu.uclm.esi.regexpbuilder.model.equations.RMCounter;

public class State extends RightMember implements IProcessable {
	private int id;
	private ArrayList<Transition> inputTransitionsList;
	private ArrayList<Transition> outputTransitionsList;
	private boolean isFinal;
	private boolean initial;
	private List<Integer> statesThatUseMe;
	private boolean removable;
	private int index;
	
	public State(int id) {
		this.id=id;
		this.inputTransitionsList = new ArrayList<>();
		this.outputTransitionsList = new ArrayList<>();
		this.statesThatUseMe = new ArrayList<>();
	}
	
	public String key() {
		StringBuilder sb = new StringBuilder();
		sb.append(initial + "/" + isFinal + "/");
		for (Transition t : this.outputTransitionsList)
			sb.append(t.key());
		return sb.toString();
	}
	
	public int getId() {
		return id;
	}
	
	private Transition findInputTransition(ILabel symbol) {
		for (Transition t : this.inputTransitionsList)
			if (t.getSymbol().equals(symbol)) {
				return t;
			}
		return null;
	}
	
	private Transition findOutputTransition(ILabel symbol) {
		for (Transition t : this.outputTransitionsList)
			if (t.getSymbol().equals(symbol)) {
				return t;
			}
		return null;
	}

	public State nextState(Symbol symbol) {
		Transition transition = findOutputTransition(symbol);
		if (transition!=null)
			return transition.getTarget();
		return null;
	}
	
	public Transition replaceOutputTransition(ILabel symbol, State nextState) {
		Transition transition = this.findOutputTransition(symbol);
		if (transition!=null) 
			this.removeOutputTransition(transition);
		transition = new Transition(this, nextState, symbol); 
		this.outputTransitionsList.add(transition);
		nextState.inputTransitionsList.add(transition);
		return transition;
	}
	
	public Transition addOutputTransition(ILabel symbol, State nextState) {
		Transition transition = this.findOutputTransition(symbol);
		if (transition==null) {
			transition = new Transition(this, nextState, symbol);
			this.outputTransitionsList.add(transition);
			nextState.inputTransitionsList.add(transition);
		}
		return transition;
	}
	
	@Override
	public String toString() {
		return "S" + this.id + (this.isFinal ? ">" : "");
	}

	public void setEnd(boolean end) {
		this.isFinal = end;
	}

	public void removeTransition(Symbol symbol) {
		Transition t = this.findOutputTransition(symbol);
		this.outputTransitionsList.remove(t);
		t.getTarget().removeInputTransition(t);
	}

	public boolean isFinal() {
		return this.isFinal;
	}

	public void setInitial(boolean initial) {
		this.initial = initial;
	}
	
	public boolean isInitial() {
		return initial;
	}

	@Override
	public int length() {
		return 1;
	}

	@Override
	public RightMember copy() {
		State state = new State(this.id);
		state.initial = this.initial;
		state.isFinal = this.isFinal;
		for (Integer n : this.statesThatUseMe)
			state.statesThatUseMe.add(n);
		return state;
	}

	@Override
	protected int depth() {
		return 0;
	}

	@Override
	public void loadSubTrees(Map<String, RMCounter> subarboles, int weight) {
		if (weight<=1) {
			String key = this.toString();
			RMCounter rmc = subarboles.get(key);
			if (rmc==null)
				rmc = new RMCounter(this);
			subarboles.put(this.toString(), rmc);
			rmc.inc();
		}
	}
	
	public boolean hasInputTransitionWith(ILabel symbol) {
		return this.findInputTransition(symbol)!=null;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTimesUsed() {
		return this.statesThatUseMe.size();
	}

	@Override
	public RightMember removeState(Equation equation, State state) {
		return this;
	}

	@Override
	public RightMember replaceState(Equation equation, Equation replacementEq) {
		if (this.equals(replacementEq.getLeftMember()))
			return replacementEq.getRightMembers().copy();
		return this.copy();
	}

	@Override
	public List<State> getStatesUsed() {
		ArrayList<State> result = new ArrayList<>();
		result.add(this);
		return result;
	}

	@Override
	public boolean contains(RightMember rightMember) {
		return rightMember.toString().equals(this.toString());
	}

	@Override
	public RightMember trimUntil(Concat node) {
		return this;
	}
	
	public List<Transition> getInputTransitionsList() {
		return inputTransitionsList;
	}
	
	public List<Transition> getOutputTransitionsList() {
		return outputTransitionsList;
	}

	public boolean equivalent(State state) {
		ArrayList<Transition> others = state.outputTransitionsList;
		if (this.isFinal && !state.isFinal)
			return false;
		if (!this.isFinal && state.isFinal)
			return false;
		if (this.initial || state.initial)
			return false;
		if (this.outputTransitionsList.size()!=others.size())
			return false;
		
		for (int i=0; i<this.outputTransitionsList.size(); i++) {
			Transition mine = this.outputTransitionsList.get(i);
			ILabel symbol = mine.getSymbol();
			Transition other = state.findOutputTransition(symbol);
			if (other==null)
				return false;
			if (!mine.getTarget().toString().equals(other.getTarget().toString()))
				return false;
		}
		return true;
	}

	public void removeOutputTransition(Transition t) {
		this.outputTransitionsList.remove(t);
		t.getTarget().removeInputTransition(t);
	}
	
	public void removeAllTransitions() {
		this.outputTransitionsList.clear();
		this.inputTransitionsList.clear();
	}
	
	public void removeInputTransition(Transition t) {
		this.inputTransitionsList.remove(t);
	}

	@Override
	public RightMember simplify() {
		return this;
	}

	public void addOutputTransition(String s, State target) {
		Symbol symbol = new Symbol(s);
		this.addOutputTransition(symbol, target);
	}
	
	@Override
	public boolean completelyContains(RightMember rightMember) {
		return rightMember instanceof State && rightMember.toString().equals(this.toString());
	}

	@Override
	public String polish() {
		return "" + this.id;
	}

	public void removeAllOutputTransitions() {
		for (Transition t : this.outputTransitionsList)
			t.getTarget().removeInputTransition(t);
		this.outputTransitionsList.clear();
	}

	public void markToRemove() {
		this.removable = true;
	}

	public boolean isRemovable() {
		return this.removable;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		State other = (State) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public RightMember loadSubTree(int currentLength) {
		return currentLength==1 ? this : null;
	}

	@Override
	public IProcessable getObject() {
		return this;
	}

	@Override
	public int getIndex() {
		return this.index;
	}

	@Override
	public void setIndex(int index) {
		this.index = index;
	}
	
	@Override
	public boolean produces(RightMember rm) {
		return rm.equals(this);
	}

	public void removeInputLambdas() {
		int size = this.inputTransitionsList.size();
		Transition t;
		for (int i=size-1; i>=0; i--) {
			t = this.inputTransitionsList.get(i);
			if (t.getSymbol() instanceof Lambda) 
				t.getSource().removeOutputTransition(t);
		}
	}

	public boolean hasOutputTransitionWith(Symbol symbol) {
		return this.findOutputTransition(symbol)!=null;
	}
}
