package edu.uclm.esi.regexpbuilder.model.re;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import edu.uclm.esi.regexpbuilder.model.equations.Equation;
import edu.uclm.esi.regexpbuilder.model.equations.RMCounter;

public class Symbol extends RightMember implements ILabel {
	private String symbol;
	private boolean separated; 
	
	public Symbol(String symbol) {
		this.symbol = symbol;
	}
	
	@Override
	public String toString() {
		if (this.separated)
			return symbol + " ";
		return symbol;
	}

	public boolean getSeparated() {
		return separated;
	}

	public void setSeparated(boolean separated) {
		this.separated = separated;
	}

	@Override
	public int length() {
		return 1;
	}
	
	@Override
	public RightMember copy() {
		Symbol symbol = new Symbol(this.symbol);
		symbol.separated = this.separated;
		return symbol;
	}

	@Override
	protected int depth() {
		return 0;
	}

	@Override
	public void loadSubTrees(Map<String, RMCounter> subarboles, int weight) {
		if (weight<=1) {
			String key = this.toString();
			RMCounter rmc = subarboles.get(key);
			if (rmc==null)
				rmc = new RMCounter(this);
			subarboles.put(this.toString(), rmc);
			rmc.inc();
		}
	}

	@Override
	public RightMember removeState(Equation equation, State state) {
		return this;
	}

	@Override
	public RightMember replaceState(Equation equation, Equation replacementEq) {
		return this.copy();
	}

	@Override
	public List<State> getStatesUsed() {
		return new ArrayList<>();
	}

	@Override
	public boolean contains(RightMember rightMember) {
		return rightMember.toString().equals(this.toString());
	}

	@Override
	public RightMember trimUntil(Concat node) {
		return this;
	}

	@Override
	public RightMember simplify() {
		return this;
	}
	
	@Override
	public boolean completelyContains(RightMember rightMember) {
		if (rightMember.getClass()==this.getClass() && rightMember.toString().equals(this.toString()))
			return true;
		return false;
	}
	
	@Override
	public String polish() {
		return this.symbol;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (separated ? 1231 : 1237);
		result = prime * result + ((symbol == null) ? 0 : symbol.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Symbol other = (Symbol) obj;
		if (separated != other.separated)
			return false;
		if (symbol == null) {
			if (other.symbol != null)
				return false;
		} else if (!symbol.equals(other.symbol))
			return false;
		return true;
	}

	@Override
	public Symbol loadSubTree(int currentLength) {
		return currentLength==1 ? this : null;
	}
	
	@Override
	public boolean produces(RightMember rm) {
		return rm.equals(this);
	}
}
