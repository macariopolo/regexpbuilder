package edu.uclm.esi.regexpbuilder.model;

import edu.uclm.esi.regexpbuilder.model.re.State;
import edu.uclm.esi.regexpbuilder.web.ws.WSServerRegExp;

public class ManagedRemover extends ManagedThread {

	@Override
	public void run() {
		Automata automata = (Automata) this.manager;
		
		IProcessable processable;
		while ((processable=this.manager.giveMeNext())!=null) {
			State state = (State) processable.getObject();
			if (state.isRemovable()) {
				WSServerRegExp.sendSubprogress(session, id, "Removing state " + state, 0.25);
				automata.remove(state);
			}
		}
		WSServerRegExp.sendSubprogress(session, id, "idle", 1);
	}
}
