package edu.uclm.esi.regexpbuilder.model.re;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import edu.uclm.esi.regexpbuilder.model.equations.Equation;
import edu.uclm.esi.regexpbuilder.model.equations.RMCounter;

public class Concat extends RightMember implements ILabel {
	private RightMember left;
	private RightMember right;

	public Concat() {
	}

	public Concat(List<Symbol> symbols) {
		if (symbols.size()==2) {
			this.left = symbols.get(0);
			this.right = symbols.get(1);
		} else {
			this.right = symbols.get(symbols.size()-1);
			symbols.remove(symbols.size()-1);
			this.left = new Concat(symbols);
		}
	}
	
	public Concat(RightMember left, RightMember right) {
		this.left = left;
		this.right = right;
		if (left!=null)
			this.left.setParent(this);
		if (right!=null)
			this.right.setParent(this);
	}
	
	@Override
	public String toString() {
		if (left!=null && right!=null) {
			String s = this.left.toString();
			if (this.left instanceof RightMemberList)
				s = "(" + s + ")";
			if (this.right instanceof RightMemberList)
				s = s + "(" + this.right.toString() + ")";
			else
				s = s + this.right.toString();
			return s;
		}
		if (left!=null)
			return left.toString();
		if (right!=null)
			return right.toString();
		return "";
	}

	@Override
	public int length() {
		return this.left.length() + this.right.length();
	}

	public RightMember getRight() {
		return this.right;
	}

	public void setChild(RightMember child) {
		if (this.left==null)
			this.left = child;
		else if (this.right == null)
			this.right = child;
	}

	public RightMember getLeft() {
		return this.left;
	}

	public void setLeft(RightMember left) {
		this.left = left;
	}
	
	public void setRight(RightMember right) {
		this.right = right;
	}

	@Override
	public RightMember copy() {
		RightMember newLeft = this.left!=null ? this.left.copy() : null;
		RightMember newRight = this.right!=null ? this.right.copy() : null;
		return new Concat(newLeft, newRight); 
	}
	
	@Override
	public void loadSubTrees(Map<String, RMCounter> subarboles, int weight) {
		int descendants = 1 + left.descendants();
		if (descendants==weight) 
			put(subarboles, left);
		else if (descendants>weight)
			this.left.loadSubTrees(subarboles, weight);
		descendants = 1 + right.descendants();
		if (descendants==weight) 
			put(subarboles, right);
		else if (descendants>weight)
			this.right.loadSubTrees(subarboles, weight);
	}

	private void put(Map<String, RMCounter> subarboles, RightMember rm) {
		String key = rm.toString();
		RMCounter rmc = subarboles.get(key);
		if (rmc==null) {
			rmc = new RMCounter(rm);
			subarboles.put(key, rmc);
		}
		rmc.inc();
	}

	@Override
	protected int depth() {
		int dl = this.left.depth();
		int dr = this.right.depth();
		return 1 + Math.max(dl, dr);
	}
	
	@Override
	protected int descendants() {
		int ld = this.left.descendants();
		int rd = this.right.descendants();
		return 2 + ld + rd;
	}
	
	@Override
	public RightMember removeState(Equation equation, State state) {
		RightMember result;
		if (this.left.toString().equals(state.toString())) {
			result = this.right;
		} else if (this.right.toString().equals(state.toString())) {
			result = this.left;
		} else {
			RightMember newLeft = this.left.removeState(equation, state);
			RightMember newRight = this.right.removeState(equation, state);
			result = new Concat(newLeft, newRight);
		}
		return result;
	}

	@Override
	public RightMember replaceState(Equation originalEquation, Equation replacementEquation) {
		Concat concat = new Concat(this.left.copy(), this.right.copy());
		if (concat.left.contains(replacementEquation.getLeftMember())) 
			concat.left = concat.left.replaceState(originalEquation, replacementEquation);
		if (concat.right.contains(replacementEquation.getLeftMember()))
			concat.right = concat.right.replaceState(originalEquation, replacementEquation);
		concat.left.setParent(concat);
		concat.right.setParent(concat);
		return concat;
	}

	@Override
	public List<State> getStatesUsed() {
		List<State> useds = this.left.getStatesUsed();
		List<State> result = new ArrayList<>();
		for (int i=0; i<useds.size(); i++) {
			State used = useds.get(i);
			if (!result.contains(used))
				result.add(used);
		}
		useds = this.right.getStatesUsed();
		for (int i=0; i<useds.size(); i++) {
			State used = useds.get(i);
			if (!result.contains(used))
				result.add(used);
		}
		return result;
	}

	@Override
	public boolean contains(RightMember rightMember) {
		if (this.left==null)
			return this.right.contains(rightMember);
		if (this.right==null)
			return this.left.contains(rightMember);
		//if (this.left.toString().equals(rightMember.toString()) || this.right.toString().equals(rightMember.toString()))
		return this.left.contains(rightMember) || this.right.contains(rightMember);
	}

	@Override
	public RightMember getDistributivableNode() {
		RightMember dn = this.left.getDistributivableNode();
		if (dn!=null)
			return dn;
		return this.right.getDistributivableNode();
	}

	@Override
	public RightMember trimUntil(Concat node) {
		if (this.left==node) {
			Concat copy = (Concat) this.copy();
			copy.setLeft(null);
			return copy.getRoot();
		}
		if (this.right==node) {
			Concat copy = (Concat) this.copy();
			copy.setRight(null);
			return copy.getRoot();
		}
		RightMember newLeft = this.left.trimUntil(node);
		RightMember newRight = this.right.trimUntil(node);
		return new Concat(newLeft, newRight);
	}

	@Override
	public void addToNull(RightMember rightMember, boolean byLeft) {
		if (byLeft && this.left==null)
			this.left = rightMember;
		else if (!byLeft && this.right==null)
			this.right = rightMember;
		else {
			this.left.addToNull(rightMember, byLeft);
			this.right.addToNull(rightMember, byLeft);
		}
	}

	@Override
	public RightMember simplify() {
		this.left = this.left.simplify();
		this.right = this.right.simplify();
		if (this.right instanceof Closure) {
			Closure cr = (Closure) this.right;
			String sc = cr.getClosured().toString();
			if (sc.equals(this.left.toString()) && cr.getOperator()=='*') {
				cr.setOperator('+');
				return cr;
			}
			if (this.left instanceof Concat) {
				Concat co = (Concat) this.left;
				if (co.getRight().equals(sc) && cr.getOperator()=='*') {
					this.left = co.getLeft();
					cr.setOperator('+');
					return this;
				}
			}
		}
		if (this.left instanceof Closure) {
			Closure cr = (Closure) this.left;
			String sc = cr.getClosured().toString();
			if (sc.equals(this.right.toString()) && cr.getOperator()=='*') {
				cr.setOperator('+');
				return cr;
			}
			if (this.right instanceof Concat) {
				Concat co = (Concat) this.right;
				if (co.getLeft().toString().equals(sc) && cr.getOperator()=='*') {
					this.right = co.getRight();
					cr.setOperator('+');
					return this;
				}
			}
		}
		return this;
	}
	
	@Override
	public RightMember sort() {
		this.left = this.left.sort();
		this.right = this.right.sort();
		return this;
	}
	
	@Override
	public boolean completelyContains(RightMember rightMember) {
		return this.equals(rightMember);
	}

	@Override
	public String polish() {
		return ".(" + this.left.polish() + "," + this.right.polish() + ")";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((left == null) ? 0 : left.hashCode());
		result = prime * result + ((right == null) ? 0 : right.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Concat other = (Concat) obj;
		if (left == null) {
			if (other.left != null)
				return false;
		} else if (!left.equals(other.left))
			return false;
		if (right == null) {
			if (other.right != null)
				return false;
		} else if (!right.equals(other.right))
			return false;
		return true;
	}

	public static Concat build(String symbols) {
		Symbol a = new Symbol("" + symbols.charAt(0));
		Symbol b = new Symbol("" + symbols.charAt(1));
		Concat result = new Concat(a, b);
		for (int i=2; i<symbols.length(); i++) {
			Symbol c = new Symbol("" + symbols.charAt(i));
			result = new Concat(result, c);
		}
		return result;
	}
	
	private Concat getDescendienteIzdo(int nivel) {
		if (this.depth()==nivel)
			return this;
		if (this.left instanceof Concat) {
			Concat hijoIzdo = (Concat) this.left;
			if (nivel==0)
				return this;
			if (nivel==1)
				return hijoIzdo;
			return hijoIzdo.getDescendienteIzdo(nivel-1);
		}
		return null;
	}

	@Override
	public RightMember loadSubTree(int currentLength) {
		if (currentLength==0)
			return null;
		Concat copy = (Concat) this.copy();
		if (currentLength==1)
			return copy.right;
		if (currentLength==2) {
			Concat child = (Concat) copy.left;
			copy.setLeft(child.getRight());
			return copy;
		}
		Concat child = copy.getDescendienteIzdo(currentLength-1);
		Concat cLeft = (Concat) child.left;
		child.setLeft(cLeft.getRight());
		return copy;
	}
	
	@Override
	public boolean produces(RightMember rm) {
		if (rm instanceof Symbol || rm instanceof State || rm instanceof Closure) {
			return false;
		}
		if (rm instanceof Concat) {
			Concat cRM = (Concat) rm;
			boolean result = this.left.produces(cRM.left);
			result = result && this.right.produces(cRM.right);
			return result;
		}
		return false;
	}
}
