package edu.uclm.esi.regexpbuilder.model.re;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import edu.uclm.esi.regexpbuilder.model.equations.Equation;
import edu.uclm.esi.regexpbuilder.model.equations.RMCounter;

public abstract class RightMember {
	private RightMember parent;
	
	public void setParent(RightMember parent) {
		this.parent = parent;
	}
	
	public RightMember getParent() {
		return parent;
	}
	
	public RightMember getRoot() {
		if (parent==null)
			return this;
		return parent.getRoot();
	}
	
	public abstract RightMember copy(); 

	@Override
	public abstract String toString();

	public abstract int length();

	protected abstract int depth();

	public List<RightMember> flatten() {
		ArrayList<RightMember> list = new ArrayList<>();
		list.add(this);
		return list;
	}
	
	public abstract void loadSubTrees(Map<String, RMCounter> subarboles, int weight);

	protected int descendants() {
		return 0;
	}

	public abstract RightMember removeState(Equation equation, State state);

	public abstract RightMember replaceState(Equation equation, Equation replacementEq);

	public abstract List<State> getStatesUsed();

	public abstract boolean contains(RightMember rightMember);

	public RightMember getDistributivableNode() {
		return null;
	}

	public RightMemberList applyDistributive() {
		RightMemberList rml = new RightMemberList();
		rml.add(this.copy());
		return rml;
	}

	public abstract RightMember trimUntil(Concat node);

	public void addToNull(RightMember rm, boolean byLeft) {
	}
	
	public abstract RightMember simplify();

	public RightMember sort() {
		return this;
	}
	
	public abstract boolean completelyContains(RightMember rightMember);

	public abstract String polish();
	
	@Override
	public abstract boolean equals(Object obj);
	
	@Override
	public abstract int hashCode();

	public abstract RightMember loadSubTree(int currentLength);

	public abstract boolean produces(RightMember rm);
}
