package edu.uclm.esi.regexpbuilder.model;

public interface IProcessable {
	IProcessable getObject();
	int getIndex();
	void setIndex(int index);
}
