package edu.uclm.esi.regexpbuilder.model.equations;

import java.util.ArrayList;
import java.util.List;

import edu.uclm.esi.regexpbuilder.model.SessionManager;
import edu.uclm.esi.regexpbuilder.model.re.Closure;
import edu.uclm.esi.regexpbuilder.model.re.Concat;
import edu.uclm.esi.regexpbuilder.model.re.RightMember;
import edu.uclm.esi.regexpbuilder.model.re.RightMemberList;
import edu.uclm.esi.regexpbuilder.model.re.State;

public class Equation {
	private EquationSystem system;
	private State leftMember;
	private RightMemberList rightMembers;
	private List<Integer> statesUsed;
	private List<Integer> replacedStates;
	private boolean separated;
	
	public Equation(EquationSystem equationSystem, boolean separated) {
		this.system = equationSystem;
		this.separated = separated;
		this.rightMembers = new RightMemberList();
		this.statesUsed = new ArrayList<>();
		this.replacedStates = new ArrayList<>();
	}
	
	public void addStateUsed(Integer stateId) {
		this.statesUsed.add(stateId);
	}

	public void setLeftMember(State state) {
		this.leftMember = state;
	}

	public boolean isInitial() {
		return this.leftMember.isInitial();
	}

	public boolean isFinal() {
		return this.leftMember.isFinal();
	}

	public Integer getId() {
		return this.leftMember.getId();
	}

	public void addRightMember(RightMember rightMember) {
		this.rightMembers.add(rightMember);
	}
	
	public RightMemberList getRightMembers() {
		return rightMembers;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(this.leftMember.toString() + " = ");
		for (RightMember rightMember : this.rightMembers)
			sb.append(rightMember.toString() + " | ");
		String r = sb.toString();
		if (r.endsWith(" | "))
			r = r.substring(0, r.length()-3);
		return r;
	}

	public int length() {
		int length = 0;
		for (RightMember rightMember : this.rightMembers)
			length = length + rightMember.length();
		return length;
	}
	
	public int getNumberOfStatesUsed() {
		return this.statesUsed.size();
	}

	public State getLeftMember() {
		return leftMember;
	}

	public int getTimesUsed() {
		return this.leftMember.getTimesUsed();
	}
	
	public void removeStates() {
		State state;
		for (int i=this.statesUsed.size()-1; i>=0; i--) {
			state = this.system.findEquation(this.statesUsed.get(i)).getLeftMember();
			this.removeState(state);
		}
	}

	public void removeState(State state) {
		for (int i=0; i<this.rightMembers.size(); i++) {
			RightMember rightMember = this.rightMembers.get(i);
			RightMember newRightMember = rightMember.removeState(this, state);
			this.rightMembers.set(i, newRightMember);
		}
		this.recalculateUsedStates();
	}

	public void replaceStateInFinalEquation(Integer stateId) {
		if (stateId.intValue()!=this.getId().intValue()) {
			Equation replacementEq = this.system.findEquation(stateId);
			for (int i=this.rightMembers.size()-1; i>=0; i--) {
				RightMember rightMember = this.rightMembers.get(i);
				RightMember newRightMember = rightMember.replaceState(this, replacementEq);
				this.rightMembers.remove(i);
				this.rightMembers.add(i, newRightMember);
			}
			this.recalculateUsedStates();
		}
	}
	
	public void finalReplacementOfUsedStatesInFinalEquation(SessionManager sessionManager) {
		sessionManager.sendProgress("Developing final equation " + this.getLeftMember());
		int n = this.statesUsed.size();
		for (int i=n-1; i>=0; i--) {
			Integer stateUsed = this.statesUsed.get(i);
			if (!this.replacedStates.contains(stateUsed) && this.leftMember.getId()!=stateUsed.intValue()) {
				this.replaceStateInOtherEquation(stateUsed);
				this.replacedStates.add(stateUsed);
			}
		}
	}

	public void replaceUsedStatesInFinalEquation(SessionManager sessionManager) {
		sessionManager.sendSubprogress(-1, "Working on equation " + this.leftMember, 0.50);
		recalculateUsedStates();
		if (this.onlyDependsOnTerminalsAndFinals()) {
			for (int i=this.statesUsed.size()-1; i>=0; i--) {
				Integer stateUsed = this.statesUsed.get(i);
				this.replaceStateInFinalEquation(stateUsed);
				this.replacedStates.add(stateUsed);
			}
		} else {
			this.replaceUsedStatesInOtherEquation(sessionManager);
		}
	}
	
	public void replaceStateInOtherEquation(Integer stateId) {
		Equation replacementEq = this.system.findEquation(stateId);		
		for (int i=this.rightMembers.size()-1; i>=0; i--) {
			RightMember rightMember = this.rightMembers.get(i);
			if (rightMember.contains(replacementEq.getLeftMember())) {
				RightMember newRightMember = rightMember.replaceState(this, replacementEq);
				this.rightMembers.remove(i);
				this.rightMembers.add(i, newRightMember);
			}
		}
		this.recalculateUsedStates();
	}

	public void replaceStatesUsed(int times) {
		for (int i=0; i<times; i++) {
			for (int j=0; j<this.statesUsed.size(); j++) {
				Integer stateUsedId = this.statesUsed.get(j);
				this.replaceStateInOtherEquation(stateUsedId);
			}
		}
	}
	
	public void replaceUsedStatesInOtherEquation(SessionManager sessionManager) {
		sessionManager.sendSubprogress(-1, "Working on equation " + this.leftMember, 0.50);
		recalculateUsedStates();
		int n = this.statesUsed.size();
		for (int i=n-1; i>=0; i--) {
			Integer stateUsed = this.statesUsed.get(i);
			if (this.system.isFinalEquation(stateUsed)) {
				this.replacedStates.add(stateUsed);
				continue;
			}
			if (!this.replacedStates.contains(stateUsed) && this.leftMember.getId()!=stateUsed.intValue()) {
				this.replaceStateInOtherEquation(stateUsed);
				this.replacedStates.add(stateUsed);
			}
		}
	}
	
	public boolean onlyDependsOnTerminalsAndFinals() {
		for (int i=0; i<this.statesUsed.size(); i++)
			if (!this.system.findEquation(this.statesUsed.get(i)).isFinal())
				return false;
		return true;
	}

	public void recalculateUsedStates() {
		this.statesUsed.clear();
		for (RightMember rightMember : this.rightMembers) {
			List<State> states = rightMember.getStatesUsed();
			for (State usedState : states)
				if (!this.statesUsed.contains(usedState.getId()))
					this.statesUsed.add(usedState.getId());
		}
	}

	public boolean allUsedStatesHaveBeenReplaced() {
		for (Integer usedStateId : this.statesUsed) {
			if (this.getLeftMember().getId()==usedStateId)
				continue;
			if (!this.replacedStates.contains(usedStateId))
				return false;
		}
		return true;
	}

	public List<Integer> getStatesUsed() {
		return this.statesUsed;
	}

	public List<Integer> getReplacedStates() {
		return this.replacedStates;
	}

	public void ardens(SessionManager sessionManager) {
		if (ardensApplicable()) {
			sessionManager.sendProgress("Applying distributive to equation " + this.getLeftMember());
			applyDistributiveToEquation();
			sessionManager.sendProgress("Applying Ardens to equation " + this.getLeftMember());
			List<RightMember> Q = new ArrayList<>();
			List<RightMember> P = new ArrayList<>();
			for (RightMember rightMember : this.rightMembers) {
				if (!rightMember.contains(this.leftMember)) {
					Q.add(rightMember);
				} else {
					RightMember newRightMember = rightMember.removeState(this, this.leftMember);
					P.add(newRightMember);
				}
			}
			RightMemberList Q2 = new RightMemberList(Q);
			Closure P2 = new Closure(new RightMemberList(P));
			this.rightMembers.clear();
			if (Q2.size()>0) {
				Concat concat = new Concat(Q2, P2);
				this.rightMembers.add(concat);
			} else {
				this.rightMembers.add(P2);
			}
		}
	}

	void applyDistributiveToEquation() {
		int size = this.rightMembers.size()-1;
		for (int i=size; i>=0; i--) {
			RightMember rightMember = this.rightMembers.get(i);
			RightMember distributableNode = rightMember.getDistributivableNode();
			if (distributableNode!=null) {
				RightMemberList rml = distributableNode.applyDistributive();
				this.rightMembers.remove(i);
				this.rightMembers.add(i, rml);
			}
		}
	}

	private boolean ardensApplicable() {
		if (this.statesUsed.isEmpty())
			return false;
		if (this.isFinal() && this.onlyDependsOnTerminalsAndFinals() && !this.statesUsed.contains(this.leftMember.getId()))
			return false;
		for (Integer stateUsedId : this.statesUsed) {
			Equation usedEquation = this.system.findEquation(stateUsedId);
			if (!usedEquation.isFinal() && this.leftMember.getId()!=stateUsedId)
				return false;
		}
		return true;
	}

	public List<Integer> getStatesUsedDifferentThanMe() {
		List<Integer> statesUsed = this.statesUsed;
		statesUsed.remove(this.getId());
		return statesUsed;
	}

	public void simplify() {
		this.rightMembers = (RightMemberList) this.rightMembers.sort();
		this.rightMembers = (RightMemberList) this.rightMembers.simplify();
	}

	public String polish() {
		return this.rightMembers.polish();
	}
}