package edu.uclm.esi.regexpbuilder.model.minimizer;

import java.util.List;
import java.util.Map;
import java.util.Vector;

import edu.uclm.esi.regexpbuilder.model.REThread;
import edu.uclm.esi.regexpbuilder.web.ws.WSServerRegExp;

public class Searcher extends REThread {
	private Map<String, List<edu.uclm.esi.regexpbuilder.model.re.State>> equivalentStates;
	private Vector<String> keysWithEquivalent;

	public Searcher(int id, int start, int end, Object[] objectsToProcess) {
		super(id, start, end, objectsToProcess);
		this.keysWithEquivalent = new Vector<>();
	}

	public void findIn(Map<String, List<edu.uclm.esi.regexpbuilder.model.re.State>> equivalentStates) {
		this.equivalentStates = equivalentStates;
	}
	
	@Override
	public synchronized void start() {
		String key;
		List<edu.uclm.esi.regexpbuilder.model.re.State> statesList;
		for (int i=start; i<end; i++) {
			edu.uclm.esi.regexpbuilder.model.re.State state = (edu.uclm.esi.regexpbuilder.model.re.State) this.objectsToProcess[i];
			WSServerRegExp.sendSubprogress(session, id, "Searching equivalent states: " + i + "/" + end, 0.15);
			key = state.key();
			statesList = equivalentStates.get(key);
			if (statesList==null) {
				statesList = new Vector<>();
				statesList.add(state);
				equivalentStates.put(key, statesList);
			} else {
				statesList.add(state);
				if (!keysWithEquivalent.contains(key))
					keysWithEquivalent.add(key);
			}
		}
		WSServerRegExp.sendSubprogress(session, id, "idle", 1);
	}
	
	public List<String> getKeysWithEquivalent() {
		return keysWithEquivalent;
	}
}
