package edu.uclm.esi.regexpbuilder.model.re;

import java.lang.reflect.Method;

@SuppressWarnings("unused")
public class Find {
	
	public static RightMember find(RightMember pattern, RightMember rm) {
		if (pattern==null || rm==null)
			return null;
		RightMember result = null;
		Class<?> aClass = pattern.getClass();
		Class<?> bClass = rm.getClass();
		String methodName = "find" + aClass.getSimpleName() + "In" + bClass.getSimpleName();
		try {
			Method method = Find.class.getDeclaredMethod(methodName, aClass, bClass);
			result = (RightMember) method.invoke(null, pattern, rm);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	private static RightMember findSymbolInSymbol(Symbol pattern, Symbol symbol) {
		if (pattern.equals(symbol))
			return pattern;
		return null;
	}
	
	private static RightMember findSymbolInStat(Symbol pattern, State state) {
		return null;
	}
	
	private static RightMember findSymbolInConcat(Symbol pattern, Concat concat) {
		RightMember result = find(pattern, concat.getLeft());
		if (result!=null)
			return result;
		return Find.find(pattern, concat.getRight());
	}
	
	private static RightMember findSymbolInClosure(Symbol pattern, Closure closure) {
		return find(pattern, closure.getClosured());
	}
	
	private static RightMember findSymbolInRightMemberList(Symbol pattern, RightMemberList rml) {
		for (RightMember rm: rml) {
			RightMember result = find(pattern, rm);
			if (result!=null)
				return result;
		}
		return null;
	}
	
	private static RightMember findStateInSymbol(State pattern, Symbol symbol) {
		return null;
	}
	
	private static RightMember findStateInStat(State pattern, State state) {
		if (pattern.equals(state))
			return pattern;
		return null;
	}
	
	private static RightMember findStateInConcat(State pattern, Concat concat) {
		RightMember result = find(pattern, concat.getLeft());
		if (result!=null)
			return result;
		return Find.find(pattern, concat.getRight());
	}
	
	private static RightMember findStateInClosure(State pattern, Closure closure) {
		return find(pattern, closure.getClosured());
	}
	
	private static RightMember findStateInRightMemberList(State pattern, RightMemberList rml) {
		for (RightMember rm: rml) {
			RightMember result = find(pattern, rm);
			if (result!=null)
				return result;
		}
		return null;
	}
	
	private static RightMember findConcatInSymbol(Concat pattern, Symbol symbol) {
		return null;
	}
	
	private static RightMember findConcatInState(Concat pattern, State state) {
		return null;
	}
	
	private static RightMember findConcatInConcat(Concat pattern, Concat concat) {
		if (pattern.equals(concat))
			return pattern;
		RightMember result = Find.find(pattern, concat.getLeft());
		if (result!=null)
			return result;
		return Find.find(pattern, concat.getRight());
	}
	
	private static RightMember findConcatInClosure(Concat pattern, Closure closure) {
		return find(pattern, closure.getClosured());
	}
	
	private static RightMember findConcatInRightMemberList(Concat pattern, RightMemberList rml) {
		for (RightMember rm: rml) {
			RightMember result = find(pattern, rm);
			if (result!=null)
				return result;
		}
		return null;
	}
	
	private static RightMember findClosureInSymbol(Closure pattern, Symbol symbol) {
		return null;
	}
	
	private static RightMember findClosureInState(Closure pattern, State state) {
		return null;
	}
	
	private static RightMember findClosureInConcat(Closure pattern, Concat concat) {
		RightMember result = find(pattern, concat.getLeft());
		if (result!=null)
			return result;
		return find(pattern, concat.getRight());
	}
	
	private static RightMember findClosureInClosure(Closure pattern, Closure closure) {
		if (pattern.equals(closure))
			return pattern;
		return find(pattern, closure.getClosured());
	}
	
	private static RightMember findClosureInRightMemberList(Closure pattern, RightMemberList rml) {
		for (RightMember rm: rml) {
			RightMember result = find(pattern, rm);
			if (result!=null)
				return result;
		}
		return null;
	}
	
	private RightMember findRightMemberListInSymbol(RightMemberList pattern, Symbol symbol) {
		return null;
	}
	
	private RightMember findRightMemberListInState(RightMemberList pattern, State state) {
		return null;
	}
	
	private RightMember findRightMemberListInConcat(RightMemberList pattern, Concat concat) {
		RightMember result = find(pattern, concat.getLeft());
		if (result!=null)
			return result;
		return find(pattern, concat.getRight());
	}
	
	private RightMember findRightMemberListInClosure(RightMemberList pattern, Closure closure) {
		return find(pattern, closure.getClosured());
	}
	
	private RightMember findRightMemberListInRightMemberList(RightMemberList pattern, RightMemberList rml) {
		if (pattern.equals(rml))
			return pattern;
		for (RightMember rm: rml) {
			RightMember result = find(pattern, rm);
			if (result!=null)
				return result;
		}
		return null;
	}
}
