package edu.uclm.esi.regexpbuilder.model;

import java.util.Comparator;

public class SequencesFrequencySorter implements Comparator<Sequence> {

	@Override
	public int compare(Sequence s1, Sequence s2) {
		return s2.getFrequency()-s1.getFrequency();
	}

}
