package edu.uclm.esi.regexpbuilder.model.equations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EquationSystem {
	private Equation initialEquation;
	private List<Equation> equations;
	private Map<Integer, Equation> equationsMap;
	private List<Equation> finalEquations;
	
	public EquationSystem() {
		this.equations = new ArrayList<>();
		this.equationsMap = new HashMap<>();
		this.finalEquations = new ArrayList<>();
	}

	public void add(Equation equation) {
		if (equation.isInitial())
			this.initialEquation=equation;
		if (equation.isFinal())
			this.finalEquations.add(equation);
		this.equations.add(equation);
		this.equationsMap.put(equation.getId(), equation);
	}

	public void solve(EquationsSolver solver) {
		solver.setEquationSystem(this);
		solver.solve();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		Collection<Equation> equations = this.equationsMap.values();
		for (Equation eq : equations)
			sb.append(eq.toString()).append("\n");
		return sb.append("\n").toString();
	}
	
	public Equation findEquation(int stateId) {
		return this.equationsMap.get(stateId);
	}
	
	public Map<Integer, Equation> getEquationsMap() {
		return this.equationsMap;
	}
	
	public List<Equation> getEquations() {
		return this.equations;
	}
	
	public List<Equation> getFinalEquations() {
		return finalEquations;
	}

	public Equation getInitialEquation() {
		return this.initialEquation;
	}

	public void remove(Equation equation) {
		this.equationsMap.remove(equation.getId());
		this.equations.remove(equation);
	}

	public boolean isFinalEquation(Integer stateId) {
		return this.findEquation(stateId).isFinal();
	}
}
