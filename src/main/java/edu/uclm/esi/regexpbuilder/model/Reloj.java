package edu.uclm.esi.regexpbuilder.model;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.web.socket.WebSocketSession;

import edu.uclm.esi.regexpbuilder.web.ws.WSServerRegExp;

public class Reloj {
	public static final String SORTING = "Sorting";
	public static final String ADD_SEQUENCES_TO_AUTOMATON = "Add sequences to automaton";
	public static final String MINIMIZE_AUTOMATA = "Automata minimizing";
	public static final String BUILD_EQUATIONS = "Equations building";
	public static final String SOLVING = "Solving";
	public static final String GROUPING_TRANSITIONS = "Grouping transitions";
	public static final String ZIPPING_TRANSITIONS = "Zipping transitions";
	public static final String COPYING_AUTOMATON = "Automaton copying";
	public static final String REMOVING_EMPTY_STATES = "Empty states removal in equations";
	public static final String REMOVING_LAMBDAS = "Lambdas removal in equations";
	public static final String DEVELOPMENT_OF_FINAL_EQUATIONS = "Development of final equations";
	public static final String DEVELOPMENT_OF_OTHER_EQUATIONS = "Development of other equations";
	public static final String ARDENS_TO_OTHER_EQUATIONS = "Arden's to other equations";
	public static final String ARDENS_TO_FINAL_EQUATIONS = "Arden's to final equations";
	public static final String REMOVING_UNSEFUL_STATES = "Removing unseful states";
	public static final String REENUMERATING_STATES = "States reenumeration";
	public static final String PROCESSING_INITIAL_STATE = "Pocessing initial state";
	public static final String PROCESSING_FINAL_STATES = "Pocessing final states";
	public static final String BUILDING_REGULAR_EXPRESSION = "Building regular expression";
	public static final String REMOVING_STATES = "Removing states";
			
	private Map<String, Long> times;
	private WebSocketSession session;
	
	public Reloj(WebSocketSession session) {
		this.session = session;
		this.times = new ConcurrentHashMap<>();
	}
	
	public void start(String task) {
		Long taskTime = times.get(task);
		if (taskTime==null) {
			taskTime = System.currentTimeMillis();
			this.times.put(task, taskTime);
			WSServerRegExp.sendTime(session, task, 0L);
		}
	}

	public void stop(String task) {
		Long taskTime = times.get(task);
		taskTime=System.currentTimeMillis()-taskTime;
		WSServerRegExp.sendTime(session, task, taskTime);
	}
}
