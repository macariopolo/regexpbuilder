package edu.uclm.esi.regexpbuilder.model.matrix;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import edu.uclm.esi.regexpbuilder.model.Automata;
import edu.uclm.esi.regexpbuilder.model.Reloj;
import edu.uclm.esi.regexpbuilder.model.SessionManager;
import edu.uclm.esi.regexpbuilder.model.equations.RMCounter;
import edu.uclm.esi.regexpbuilder.model.re.Closure;
import edu.uclm.esi.regexpbuilder.model.re.Concat;
import edu.uclm.esi.regexpbuilder.model.re.Lambda;
import edu.uclm.esi.regexpbuilder.model.re.RightMember;
import edu.uclm.esi.regexpbuilder.model.re.RightMemberList;

public class TransitiveClosureMatrixSolver extends MatrixSolver {

	public TransitiveClosureMatrixSolver(Automata automata, SessionManager sessionManager, Reloj reloj) {
		super(automata, sessionManager, reloj);
	}

	@Override
	public void solve() {
		int n = automata.getNumberOfStates();
		Matrix R = new Matrix(automata);
		System.out.println("----- K=0 ------");
		System.out.println(R);
		
		this.matrix = new Matrix(automata);		
		
		for (int k=1; k<n; k++) {
			for (int i=0; i<n; i++) {
				for (int j=0; j<n; j++) {
					RightMember rij = R.get(i, j);
					RightMember rik = R.get(i, k);
					RightMember rkk = R.get(k, k);
					RightMember rkj = R.get(k, j);
					
					String s;
					if (k==0)
						s = Mostrador.r(i, j, k);
					else
						s = Mostrador.print(i, j, k);
					System.out.print(s + " = ");
					RightMember newValue = compose(rij, rik, rkk, rkj, i, j, k);
					System.out.println(newValue);
					matrix.setValue(i, j, newValue);
				}
			}
			R = matrix;
			System.out.println("---- K = " + k + "-----");
			System.out.println(matrix);
		}
		System.out.println("--------------");
		System.out.println(matrix);
	}

	private RightMember compose(RightMember rij, RightMember rik, RightMember rkk, RightMember rkj, int i, int j, int k) {
		RightMember a = rij;
		RightMember b = compose(rik, rkk, rkj, i, j, k);
		
		if (a==null && b==null)
			return null;
		
		if (a==null && b!=null)
			return b;
		
		if (a!=null && b==null)
			return a;
		
		if (a instanceof Lambda && !(b instanceof Lambda))
			return b;
		
		if (!(a instanceof Lambda) && b instanceof Lambda)
			return a;
		
		if (a instanceof Lambda && b instanceof Lambda)
			return a;
		
		if (a.toString().equals(b.toString()))
			return a;
		RightMemberList result = new RightMemberList();
		result.add(a);
		result.add(b);
		return result;
	}

	private RightMember compose(RightMember rik, RightMember rkk, RightMember rkj, int i, int j, int k) {
		if (rik==null || rkk==null || rkj==null)
			return null;
		
		if (rik instanceof Lambda) {
			rkk = star(rkk, i, j, k);
			return concat(rkk, rkj);
		}
		
		rkk = star(rkk, i, j, k);
		if (rkk instanceof Lambda)
			return concat(rik, rkj);
		
		if (rkj instanceof Lambda)
			return concat(rik, rkk);
		
		Concat result = new Concat(rik, rkk);
		result = new Concat(result, rkj);
		return result;
	}

	private RightMember concat(RightMember a, RightMember b) {
		if (a instanceof Lambda)
			return b;
		if (b instanceof Lambda)
			return a;
		return new Concat(a, b);
	}

	private RightMember star(RightMember a, int i, int j, int k) {
		if (a instanceof Lambda)
			return a;
		return new Closure(a);
	}

	public static List<RMCounter> loadSubtrees(RightMember regexp) {
		HashMap<String, RMCounter> subarboles = new HashMap<>();
		regexp.loadSubTrees(subarboles, 5);
		
		Collection<RMCounter> cPatterns = subarboles.values();
		ArrayList<RMCounter> patterns = new ArrayList<>();
		for (RMCounter pattern : cPatterns)
			patterns.add(pattern);
		Collections.sort(patterns, new Comparator<RMCounter>() {
			@Override
			public int compare(RMCounter o1, RMCounter o2) {
				return o2.getN()-o1.getN();
			}			
		});
		return patterns;
	}

	public String getRegularExpression() {
		RightMember regexp = matrix.getRegularExpression();
		regexp = regexp.simplify();
		String sre = regexp.toString();
		return sre;
	}
}
