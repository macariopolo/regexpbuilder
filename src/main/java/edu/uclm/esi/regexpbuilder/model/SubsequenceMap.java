package edu.uclm.esi.regexpbuilder.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SubsequenceMap {
	private int length;
	private Map<String, Sequence> map;
	private static final SequencesFrequencySorter sequencesFrequencySorter = new SequencesFrequencySorter();
	
	public SubsequenceMap(int length) {
		this.length = length;
		this.map = new HashMap<>();
	}
	
	public int getLength() {
		return length;
	}

	public void index(Sequence sequence) {
		Sequence value = this.map.get(sequence.toString());
		if (value==null)
			value = sequence;
		value.increaseFrecuency();
		this.map.put(value.toString(), value);
	}

	public Collection<Sequence> values() {
		return this.map.values();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		Collection<Sequence> values = map.values();
		for (Sequence value : values)
			sb.append(value.toString() + "-> " + value.getFrequency() + "\n");
		return sb.toString();
	}

	public JSONArray toJSON() throws JSONException {
		JSONArray jsa = new JSONArray();
		Collection<Sequence> values = map.values();
		ArrayList<Sequence> sequences = new ArrayList<>(); 
		for (Sequence value : values)
			sequences.add(value);
		Collections.sort(sequences, sequencesFrequencySorter);
		for (Sequence sequence : sequences) {
			JSONObject jso = new JSONObject();
			jso.put("sequence", sequence.toString());
			jso.put("frequency", sequence.getFrequency());
			jsa.put(jso);
		}
		return jsa;
	}
}
