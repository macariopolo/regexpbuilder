package edu.uclm.esi.regexpbuilder.model.equations;

import java.util.Comparator;

public class EquationsSorter implements Comparator<Equation> {
	public static final int SORTHEST_FIRST = 1;
	public static final int LONGEST_FIRST = 2;
	public static final int STATES_USES_LESS_FIRST = 3;
	public static final int STATES_USES_MORE_FIRST = 4;
	public static final int TIMES_USED_LESS_FIRST = 5;
	public static final int TIMES_USED_MORE_FIRST = 6;
	
	private int mode;

	public EquationsSorter(int mode) {
		this.mode = mode;
	}

	@Override
	public int compare(Equation eq1, Equation eq2) {
		if (mode == SORTHEST_FIRST) {
			int result = eq1.length()-eq2.length();
			if (result==0)
				return eq1.getId()-eq2.getId();				
			return result;
		} else if (mode == LONGEST_FIRST) {
			return eq2.length()-eq1.length();
		} else if (mode == STATES_USES_LESS_FIRST) {
			if (eq1.getNumberOfStatesUsed()!=eq2.getNumberOfStatesUsed())
				return eq1.getNumberOfStatesUsed()-eq2.getNumberOfStatesUsed();
			return eq1.length()-eq2.length();
		} else if (mode == STATES_USES_MORE_FIRST) {
			if (eq2.getNumberOfStatesUsed()!=eq1.getNumberOfStatesUsed())
				return eq2.getNumberOfStatesUsed()-eq1.getNumberOfStatesUsed();
			return eq2.length()-eq1.length();
		} else if (mode == TIMES_USED_LESS_FIRST) {
			if (eq1.getTimesUsed()!=eq2.getTimesUsed())
				return eq1.getTimesUsed()-eq2.getTimesUsed();
			return eq1.length()-eq2.length();
		} else {
			if (eq2.getTimesUsed()!=eq1.getTimesUsed())
				return eq2.getTimesUsed()-eq1.getTimesUsed();
			return eq1.length()-eq2.length();
		}
	}

}