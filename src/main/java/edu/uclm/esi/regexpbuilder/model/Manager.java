package edu.uclm.esi.regexpbuilder.model;

import java.io.File;
import java.security.SecureRandom;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.web.socket.WebSocketSession;

public class Manager {
	private ConcurrentHashMap<String, SessionManager> managers;
	private SecureRandom dado;
	private String workingFolder;
	private String projectFolder;
	private String suaFolder;
	private String instrumentedFolder;
	private String instrumentationKind;
	
	public Manager() {
		this.managers = new ConcurrentHashMap<>();
		this.dado = new SecureRandom(); 
		this.workingFolder = System.getProperty("user.home").replace('\\', File.separatorChar);
		if (!this.workingFolder.endsWith(File.separator))
			this.workingFolder+=File.separator;
		this.workingFolder=this.workingFolder + "testimo" + File.separator;
		new File(this.workingFolder).mkdirs();
	}

	private static class StoreHolder {
		static Manager singleton=new Manager();
	}

	public static Manager get() {
		return StoreHolder.singleton;
	}

	public void add(SessionManager manager) {
		this.managers.put(manager.getSession().getId(), manager);
	}

	public SessionManager getSessionManager(String id) {
		return this.managers.get(id);
	}

	public void remove(WebSocketSession session) {
		this.managers.remove(session.getId());
	}

	public SecureRandom getDado() {
		return dado;
	}

	public String getWorkingFolder() {
		return workingFolder;
	}

	public String getSuaFolder() {
		return suaFolder;
	}

	public void setSuaFolder(String suaFolder) {
		this.suaFolder = suaFolder;
	}

	public String getInstrumentedFolder() {
		return instrumentedFolder;
	}

	public void setInstrumentedFolder(String instrumentedFolder) {
		this.instrumentedFolder = instrumentedFolder;
	}

	public String getProjectFolder() {
		return projectFolder;
	}

	public void setProjectFolder(String projectFolder) {
		this.projectFolder = projectFolder;
	}

	public void setInstrumentationKind(String instrumentationKind) {
		this.instrumentationKind = instrumentationKind;
	}

	public String getInstrumentationKind() {
		return instrumentationKind;
	}
	
}
