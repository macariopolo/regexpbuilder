package edu.uclm.esi.regexpbuilder.model.statemachines;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import edu.uclm.esi.regexpbuilder.model.Automata;
import edu.uclm.esi.regexpbuilder.model.Reloj;
import edu.uclm.esi.regexpbuilder.model.SessionManager;
import edu.uclm.esi.regexpbuilder.model.Solver;
import edu.uclm.esi.regexpbuilder.model.Transition;
import edu.uclm.esi.regexpbuilder.model.re.State;

public class TransitionsPairsSolver extends Solver {
	private TreeNode root;
	private ArrayList<IOPairTable> tables;

	public TransitionsPairsSolver(Automata automata, SessionManager sessionManager, Reloj reloj) {
		super(automata, sessionManager, reloj);
	}

	@Override
	public void solve() {
		List<IOPairTable> tables = buildTables();
		this.root = buildTree(tables);
		for (IOPairTable table : tables) {
			IOPair pair = table.findUnvisitedPair();
			if (pair!=null) {
				System.err.println("Hay un par no visitado: " + pair);
				System.exit(-1);
			}
		}
	}

	private TreeNode buildTree(List<IOPairTable> tables) {
		State currentState = this.automata.getState(0);
		TreeNode currentNode = new TreeNode(currentState);
		currentNode.addChildren(tables);
		return currentNode;
	}

	public List<IOPairTable> buildTables() {
		if (this.tables!=null)
			return this.tables;
		this.tables = new ArrayList<>();
		Collection<State> states = automata.getStates().values();
		for (State state : states) {
			IOPairTable table = buildPairTable(state);
			tables.add(table);
		}
		return tables;
	}

	private IOPairTable buildPairTable(State state) {
		IOPairTable result = new IOPairTable(state);
		List<Transition> inputs = state.getInputTransitionsList();
		List<Transition> outputs = state.getOutputTransitionsList();
		for (Transition input : inputs) {
			for (Transition output : outputs) {
				IOPair pair = new IOPair(result, input, output);
				result.add(pair);
			}
		}
		return result;
	}

	@Override
	public String getRegularExpression() {
		List<TreeNode> leaves = root.getLeaves();
		StringBuilder sb = new StringBuilder();
		for (TreeNode leaf : leaves)
			sb.append(leaf.getSequence()).append(" | ");
		String r = sb.toString();
		r = r.substring(0, r.length()-3);
		return r;
	}

}
