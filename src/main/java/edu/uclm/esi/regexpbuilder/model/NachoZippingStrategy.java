package edu.uclm.esi.regexpbuilder.model;

import java.util.ArrayList;
import java.util.List;

import edu.uclm.esi.regexpbuilder.model.re.State;
import edu.uclm.esi.regexpbuilder.web.ws.WSServerRegExp;

public class NachoZippingStrategy extends ZippingStrategy {

	public NachoZippingStrategy(ManagedTransitionZipper zipper) {
		super(zipper, zipper.getRepeticiones());
	}

	@Override
	public String zip(State state, CharSequence expresion, int depthLevel) {
		String s = expresion.toString();
		int length = s.length();
		int shortestLength = Integer.MAX_VALUE;
		String shortestExpression = s;
		
		for (int currentLength=depthLevel; currentLength<=length/repeticiones; currentLength++) {
			if (state!=null)
				WSServerRegExp.sendSubprogress(zipper.getSession(), zipper.id, "Zipping transitions of state S" + state.getId(), 1);
			for (int offset=0; offset<currentLength; offset++) {
				int start = offset;
				int end = start + currentLength;
				List<String> subcadenas = getSubstrings(s, currentLength, start, end);
				if (subcadenas.size()<repeticiones)
					break;
				
				ArrayList<Integer> starts = new ArrayList<>();
				ArrayList<Integer> ends = new ArrayList<>();
				boolean candidato = false; 
				for (int j=0; j<=subcadenas.size()-repeticiones; j++) {
					if (subcadenas.get(j).equals(subcadenas.get(j+1)) && subcadenas.get(j+1).equals(subcadenas.get(j+2))) {
						if (!candidato) {
							starts.add(j);
							ends.add(j+2);
							candidato = true;
						} else {
							ends.set(ends.size()-1, j);
						}
					} else {
						candidato = false;
					}					
				} 
				if (starts.isEmpty())
					continue;
				StringBuilder tmp = new StringBuilder();
				int currentPos = 0;
				for (int j=0; j<starts.size(); j++) {
					start = starts.get(j);
					end = ends.get(j);
					String sub = s.substring(currentPos, start); 
					if (start-currentLength>repeticiones) 
						tmp.append(zip(state, sub, depthLevel+1));
						//tmp.append(sub);
					else 
						tmp.append(sub);
					tmp.append("{" + s.substring(start, start+currentLength) + "}");
					currentPos = currentLength * (end-start+1) + offset;
				}
				tmp.append(s.substring(currentPos));
				if (tmp.length()<=shortestLength) {
					shortestLength = tmp.length();
					shortestExpression = tmp.toString();
				}
			}
		}
		return shortestExpression;
	}
	
	private List<String> getSubstrings(String s, int currentLength, int start, int end) {
		ArrayList<String> result = new ArrayList<>();
		int length = s.length();
		String substring;
		do {
			substring = s.substring(start, end);
			result.add(substring);
			//System.out.print(substring + ", ");
			start=end;
			end = start + currentLength;
		} while (end<=length);
		return result;
	}

}
