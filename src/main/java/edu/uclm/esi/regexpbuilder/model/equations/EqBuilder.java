package edu.uclm.esi.regexpbuilder.model.equations;

import java.util.ArrayList;
import java.util.Stack;

import edu.uclm.esi.regexpbuilder.model.re.Closure;
import edu.uclm.esi.regexpbuilder.model.re.Concat;
import edu.uclm.esi.regexpbuilder.model.re.RightMember;
import edu.uclm.esi.regexpbuilder.model.re.RightMemberList;
import edu.uclm.esi.regexpbuilder.model.re.State;
import edu.uclm.esi.regexpbuilder.model.re.Symbol;

public class EqBuilder {

	public static RightMember fromString(String re) {
		return getRightMember(re);
	}
	
	private static RightMember getRightMember(String re) {
		ArrayList<Integer> barras = buildBarras(re);
		if (barras.isEmpty())
			return buildRightMember(re);
		RightMemberList rml = new RightMemberList();
		int start = 0;
		String subre;
		int posBarra;
		for (int i=0; i<barras.size(); i++) {
			posBarra = barras.get(i);
			subre = re.substring(start, posBarra);
			start = posBarra + 1;
			RightMember rm = buildRightMember(subre);
			rml.add(rm);
		}
		subre = re.substring(start);
		RightMember rm = buildRightMember(subre);
		rml.add(rm);
		return rml;
	}

	private static RightMember buildRightMember(String re) {
		ArrayList<Trio> trios = buildTrios(re);
		ArrayList<Integer> barras = buildBarras(re);
		if (!barras.isEmpty()) {
			RightMemberList rml = new RightMemberList();
			RightMember rm = getRightMember(re);
			rml.add(rm);
			return rml;
		} else {
			char c = re.charAt(0);
			if (c=='.') {
				Trio trio = trios.remove(0);
				String sLeft = re.substring(trio.posParIzdo+1, trio.posComa);
				String sRight = re.substring(trio.posComa+1, trio.posParDcho);
				RightMember left = buildRightMember(sLeft);
				RightMember right = buildRightMember(sRight);
				return new Concat(left, right);
			} else if (c=='*' || c=='+') {
				Trio trio = trios.remove(0);
				String sClosured = re.substring(trio.posParIzdo+1, trio.posParDcho);
				RightMember closured = buildRightMember(sClosured);
				Closure closure = new Closure(closured);
				closure.setOperator(c);
				return closure;
			} else if (Character.isDigit(c)) {
				String token = readToken(re);
				return new State(Integer.parseInt(token));
			} else if (Character.isAlphabetic(c)) {
				String token = readToken(re);
				return new Symbol(token);
			}
		}
		return null;
	}
	
	private static ArrayList<Integer> buildBarras(String re) {
		ArrayList<Integer> barras = new ArrayList<>();
		int parsIzdos = 0, parsDchos = 0;
		for (int i=0; i<re.length(); i++) {
			char c = re.charAt(i);
			if (c=='(')
				parsIzdos++;
			else if (c==')')
				parsDchos++;
			else if (c=='|' && parsIzdos == parsDchos)
				barras.add(i);
		}
		return barras;
	}
	
	private static ArrayList<Trio> buildTrios(String re) {
		ArrayList<Trio> trios = new ArrayList<>();
		Stack<Integer> lastPos = new Stack<>();
		for (int i=0; i<re.length(); i++) {
			char c = re.charAt(i);
			if (c=='(') {
				Trio trio = new Trio();
				trio.posParIzdo = i;
				trio.posComa = -1;
				trios.add(trio);
				lastPos.push(trios.size()-1);
			} else if (c==')') {
				Trio trio = trios.get(lastPos.pop());
				trio.posParDcho = i;
			} else if (c==',') {
				Trio trio = trios.get(lastPos.peek());
				trio.posComa = i;
			}
		}
		return trios;
	}

	private static boolean isSymbol(String token) {
		if (token.length()==1 && Character.isAlphabetic(token.charAt(0)))
			return true;
		if (token.startsWith("[") && token.endsWith("]"))
			return true;
		return false;
	}

	private static boolean isNumber(String token) {
		try {
			Integer.parseInt(token);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	private static Symbol symbol(String re) {
		String token;
		int posComa = re.indexOf(',', 1);
		if (posComa!=-1)
			token = re.substring(0, posComa);
		else
			token = re.substring(0);
		return new Symbol(token);
	}

	private static State state(String re) {
		String token;
		int posComa = re.indexOf(',', 1);
		if (posComa!=-1)
			token = re.substring(0, posComa);
		else
			token = re.substring(0);
		return new State(Integer.parseInt(token));
	}

	private static String readToken(String re) {
		int posParDcho = re.indexOf(')', 1);
		int posComa = re.indexOf(',', 1);
		String token;
		if (posParDcho!=-1 && posParDcho<posComa)
			token = re.substring(0, posParDcho);
		else if (posComa!=-1) { 
			token = re.substring(0, posComa);
		} else {
			token = re;
		}
		return token;
	}
	
	private static State leftState(String left) {
		if (left.endsWith(">"))
			left = left.substring(0, left.length()-1);
		return new State(Integer.parseInt(left.trim().substring(1)));
	}
	
	public static void main(String[] args) {
		//String s = "S1=S1(A(AB)*)*CD|S4ACD";
		String s1 = "concat(concat(concat(state(1),closure(concat(symbol(A),closure(concat(symbol(A),symbol(B)))))),symbol(C)),symbol(D))";
		String s2 = "concat(concat(concat(state(4),symbol(A)),symbol(C)),symbol(D))";
		String s3 = ".(.(.(4,+(A)),B),*(.(C,D)))";
		String s4 = ".(.(.(B,E),F),*(.(.(.(.(.(B,E),*(.(C,E)|.(.(.(T,E),F),E))),C),E),F)|.(.(.(.(.(.(.(B,E),*(.(C,E)|.(.(.(T,E),F),E))),T),E),F),E),F)|.(.(B,E),F)))";
		String s = "S1>=" + s4;
		System.out.println(fromString(s));
	}
}
