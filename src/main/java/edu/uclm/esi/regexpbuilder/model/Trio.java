package edu.uclm.esi.regexpbuilder.model;

public class Trio {
	public int inicio, fin, longitud;

	public Trio(int inicio, int fin, int longitud) {
		super();
		this.inicio = inicio;
		this.fin = fin;
		this.longitud = longitud;
	}
	
	@Override
	public String toString() {
		return "[" + inicio + ", " + fin + ", " + longitud + "]";
	}
}
