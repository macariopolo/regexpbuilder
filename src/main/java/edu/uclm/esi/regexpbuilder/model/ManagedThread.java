package edu.uclm.esi.regexpbuilder.model;

import org.springframework.web.socket.WebSocketSession;

public abstract class ManagedThread implements Runnable {
	protected int id;
	protected int size;
	protected WebSocketSession session;
	protected IThreadManager manager;
	
	public void setManager(IThreadManager manager) {
		this.manager = manager;
		this.size = this.manager.size();
	}
	
	public int getSize() {
		return size;
	}

	public void setId(int id) {
		this.id = id;
	}

	public WebSocketSession getSession() {
		return session;
	}

	public void setSession(WebSocketSession session) {
		this.session = session;
	}
}
