package edu.uclm.esi.regexpbuilder.model.equations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import edu.uclm.esi.regexpbuilder.model.Automata;
import edu.uclm.esi.regexpbuilder.model.Reloj;
import edu.uclm.esi.regexpbuilder.model.SessionManager;
import edu.uclm.esi.regexpbuilder.model.Solver;

public class EquationsSolver extends Solver {
	protected EquationSystem equationSystem;
	protected Map<Integer, Equation> equationsMap;
	private String regularExpression;
	
	public EquationsSolver(Automata automata, SessionManager sessionManager, Reloj reloj) {
		super(automata, sessionManager, reloj);
		automata.buildEquations();
		this.equationSystem = automata.getEquationSystem();
	}

	public void solve() {
		sessionManager.sendProgress("Sorting equations");
		reloj.start(Reloj.SORTING);
		List<Equation> sortedEquations = getSortedEquations(EquationsSorter.STATES_USES_LESS_FIRST);
		reloj.stop(Reloj.SORTING);
		
		reloj.start(Reloj.REMOVING_EMPTY_STATES);
		sessionManager.sendProgress("Removing empty states");
		Equation initialEquation = this.equationSystem.getInitialEquation();
		List<Integer> terminalStates = new ArrayList<>();
		if (initialEquation.getRightMembers().isEmpty()) {
			this.equationSystem.remove(initialEquation);
			for (Equation eq : sortedEquations) {
				sessionManager.sendSubprogress(-1, "Removing empty states in equation " + eq.getLeftMember().toString(), 0.50);
				if (eq.getStatesUsed().contains(initialEquation.getLeftMember().getId())) {
					eq.removeState(initialEquation.getLeftMember());
					if (eq.getNumberOfStatesUsed()==0)
						terminalStates.add(eq.getLeftMember().getId());
				}
			}
		}
		reloj.stop(Reloj.REMOVING_EMPTY_STATES);
		
		reloj.start(Reloj.REMOVING_LAMBDAS);
		sessionManager.sendProgress("Removing lambdas");
		boolean solved;
		do {
			solved = true;
			int terminalState;
			for (Equation eq : sortedEquations) {
				int size = terminalStates.size();
				sessionManager.sendSubprogress(-1, "Removing lambdas in equation " + eq.getLeftMember().toString(), 0.50);
				for (int i=size-1; i>=0; i--) {
					terminalState = terminalStates.get(i);
					if (eq.getStatesUsed().contains(terminalState)) {
						eq.replaceStateInOtherEquation(terminalState);
						if (eq.getNumberOfStatesUsed()==0 && !terminalStates.contains(eq.getLeftMember().getId())) {
							terminalStates.add(eq.getLeftMember().getId());
							solved = false;
						}
					}
				}
			}
		} while (!solved);
		reloj.stop(Reloj.REMOVING_LAMBDAS);
		
		reloj.start(Reloj.SORTING);
		sessionManager.sendProgress("Sorting equations");
		sortedEquations = getSortedEquations(EquationsSorter.TIMES_USED_MORE_FIRST);
		reloj.stop(Reloj.SORTING);
		
		sessionManager.sendProgress("Simplifying terminal states");
		List<Equation> finalEquations = this.equationSystem.getFinalEquations();
		int iter = 3;
		boolean developementSolved;
		do {
			developementSolved = false;
			sessionManager.sendSubprogress(-1, "Iteration " + iter++ + ": development of the " + finalEquations.size() + " existing final equations", 0.50);
			reloj.start(Reloj.DEVELOPMENT_OF_FINAL_EQUATIONS);
			developFinalEquations(finalEquations);
			reloj.stop(Reloj.DEVELOPMENT_OF_FINAL_EQUATIONS);
			
			if (finalEquationsAreSolved(finalEquations)) {
				developementSolved=true;
			} else {
				List<Equation> otherEquations = new ArrayList<>();
				reloj.start(Reloj.DEVELOPMENT_OF_OTHER_EQUATIONS);
				for (Equation eq : finalEquations) {
					for (Integer usedStateId : eq.getStatesUsed()) {
						Equation otherEquation = this.equationSystem.findEquation(usedStateId);
						if (otherEquation.getLeftMember().isFinal())
							continue;
						if (usedStateId!=eq.getLeftMember().getId() && !otherEquations.contains(otherEquation))
							otherEquations.add(otherEquation);
					}
				}
				sessionManager.sendProgress(iter + ": development of the " + otherEquations.size() + " required equations");
				developOtherEquations(otherEquations);
				reloj.stop(Reloj.DEVELOPMENT_OF_OTHER_EQUATIONS);
				
				sessionManager.sendProgress(iter + ": applying Ardens to the " + otherEquations.size() + " required equations");
				
				reloj.start(Reloj.ARDENS_TO_OTHER_EQUATIONS);
				applyArdens(otherEquations);
				reloj.stop(Reloj.ARDENS_TO_OTHER_EQUATIONS);
			}
		} while (!developementSolved);
		
		do {
			sessionManager.sendProgress(++iter + ": applying Ardens to the " + finalEquations.size() + " final equations");
			reloj.start(Reloj.ARDENS_TO_FINAL_EQUATIONS);
			applyArdens(finalEquations);
			reloj.start(Reloj.ARDENS_TO_FINAL_EQUATIONS);
			
			sessionManager.sendProgress(iter + ": developing the " + finalEquations.size() + " final equations");
			reloj.start(Reloj.DEVELOPMENT_OF_FINAL_EQUATIONS);
			finalDevelopmentOfFinalEquations(finalEquations);
			reloj.stop(Reloj.DEVELOPMENT_OF_FINAL_EQUATIONS);
		} while (!this.finalEquationsAreCompletelySolved(finalEquations));
		
		this.setRegularExpression(finalEquations, iter);
	}

	private String polish(List<Equation> finalEquations) {
		StringBuilder sb = new StringBuilder();
		for (Equation eq : finalEquations)
			sb.append(eq.polish());
		return sb.toString();
	}

	private void applyArdens(List<Equation> equations) {
		for (Equation eq : equations) {
			eq.ardens(sessionManager);
		}
	}
	
	private void finalDevelopmentOfFinalEquations(List<Equation> equations) {
		for (Equation eq : equations)
			eq.getReplacedStates().clear();
		boolean systemSolved;
		do {
			systemSolved = true;
			int equationsSolved = 0;
			for (Equation eq : equations) {
				if (eq.getStatesUsedDifferentThanMe().isEmpty())
					equationsSolved++;
				else {
					eq.finalReplacementOfUsedStatesInFinalEquation(sessionManager);
					eq.ardens(sessionManager);
				}
			}
			systemSolved = equationsSolved==equations.size();
		} while (!systemSolved);
		for (Equation eq : equations)
			eq.recalculateUsedStates();
	}

	private void developFinalEquations(List<Equation> equations) {
		for (Equation eq : equations)
			eq.getReplacedStates().clear();
		boolean systemSolved;
		do {
			systemSolved = true;
			int equationsSolved = 0;
			for (Equation eq : equations) {
				if (eq.onlyDependsOnTerminalsAndFinals() || eq.allUsedStatesHaveBeenReplaced())
					equationsSolved++;
				else {
					eq.replaceUsedStatesInFinalEquation(sessionManager);
					eq.applyDistributiveToEquation();
				}
			}
			systemSolved = equationsSolved==equations.size();
		} while (!systemSolved);
	}
	
	private void developOtherEquations(List<Equation> equations) {
		for (Equation eq : equations)
			eq.getReplacedStates().clear();
		boolean systemSolved;
		do {
			systemSolved = true;
			int equationsSolved = 0;
			for (Equation eq : equations) {
				eq.replaceUsedStatesInOtherEquation(sessionManager);
				if (eq.allUsedStatesHaveBeenReplaced())
					equationsSolved++;
			}
			systemSolved = equationsSolved==equations.size();
		} while (!systemSolved);
	}
	
	public void setEquationSystem(EquationSystem equationSystem) {
		this.equationSystem = equationSystem;
		this.equationsMap = equationSystem.getEquationsMap();
	}
	
	protected List<Equation> getSortedEquations(int mode) {
		List<Equation> sortedEquations = this.equationSystem.getEquations();
		EquationsSorter equationSorter=new EquationsSorter(mode);
		Collections.sort(sortedEquations, equationSorter);
		return sortedEquations;
	}

	public void printEquations() {
		System.out.println(equationSystem.toString());
	}

	protected void printEquations(String title, List<Equation> sortedEquations) {
		System.out.println("\n" + title);
		for (int i=0; i<title.length()-1; i++)
			System.out.print("--");
		System.out.println("-");
		for (Equation eq : sortedEquations)
			System.out.println(eq.toString());
	}
	
	@Override
	public String getRegularExpression() {
		return this.regularExpression;
	}

	private void setRegularExpression(List<Equation> finalEquations, int iter) {
		this.regularExpression = "";
		StringBuilder sb = new StringBuilder();
		for (Equation eq : finalEquations) {
			eq.simplify();
			String s = eq.getRightMembers().toString();
			sb.append(this.regularExpression + s + " | ");
		}
		this.regularExpression = sb.toString();
		if (this.regularExpression.length()>3)
			this.regularExpression = this.regularExpression.substring(0, this.regularExpression.length()-3);
	}

	private boolean finalEquationsAreSolved(List<Equation> finalEquations) {
		for (Equation eq : finalEquations)
			if (!eq.onlyDependsOnTerminalsAndFinals())
				return false;
		return true;
	}
	
	private boolean finalEquationsAreCompletelySolved(List<Equation> finalEquations) {
		for (Equation eq : finalEquations)
			if (!eq.getStatesUsed().isEmpty())
				return false;
		return true;
	}
}