package edu.uclm.esi.regexpbuilder.model.matrix;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import edu.uclm.esi.regexpbuilder.model.Automata;
import edu.uclm.esi.regexpbuilder.model.Transition;
import edu.uclm.esi.regexpbuilder.model.re.By;
import edu.uclm.esi.regexpbuilder.model.re.Closure;
import edu.uclm.esi.regexpbuilder.model.re.Lambda;
import edu.uclm.esi.regexpbuilder.model.re.Or;
import edu.uclm.esi.regexpbuilder.model.re.RightMember;
import edu.uclm.esi.regexpbuilder.model.re.RightMemberList;
import edu.uclm.esi.regexpbuilder.model.re.State;

public class Matrix {
	private int n;
	private Automata automata;
	private State[] states;
	private RightMember[][] matrix;
	
	public Matrix(int n, State[] states) {
		this.n = n;
		this.states=states;
		this.matrix = new RightMember[n][];
		for (int i=0; i<n; i++)
			this.matrix[i] = new RightMember[n];
	}

	public Matrix(Automata automata) {
		this.n = automata.getNumberOfStates();
		this.states=new State[n];
		this.matrix = new RightMember[n][];
		for (int i=0; i<n; i++)
			this.matrix[i] = new RightMember[n];
		this.automata = automata;
		for (int i=0; i<n; i++)
			this.matrix[i] = new RightMember[n];
		this.init();
	}
	
	private void init() {
		ArrayList<State> statesI = new ArrayList<>();
		ArrayList<State> statesJ = new ArrayList<>();
		Iterator<State> states = automata.getStates().values().iterator();
		int cont = 0;
		while (states.hasNext()) {
			State state = states.next();
			statesI.add(state);
			statesJ.add(state);
			this.states[cont++] = state;
		}
		for (int i=0; i<statesI.size(); i++) {
			State sI = statesI.get(i);
			for (int j=0; j<statesJ.size(); j++) {
				State sJ = statesJ.get(j);
				List<Transition> transitions = this.automata.getTransitions(sI, sJ);
				if (transitions.isEmpty() && sI.toString().equals(sJ.toString())) 
					this.matrix[i][j] = new Lambda();
				for (Transition transition : transitions) {
					RightMember pre = this.matrix[i][j];
					if (pre==null)
						this.matrix[i][j] = (RightMember) transition.getSymbol();
					else {
						RightMemberList rml = new RightMemberList();
						rml.add(pre); rml.add((RightMember) transition.getSymbol());
						this.matrix[i][j] = rml;
					}
				}
			}
		}
	}

	@Override
	public String toString() {
		RightMember rm;
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<n; i++) {
			for (int j=0; j<n; j++) {
				rm = this.matrix[i][j];
				if (rm!=null)
					sb.append(rm.toString());
				else
					sb.append("-");
				sb.append("\t");
			}
			sb.append("\n");
		}
		return sb.toString();
	}

	public RightMember get(int row, int col) {
		return this.matrix[row][col];
	}

	public void setValue(int row, int col, RightMember newValue) {
		this.matrix[row][col] = newValue;
	}

	public RightMember getRegularExpression() {
		RightMemberList result = new RightMemberList();
		for (int i=0; i<this.states.length; i++) {
			State state = this.states[i];
			if (state.isFinal())
				result.add(this.matrix[0][i]);
		}
		if (result.size()==1)
			return result.getRightMembers().get(0);
		return result;
	}
	
	public Matrix by(Matrix matrix) {
		int n = automata.getNumberOfStates();
		for (int i=0; i<n; i++)
			if (!(matrix.get(i, i) instanceof Lambda)) {
				matrix.setValue(i, i, new Closure(matrix.get(i, i)));
			}
		
		Matrix resultado = new Matrix(n, this.states);
		for (int k=0; k<n-1; k++) {
			for (int i=0; i<n; i++) {
				for (int j=0; j<n; j++) {
					RightMember[] row = this.matrix[i];
					RightMember[] col = matrix.getColumn(j);
					RightMember result = by(row, col);
					resultado.matrix[i][j] = result;
					System.out.println(k + ", " + i + ", " + j + "-> " + result);
				}
			}
		}
		return resultado;
	}

	private RightMember by(RightMember[] row, RightMember[] col) {
		RightMember result=null;
		RightMember a = row[0];
		RightMember b = col[0];
		
		result = by(a, b);
		
		int length = row.length;
		for (int i=1; i<length; i++) {
			a = row[i];
			b = col[i];
			RightMember by = by(a, b);
			result = or(result, by);
		}
		return result;
	}

	private RightMember or(RightMember a, RightMember b) {
		if (a==null && b==null)
			return null;
		if (a==null)
			return b;
		if (b==null)
			return a;
		return Or.or(a, b);
	}

	private RightMember by(RightMember a, RightMember b) {
		if (a==null || b==null)
			return null;
		return By.by(a, b);
	}

	private RightMember[] getColumn(int j) {
		int length = this.matrix[0].length; 
		RightMember[] column = new RightMember[length];
		for (int i=0; i<length; i++)
			column[i] = this.matrix[i][j];
		return column;
	}

	private Matrix copy() {
		RightMember rm;
		Matrix result = new Matrix(this.automata);
		for (int i=0; i<n; i++) 
			for (int j=0; j<n; j++) {
				rm = this.matrix[i][j];
				if (rm!=null)
					result.matrix[i][j] = rm.copy();
			}
		return result;
	}
}
