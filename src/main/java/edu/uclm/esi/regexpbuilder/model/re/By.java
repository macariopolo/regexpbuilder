package edu.uclm.esi.regexpbuilder.model.re;

import java.lang.reflect.Method;

@SuppressWarnings("unused")
public class By {
	public static RightMember by(RightMember a, RightMember b) {
		if (a==null || b==null)
			return null;
		RightMember result = null;
		String aName = a.getClass().getSimpleName();
		String bName = b.getClass().getSimpleName();
		String methodName = "by" + aName + bName;
		try {
			Method method = By.class.getDeclaredMethod(methodName, RightMember.class, RightMember.class);
			result = (RightMember) method.invoke(null, a, b);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	private static RightMember byLambdaLambda(RightMember a, RightMember b) {
		return a.copy();
	}
	
	private static RightMember byLambdaSymbol(RightMember a, RightMember b) {
		return b.copy();
	}
	
	private static RightMember byLambdaState(RightMember a, RightMember b) {
		return b.copy();
	}
	
	private static RightMember byLambdaConcat(RightMember a, RightMember b) {
		return b.copy();
	}
	
	private static RightMember byLambdaClosure(RightMember a, RightMember b) {
		return b.copy();
	}
	
	private static RightMember byLambdaRightMemberList(RightMember a, RightMember b) {
		return b.copy();
	}
	
	private static RightMember bySymbolLambda(RightMember a, RightMember b) {
		return a.copy();
	}
	
	private static RightMember bySymbolSymbol(RightMember a, RightMember b) {
		return new Concat(a.copy(), b.copy());
	}
	
	private static RightMember bySymbolState(RightMember a, RightMember b) {
		return new Concat(a.copy(), b.copy());
	}
	
	private static RightMember bySymbolConcat(RightMember a, RightMember b) {
		if (b.depth()==1) {
			Concat concat = (Concat) b;
			if (concat.getLeft().equals(concat.getLeft()) && concat.getLeft().equals(a)) {
				Closure closure = new Closure(a);
				closure.setOperator('+');
				return closure;
			}
		}
		return new Concat(a.copy(), b.copy());
	}
	
	private static RightMember bySymbolClosure(RightMember a, RightMember b) {
		return new Concat(a.copy(), b.copy());
	}
	
	private static RightMember bySymbolRightMemberList(RightMember a, RightMember b) {
		return new Concat(a.copy(), b.copy());
	}
	
	private static RightMember byStateLambda(RightMember a, RightMember b) {
		return byLambdaState(b, a);
	}
	
	private static RightMember byStateSymbol(RightMember a, RightMember b) {
		return new Concat(a, b);
	}
	
	private static RightMember byStateState(RightMember a, RightMember b) {
		return new Concat(a, b);
	}
	
	private static RightMember byStateConcat(RightMember a, RightMember b) {
		return new Concat(a, b);
	}
	
	private static RightMember byStateClosure(RightMember a, RightMember b) {
		return new Concat(a, b);
	}
	
	private static RightMember byStateRightMemberList(RightMember a, RightMember b) {
		return new Concat(a, b);
	}
	
	private static RightMember byConcatLambda(RightMember a, RightMember b) {
		return a.copy();
	}
	
	private static RightMember byConcatSymbol(RightMember a, RightMember b) {
		if (a.depth()==1) {
			Concat concat = (Concat) a;
			if (concat.getLeft().equals(concat.getRight()) && concat.getLeft().equals(b)) {
				Closure closure = new Closure(b);
				closure.setOperator('+');
				return closure;
			}
		}
		return new Concat(a, b);
	}
	
	private static RightMember byConcatState(RightMember a, RightMember b) {
		return new Concat(a, b);
	}
	
	private static RightMember byConcatConcat(RightMember a, RightMember b) {
		return new Concat(a, b);
	}
	
	private static RightMember byConcatClosure(RightMember a, RightMember b) {
		return new Concat(a, b);
	}
	
	private static RightMember byConcatRightMemberList(RightMember a, RightMember b) {
		return new Concat(a, b);
	}
	
	private static RightMember byClosureLambda(RightMember a, RightMember b) {
		return a.copy();
	}
	
	private static RightMember byClosureSymbol(RightMember a, RightMember b) {
		return new Concat(a, b);
	}
	
	private static RightMember byClosureState(RightMember a, RightMember b) {
		return new Concat(a, b);
	}
	
	private static RightMember byClosureConcat(RightMember a, RightMember b) {
		return new Concat(a, b);
	}
	
	private static RightMember byClosureClosure(RightMember a, RightMember b) {
		if (a.equals(b))
			return a.copy();
		return new Concat(a, b);
	}
	
	private static RightMember byClosureRightMemberList(RightMember a, RightMember b) {
		RightMemberList result = new RightMemberList();
		RightMemberList rb = (RightMemberList) b;
		for (int i=0; i<rb.size(); i++)
			result.add(new Concat(a.copy(), rb.get(i).copy()));
		return result;
	}
	
	private static RightMember byRightMemberListRightMemberList(RightMember a, RightMember b) {
		RightMemberList result = new RightMemberList();
		RightMemberList ra = (RightMemberList) a;
		RightMemberList rb = (RightMemberList) b;
		for (int i=0; i<ra.size(); i++)
			for (int j=0; j<rb.size(); j++)
				result.add(new Concat(ra.get(i).copy(), rb.get(j).copy()));
		return result;
	}
	
	private static RightMember byRightMemberListLambda(RightMember a, RightMember b) {
		return a.copy();
	}
	
	private static RightMember byRightMemberListSymbol(RightMember a, RightMember b) {
		RightMemberList result = new RightMemberList();
		RightMemberList ra = (RightMemberList) a;
		for (int i=0; i<ra.size(); i++)
			result.add(new Concat(ra.get(i).copy(), b.copy()));
		return result;
	}
	
	private static RightMember byRightMemberListState(RightMember a, RightMember b) {
		RightMemberList result = new RightMemberList();
		RightMemberList ra = (RightMemberList) a;
		for (int i=0; i<ra.size(); i++)
			result.add(new Concat(ra.get(i).copy(), b.copy()));
		return result;
	}
	
	private static RightMember byRightMemberListConcat(RightMember a, RightMember b) {
		RightMemberList result = new RightMemberList();
		RightMemberList ra = (RightMemberList) a;
		for (int i=0; i<ra.size(); i++)
			result.add(new Concat(ra.get(i).copy(), b.copy()));
		return result;
	}
	
	private static RightMember byRightMemberListClosure(RightMember a, RightMember b) {
		Closure cb = (Closure) b;
		if (cb.getClosured().equals(a)) {
			Closure result = (Closure) b;
			result = (Closure) result.copy();
			result.setOperator('+');
			return result;
		}
		if (cb.equals(a))
			return cb.copy();
		RightMemberList result = new RightMemberList();
		RightMemberList ra = (RightMemberList) a;
		for (int i=0; i<ra.size(); i++)
			result.add(new Concat(ra.get(i).copy(), b.copy()));
		return result;
	}
}
