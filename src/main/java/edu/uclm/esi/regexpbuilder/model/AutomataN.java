package edu.uclm.esi.regexpbuilder.model;

import java.util.List;

import edu.uclm.esi.regexpbuilder.model.re.State;
import edu.uclm.esi.regexpbuilder.model.re.Symbol;

public class AutomataN extends Automata {
	public AutomataN(boolean separated, SessionManager sessionManager, Reloj reloj) {
		super(separated, sessionManager, reloj);
	}
	
	@Override
	protected Automata newAutomata() {
		return new AutomataN(this.separated, sessionManager, reloj);
	}

	protected void endOfSequence(Sequence sequence, State nextState, State[] currentState, List<Symbol> symbols) {
		if (nextState!=null) {
			nextState.setEnd(true);
			sequence.setFinalState(nextState);
			addAllSubsequences(sequence, nextState);
		}
	}
	
	protected State getNextState(State[] currentState, Symbol symbol, boolean lastSymbol) {
		State nextState = currentState[0].nextState(symbol);
		if (nextState == null) {
			nextState = newState();
			currentState[0].addOutputTransition(symbol, nextState);
			currentState[0] = nextState;
		} else {
			currentState[0] = nextState;
		}
		return nextState;
	}
	
	protected State newState() {
		State state = new State(this.counter++);
		this.add(state);
		return state;
	}
}
