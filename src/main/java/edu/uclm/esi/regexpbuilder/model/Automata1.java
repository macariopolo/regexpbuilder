package edu.uclm.esi.regexpbuilder.model;

import java.util.List;

import edu.uclm.esi.regexpbuilder.model.re.State;
import edu.uclm.esi.regexpbuilder.model.re.Symbol;

public class Automata1 extends Automata {
	private State finalState;
	
	public Automata1(boolean separated, SessionManager sessionManager, Reloj reloj) {
		super(separated, sessionManager, reloj);
	}
	
	@Override
	protected Automata newAutomata() {
		return new Automata1(this.separated, sessionManager, reloj);
	}

	protected void endOfSequence(Sequence sequence, State nextState, State[] currentState, List<Symbol> symbols) {
		//currentState[0].addOutputTransition(symbols.get(symbols.size()-1), this.finalState);
		addAllSubsequences(sequence, this.finalState);
	}
	
	protected State getNextState(State[] currentState, Symbol symbol, boolean lastSymbol) {
		State nextState;
		if (lastSymbol && this.finalState==null) {
			nextState = newState();
			currentState[0].addOutputTransition(symbol, nextState);
			nextState.setEnd(true);
			this.finalState = nextState;
		} else if (lastSymbol) {
			nextState = this.finalState;
			currentState[0].addOutputTransition(symbol, nextState);
		} else {
			nextState = currentState[0].nextState(symbol);
			if (nextState == null) {
				if (currentState[0].isFinal() && currentState[0].hasInputTransitionWith(symbol)) {
					nextState = currentState[0];
				} else {
					nextState = newState();
				}
				currentState[0].addOutputTransition(symbol, nextState);
				currentState[0] = nextState;
			} else {
				currentState[0] = nextState;
			}
		}
		return nextState;
	}
	
	protected State newState() {
		/*if (finalState!=null) {
			this.remove(finalState);
			this.finalState.setId(this.states.size()+1);
			this.add(finalState);
		}*/
		State state = new State(this.counter++);
		this.add(state);
		return state;
	}
}
