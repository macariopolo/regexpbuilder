package edu.uclm.esi.regexpbuilder.model;

import edu.uclm.esi.regexpbuilder.model.re.State;

public class MacoZippingStrategy extends ZippingStrategy {

	public MacoZippingStrategy(ManagedTransitionZipper zipper) {
		super(zipper, zipper.getRepeticiones());
	}

	@Override
	public StringBuilder zip(State state, CharSequence s, int depthLevel) {
		StringBuilder expresion = (StringBuilder) s;
		int length = expresion.length();
		if (length<repeticiones)
			return expresion;
		int patternLength = length/repeticiones;
		
		StringBuilder masCorta = expresion, candidata = new StringBuilder();
		int remainder, parentesisMasCorta=0;
		StringBuilder r0=new StringBuilder(), interior=new StringBuilder(), interiorClausurada, rF=new StringBuilder();
		for (int currentLength=patternLength; currentLength>=1; currentLength--) {
			int offset = 0;
			do {
				remainder = (length-offset) % currentLength;
				r0.delete(0, r0.length()).append(expresion.substring(0, offset));	
				interior.delete(0, interior.length()).append(expresion.substring(offset, length-remainder));
				interiorClausurada = clausura(state, interior, currentLength);
				rF.delete(0, rF.length()).append(expresion.substring(length-remainder, expresion.length()));
				candidata.delete(0, candidata.length()).append(r0).append(interiorClausurada).append(rF);
				
				if (candidata.length()<masCorta.length()) {
					masCorta = new StringBuilder(candidata);
					//parentesisMasCorta = getParentesis(masCorta);
				} /*else if (candidata.length()==masCorta.length()) {
					int parentesisCandidata = getParentesis(candidata);
					if (parentesisCandidata>parentesisMasCorta) {
						masCorta = new StringBuilder(candidata);
						parentesisMasCorta = parentesisCandidata;
					}
				}*/
				offset++;
			} while (interior.length()>repeticiones*currentLength);
		}
		return masCorta;
	}
	

	private int getParentesis(StringBuilder masCorta) {
		int r = 0;
		for (int i=0; i<masCorta.length(); i++)
			if (masCorta.charAt(i)=='{')
				r++;
		return r;
	}

	private StringBuilder clausura(State state, StringBuilder cadena, int currentLength) {
		int length = cadena.length();
		int numeroDeTrozos = length/currentLength;
		
		StringBuilder nueva=new StringBuilder();
		int inicioCoincidencia = 0, finCoincidencia = 0;
		StringBuilder trozo=new StringBuilder(currentLength), lastTrozo=null;
		int start = 0;
		for (int indexTrozo=0; indexTrozo<numeroDeTrozos; indexTrozo++) {
			trozo.delete(0, trozo.length()).append(cadena.substring(start, start+currentLength));
			if (indexTrozo>0) {
				if (trozo.toString().equals(lastTrozo.toString())) {
					if (finCoincidencia-inicioCoincidencia == 0) {
						inicioCoincidencia = indexTrozo-1;
						finCoincidencia = indexTrozo;
					} else { 
						finCoincidencia = indexTrozo;
					}
				} else {
					if (finCoincidencia-inicioCoincidencia>=this.repeticiones-1) {
						if (lastTrozo.length()*currentLength>=this.repeticiones) {
							StringBuilder clausuraLastTrozo = zip(state, lastTrozo, 1);
							nueva.append("{").append(clausuraLastTrozo).append("}");
						} else {
							nueva.append("{").append(lastTrozo).append("}");
						}
					} else if (finCoincidencia-inicioCoincidencia==1) {
						nueva.append(lastTrozo).append(lastTrozo);
					} else {
						nueva.append(lastTrozo);
					}
					inicioCoincidencia = 0;
					finCoincidencia = 0;
				}
			}
			lastTrozo = new StringBuilder(trozo.length());
			lastTrozo.append(trozo);
			start = start+currentLength;
		}
		
		nueva = this.zip(state, nueva, 1);
		if (finCoincidencia-inicioCoincidencia>1) {
			StringBuilder clausuraLastTrozo = zip(state, lastTrozo, 1);
			nueva.append("{").append(clausuraLastTrozo).append("}");
		} else if (finCoincidencia-inicioCoincidencia==1) {
			start = currentLength*inicioCoincidencia;
			nueva.append(cadena.substring(start, cadena.length()));
		} else {
			nueva.append(lastTrozo);
		}

		return nueva;
	}

}
