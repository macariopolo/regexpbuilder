package edu.uclm.esi.regexpbuilder.model.re;

public interface ILabel {

	RightMember copy();

}
