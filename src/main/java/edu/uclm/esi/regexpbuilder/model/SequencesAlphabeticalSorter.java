package edu.uclm.esi.regexpbuilder.model;

import java.util.Comparator;

public class SequencesAlphabeticalSorter implements Comparator<Sequence> {
	public static final int A_TO_Z = 1;
	public static final int Z_TO_A = 2;
	
	private int mode;

	public SequencesAlphabeticalSorter(int mode) {
		this.mode = mode;
	}

	@Override
	public int compare(Sequence s1, Sequence s2) {
		if (mode == 1)
			return s1.toString().compareTo(s2.toString());
		else
			return s2.toString().compareTo(s1.toString());
	}

}
