package edu.uclm.esi.regexpbuilder.model.equations;

import edu.uclm.esi.regexpbuilder.model.re.RightMember;

public class RMCounter {
	private RightMember rightMember;
	private int n;
	
	public RMCounter(RightMember rightMember) {
		this.rightMember = rightMember;
	}
	
	public void inc() {
		this.n++;
	}
	
	public RightMember getRightMember() {
		return rightMember;
	}
	
	public int getN() {
		return n;
	}
	
	@Override
	public String toString() {
		return this.rightMember.toString() + " -> " + this.n;
	}
}
