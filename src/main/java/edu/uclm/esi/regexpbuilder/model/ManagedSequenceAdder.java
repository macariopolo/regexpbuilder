package edu.uclm.esi.regexpbuilder.model;

import edu.uclm.esi.regexpbuilder.web.ws.WSServerRegExp;

public class ManagedSequenceAdder extends ManagedThread {

	private Automata automata;
	
	public void setAutomata(Automata automata) {
		this.automata = automata;
	}
	
	@Override
	public void run() {		
		IProcessable processable;
		while ((processable=this.manager.giveMeNext())!=null) {
			Sequence sequence = (Sequence) processable.getObject();
			automata.add(this.id, sequence.getObject(), processable.getIndex(), this.size);
		}
		WSServerRegExp.sendSubprogress(session, id, "idle", 1);
	}
}
