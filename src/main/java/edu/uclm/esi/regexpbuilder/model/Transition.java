package edu.uclm.esi.regexpbuilder.model;

import edu.uclm.esi.regexpbuilder.model.re.ILabel;
import edu.uclm.esi.regexpbuilder.model.re.State;

public class Transition {
	private State source;
	private State target;
	private ILabel symbol;
	private boolean removable;
	
	private Transition(State source, State target) {
		this.source = source;
		this.target = target;
	}

	public Transition(State source, State target, ILabel symbol) {
		this(source, target);
		this.symbol=symbol;
	}
	
	public State getSource() {
		return source;
	}

	public State getTarget() {
		return this.target;
	}

	@Override
	public String toString() {
		return "[" + this.source + " + " + this.symbol.toString() + "-> " + this.target + "] ";
	}

	public ILabel getSymbol() {
		return this.symbol;
	}

	public String key() {
		return "[" + this.target.getId() + "|" + this.symbol.toString() + "]";
	}

	public void setSymbol(ILabel symbol) {
		this.symbol = symbol;
	}
	
	public boolean isSelfTransition() {
		return this.source==this.target;
	}

	public void markToRemove() {
		this.removable = true;
	}
	
	public boolean isRemovable() {
		return removable;
	}
}
