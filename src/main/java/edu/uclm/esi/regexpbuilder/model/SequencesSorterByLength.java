package edu.uclm.esi.regexpbuilder.model;

import java.util.Comparator;

public class SequencesSorterByLength implements Comparator<Sequence> {
	public static final int SORTHEST_FIRST = 1;
	public static final int LONGEST_FIRST = 2;
	
	private int mode;

	public SequencesSorterByLength(int mode) {
		this.mode = mode;
	}

	@Override
	public int compare(Sequence s1, Sequence s2) {
		if (mode == 2)
			return s2.length()-s1.length();
		else
			return s1.length()-s2.length();
	}

}
