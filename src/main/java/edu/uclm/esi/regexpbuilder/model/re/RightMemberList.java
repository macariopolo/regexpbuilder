package edu.uclm.esi.regexpbuilder.model.re;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import edu.uclm.esi.regexpbuilder.model.equations.Equation;
import edu.uclm.esi.regexpbuilder.model.equations.RMCounter;

public class RightMemberList extends RightMember implements Iterable<RightMember> {
	private List<RightMember> rightMembers;

	public RightMemberList() {
		this.rightMembers = new ArrayList<>();
	}

	public RightMemberList(List<RightMember> rightMembers) {
		this.rightMembers = rightMembers; 
		for (RightMember rm : this.rightMembers)
			rm.setParent(this);
	}
	
	public RightMemberList(RightMember... members) {
		RightMember rm;
		for (int i=0; i<members.length; i++) {
			rm = members[i];
			if (rm instanceof RightMemberList) {
				List<RightMember> elements = rm.flatten();
				for (RightMember element : elements)
					if (!this.rightMembers.contains(element))
						this.rightMembers.add(element);
			} else {
				this.rightMembers.add(rm);
			}
		}
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (RightMember rightMember : this.rightMembers)
			sb.append(rightMember.toString() + " | ");
		String r = sb.toString();
		if (r.length()>3) 
			r = r.substring(0, r.length()-3);
		return r;
	}

	@Override
	public int length() {
		int length = 0;
		for (RightMember rightMember : this.rightMembers)
			length = length + rightMember.length();
		return length;
	}

	public int size() {
		return this.rightMembers.size();
	}

	public List<RightMember> getRightMembers() {
		return rightMembers;
	}

	public void add(RightMember newRM) {
		RightMember rm;
		for (int i=0; i<this.rightMembers.size(); i++) {
			rm = this.rightMembers.get(i);
			if (rm.completelyContains(newRM))
				return;
			if (newRM.completelyContains(rm)) {
				this.rightMembers.remove(i);
				this.rightMembers.add(newRM);
				return;
			}
		}
		this.rightMembers.add(newRM);
	}

	@Override
	public RightMember copy() {
		RightMemberList rml = new RightMemberList();
		for (RightMember rm : this.rightMembers)
			rml.add(rm.copy());
		return rml;
	}

	@Override
	protected int depth() {
		int max = 0;
		for (RightMember rm : this.rightMembers) {
			int aux = rm.depth();
			if (aux>max)
				max = aux;
		}
		return max;
	}
	
	@Override
	public List<RightMember> flatten() {
		ArrayList<RightMember> list = new ArrayList<>();
		for (RightMember rm : this.rightMembers)
			list.addAll(rm.flatten());
		return list;
	}

	@Override
	public void loadSubTrees(Map<String, RMCounter> subarboles, int weight) {
		for (RightMember rm : this.rightMembers)
			rm.loadSubTrees(subarboles, weight);
	}
	
	@Override
	protected int descendants() {
		int r = 1;
		for (RightMember rm : this.rightMembers)
			r = r + rm.descendants();
		return r;
	}
	
	@Override
	public RightMember removeState(Equation equation, State state) {
		for (int i=this.rightMembers.size()-1; i>=0; i--) {
			this.rightMembers.set(i, this.rightMembers.get(i).removeState(equation, state));
		}
		return this;
	}

	@Override
	public RightMember replaceState(Equation equation, Equation replacementEq) {
		RightMember rm;
		for (int i = this.rightMembers.size()-1; i>=0; i--) {
			rm = this.rightMembers.get(i);
			if (rm.contains(replacementEq.getLeftMember())) 
				this.rightMembers.set(i, rm.replaceState(equation, replacementEq));
		}
		return this;
	}

	@Override
	public List<State> getStatesUsed() {
		List<State> statesUsed = new ArrayList<>();
		for (RightMember rightMember : this.rightMembers)
			for (State stateUsed : rightMember.getStatesUsed())
				if (!statesUsed.contains(stateUsed))
					statesUsed.add(stateUsed);
		return statesUsed;
	}

	@Override
	public boolean contains(RightMember rightMember) {
		for (RightMember rm : this.rightMembers)
			if (rm.contains(rightMember))
				return true;
		return false;
	}
	
	@Override
	public RightMember getDistributivableNode() {
		if (this.getParent() instanceof Concat)
			return this;
		return null;
	}
	
	@Override
	public RightMemberList applyDistributive() {
		Concat parent = (Concat) this.getParent();
		
		RightMember sibling;
		boolean byLeft;
		if (parent.getLeft()==this) {
			sibling = parent.getRight();
			byLeft = true;
		} else {
			sibling = parent.getLeft();
			byLeft = false;
		}
		
		RightMemberList rml = new RightMemberList();
		for (int i=0; i<this.rightMembers.size(); i++) {
			RightMember rm = this.rightMembers.get(i);
			Concat concat;
			RightMember rmCopy = rm.copy();
			RightMember siblingCopy = sibling.copy();
			concat = byLeft ? new Concat(rmCopy, siblingCopy) : new Concat(siblingCopy, rmCopy);
			rml.add(concat);
		}
		
		RightMember root = getRoot();
		if (root!=parent) {
			root = root.trimUntil(parent);
			for (int i=0; i<rml.size(); i++) {
				RightMember rm = rml.getRightMembers().get(i);
				Concat concat = (Concat) root;
				concat = (Concat) concat.copy();
				concat.addToNull(rm, byLeft);
				rml.rightMembers.set(i, concat);
			}
		}
		
		return rml;
	}

	@Override
	public RightMember trimUntil(Concat node) {
		for (RightMember rightMember : this.rightMembers)
			rightMember.trimUntil(node);
		return this;
	}

	public RightMember get(int index) {
		return this.rightMembers.get(index);
	}

	@Override
	public Iterator<RightMember> iterator() {
		return this.rightMembers.iterator();
	}

	public void set(int index, RightMember newRightMember) {
		this.rightMembers.set(index, newRightMember);
	}

	public void remove(int index) {
		this.rightMembers.remove(index);
	}

	public void add(int index, RightMember rightMember) {
		if (!this.rightMembers.contains(rightMember))
			this.rightMembers.add(index, rightMember);
	}

	public void clear() {
		this.rightMembers.clear();
	}

	public boolean isEmpty() {
		return this.rightMembers.isEmpty();
	}

	@Override
	public RightMember simplify() {
		RightMember ri, rj;
		for (int i=this.rightMembers.size()-1; i>0; i--) {
			ri = this.rightMembers.get(i);
			for (int j=i-1; j>=0; j--) {
				rj = this.rightMembers.get(j);
				if (ri.toString().equals(rj.toString())) {
					this.rightMembers.remove(i);
					break;
				}
			}
		}
		for (int i=0; i<this.rightMembers.size(); i++) {
			ri = this.rightMembers.get(i);
			this.rightMembers.set(i, ri.simplify());
		}
		return this;
	}

	@Override
	public RightMember sort() {
		for (int i=0; i<this.rightMembers.size(); i++)
			this.rightMembers.get(i).sort();
		Collections.sort(this.rightMembers, new Comparator<RightMember>() {
			@Override
			public int compare(RightMember o1, RightMember o2) {
				return o1.toString().compareTo(o2.toString());
			}
		});
		return this;
	}
	
	@Override
	public boolean completelyContains(RightMember rightMember) {
		for (RightMember rm : this.rightMembers)
			if (rm.completelyContains(rightMember))
				return true;
		return false;
	}

	public RightMember last() {
		return this.rightMembers.get(this.rightMembers.size()-1);
	}

	@Override
	public String polish() {
		StringBuilder sb = new StringBuilder();
		RightMember rm;
		for (int i=0; i<this.rightMembers.size()-1; i++) {
			rm = this.rightMembers.get(i);
			sb.append(rm.polish() + "|");			
		}
		rm = this.rightMembers.get(this.rightMembers.size()-1);
		sb.append(rm.polish());
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((rightMembers == null) ? 0 : rightMembers.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RightMemberList other = (RightMemberList) obj;
		if (rightMembers == null) {
			if (other.rightMembers != null)
				return false;
		} else if (!rightMembers.equals(other.rightMembers))
			return false;
		return true;
	}

	@Override
	public RightMember loadSubTree(int currentLength) {
		return null;
	}
	
	@Override
	public boolean produces(RightMember rm) {
		for (int i=0; i<this.rightMembers.size()-1; i++) {
			if (this.rightMembers.get(i).produces(rm))
				return true;
		}
		return false;
	}
}
