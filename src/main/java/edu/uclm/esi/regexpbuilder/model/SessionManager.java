package edu.uclm.esi.regexpbuilder.model;

import java.io.IOException;
import java.lang.reflect.Constructor;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;

import edu.uclm.esi.regexpbuilder.model.equations.EquationsSolver;
import edu.uclm.esi.regexpbuilder.model.matrix.HeuristicMatrixSolver;
import edu.uclm.esi.regexpbuilder.model.matrix.TransitiveClosureMatrixSolver;
import edu.uclm.esi.regexpbuilder.model.statemachines.TransitionsPairsSolver;
import edu.uclm.esi.regexpbuilder.model.statesRemoval.StatesRemovalSolver;

public class SessionManager {
	private WebSocketSession session;
	private int numberOfThreads;
	private SequenceCollection sequenceCollection;

	public SessionManager(WebSocketSession session) {
		this.session = session;
	}
	
	public int getNumberOfProcessors() {
		return Runtime.getRuntime().availableProcessors();
	}
	
	public void setNumberOfThreads(int numberOfThreads) {
		this.numberOfThreads = numberOfThreads;
	}
	
	public ManagedThread[] createThreads(Class<?> clazz, IThreadManager threadManager) {
		threadManager.reset();
		ManagedThread[] threads = new ManagedThread[this.numberOfThreads];
		try {
			for (int i=0; i<threads.length; i++) { 
				threads[i] = (ManagedThread) clazz.newInstance();
				threads[i].setId(i+1);
				threads[i].setManager(threadManager);
				threads[i].setSession(session);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return threads;
	}
	
	@SuppressWarnings("unchecked")
	public REThread[] createThreads(Class<?> clazz, Object[] objects) {
		int size = objects.length;
		REThread[] threads = new REThread[this.numberOfThreads];
		try {
			Constructor<REThread> cons = (Constructor<REThread>) clazz.getDeclaredConstructors()[0];
			int start=0, end=0;
			for (int i=0; i<this.numberOfThreads; i++) {
				start = (size/this.numberOfThreads)*i;
				end = (size/this.numberOfThreads)*(i+1);
				if (i==this.numberOfThreads-1 && size>end)
					end = size;
				try {
					threads[i] = cons.newInstance(i+1, start, end, objects);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		} 
		return threads;
	}

	public WebSocketSession getSession() {
		return this.session;
	}

	public String getSessionId() {
		return this.session.getId();
	}

	public void loadSequenceCollection(String[] cadenas, boolean separated) {
		this.sequenceCollection = new SequenceCollection(cadenas, separated, this);
	}

	public JSONObject getRegularExpression(String numberOfFinalStates, String order, boolean minimizeAutomata, String algorithm) throws JSONException, IOException {
		sendProgress("Sorting sequences");
		Reloj reloj = new Reloj(session);
		sortSequences(order, reloj);
		
		sendProgress("Building original automata");
		this.sequenceCollection.buildAutomata(numberOfFinalStates, this, reloj);
		Automata originalAutomata = this.sequenceCollection.getAutomata();
		sendProgress("Calculating minimized automata");
		Automata minimizedAutomata = originalAutomata.minimize();
		sendAutomatas(originalAutomata, minimizedAutomata);
		Automata automata = minimizeAutomata ? minimizedAutomata : originalAutomata;
		Solver solver;
		if (algorithm.equals("equations")) {
			reloj.start(Reloj.BUILD_EQUATIONS);
			solver = new EquationsSolver(automata, this, reloj);
			reloj.stop(Reloj.BUILD_EQUATIONS);
		} else if (algorithm.equals("statesRemoval")) {
			solver = new StatesRemovalSolver(automata, this, reloj);
		} else if (algorithm.equals("transitionPairs")) {
			solver = new TransitionsPairsSolver(automata, this, reloj);
		} else if (algorithm.equals("matrices")) { 
			solver = new TransitiveClosureMatrixSolver(automata, this, reloj);
		} else { 
			solver = new HeuristicMatrixSolver(automata, this, reloj);
		}
		reloj.start(Reloj.SOLVING);
		solver.solve();
		reloj.stop(Reloj.SOLVING);
		JSONObject resp = new JSONObject();
		resp.put("type", "regularExpression");
		resp.put("regularExpression", solver.getRegularExpression());
		session.sendMessage(new TextMessage(resp.toString()));
		return resp;
	}

	private void sortSequences(String order, Reloj reloj) {
		reloj.start(Reloj.SORTING);
		if (order.equals("shortest"))
			this.sequenceCollection.sortByLength(true);
		else if (order.equals("longest"))
			this.sequenceCollection.sortByLength(false);
		else if (order.equals("byStart"))
			this.sequenceCollection.sortByStart();
		else 
			this.sequenceCollection.sortByEnd();
		reloj.stop(Reloj.SORTING);
	}
	
	public synchronized void sendProgress(String text) {
		if (session!=null) {
			try {
				JSONObject jso = new JSONObject();
				jso.put("type", "progress");
				jso.put("message", text);
				WebSocketMessage<?> wsMessage=new TextMessage(jso.toString());
				session.sendMessage(wsMessage);
			} catch (Exception e) {
				System.err.println(e.toString());
			}
		}
	}
	
	public void sendAutomata(Automata automata, String title) {
		if (session==null || automata.getNumberOfStates()>40)
			return;
		try {
			JSONObject resp = new JSONObject();
			resp.put("type", "automata");
			resp.put("automata", automata.toJSON());
			resp.put("title", title);
			session.sendMessage(new TextMessage(resp.toString()));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void sendAutomatas(Automata original, Automata minimized) {
		if (session==null || original.getNumberOfStates()>40 || minimized.getNumberOfStates()>40)
			return;
		try {
			JSONObject resp = new JSONObject();
			resp.put("type", "automatas");
			resp.put("originalAutomata", original.toJSON());
			resp.put("minimizedAutomata", minimized.toJSON());
			session.sendMessage(new TextMessage(resp.toString()));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void sendSubprogress(int threadId, String msg, double chance) {
		if (session==null || (session!=null && Manager.get().getDado().nextDouble()>chance))
			return;
		try {
			JSONObject jso = new JSONObject();
			jso.put("type", "subprogress");
			jso.put("message", "Thread " + threadId + ": " + msg);
			WebSocketMessage<?> wsMessage=new TextMessage(jso.toString());
			session.sendMessage(wsMessage);
		} catch (Exception e) {
			System.err.println(e.toString());
		}
	}
}
