package edu.uclm.esi.regexpbuilder.model;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import edu.uclm.esi.regexpbuilder.model.re.State;
import edu.uclm.esi.regexpbuilder.model.re.Symbol;

public class Sequence implements IProcessable {
	private List<Symbol> symbols;
	private int frequency;
	private boolean separated;
	private State secondState;
	private State finalState;
	private int index;
	
	public Sequence() {
		this.symbols = new ArrayList<>();
	}

	public Sequence(String cadena, boolean separated) {
		this();
		this.separated = separated;
		if (!separated) {
			for (int i=0; i<cadena.length(); i++)
				if (!Character.isWhitespace(cadena.charAt(i)))
					this.symbols.add(new Symbol("" + cadena.charAt(i)));
		} else {
			StringTokenizer st = new StringTokenizer(cadena, " ");
			while (st.hasMoreTokens())
				this.symbols.add(new Symbol(st.nextToken()));
		}
	}

	public Sequence(List<Symbol> symbols) {
		this();
		for (Symbol symbol : symbols)
			this.symbols.add(symbol);
	}

	public int length() {
		return this.symbols.size();
	}
	
	void increaseFrecuency() {
		this.frequency++;
	}
	
	int getFrequency() {
		return frequency;
	}

	public void analyze(SubsequenceMap map) {
		int weight = map.getLength();
		int[] pos = { 0 };
		Sequence pattern;
		while (pos[0]<symbols.size()) {
			pattern = pickSubsequence(weight, pos);
			if (pattern.length()==weight)
				map.index(pattern);
		}
	}

	private Sequence pickSubsequence(int weight, int[] pos) {
		int start = pos[0];
		Sequence subsequence = new Sequence();
		for (int i=pos[0]; i<start + weight && i<this.symbols.size(); i++) {
			subsequence.symbols.add(this.symbols.get(i));
			pos[0]++;
		}
		return subsequence;
	}
	
	public List<Symbol> getSymbols() {
		return symbols;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Symbol symbol : this.symbols) {
			sb.append(symbol.toString());
			if (separated) 
				sb.append(" ");
		}
		return sb.toString().trim();
	}

	public Sequence reverse() {
		Sequence sequence = new Sequence();
		sequence.separated = this.separated;
		for (int i=symbols.size()-1; i>=0; i--)
			sequence.symbols.add(this.symbols.get(i));
		return sequence;
	}

	public Sequence getSubsequence(int start) {
		Sequence subsequence = new Sequence();
		subsequence.separated = this.separated;
		for (int i=start; i<this.symbols.size(); i++)
			subsequence.symbols.add(this.symbols.get(i));
		return subsequence;
	}

	public void setFinalState(State finalState) {
		this.finalState = finalState;
	}

	public State getFinalState() {
		return this.finalState;
	}

	public void setSecondState(State secondState) {
		this.secondState = secondState;
	}
	
	public State getSecondState() {
		return secondState;
	}

	public void add(Symbol symbol) {
		this.symbols.add(symbol);
	}

	public void prepend(Symbol symbol) {
		this.symbols.add(0, symbol);
	}
	
	@Override
	public boolean equals(Object obj) {
		return this.toString().equals(obj.toString());
	}

	@Override
	public Sequence getObject() {
		return this;
	}

	@Override
	public int getIndex() {
		return this.index;
	}

	@Override
	public void setIndex(int index) {
		this.index = index;
	}
}
