package edu.uclm.esi.regexpbuilder.model;

public interface IThreadManager {
	IProcessable giveMeNext();
	void reset();
	int size();
}
