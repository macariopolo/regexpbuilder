package edu.uclm.esi.regexpbuilder.model.statesRemoval;

import java.util.ArrayList;
import java.util.List;

import edu.uclm.esi.regexpbuilder.model.Automata;
import edu.uclm.esi.regexpbuilder.model.Reloj;
import edu.uclm.esi.regexpbuilder.model.SessionManager;
import edu.uclm.esi.regexpbuilder.model.Solver;
import edu.uclm.esi.regexpbuilder.model.Transition;
import edu.uclm.esi.regexpbuilder.model.re.Closure;
import edu.uclm.esi.regexpbuilder.model.re.Concat;
import edu.uclm.esi.regexpbuilder.model.re.ILabel;
import edu.uclm.esi.regexpbuilder.model.re.Lambda;
import edu.uclm.esi.regexpbuilder.model.re.RightMember;
import edu.uclm.esi.regexpbuilder.model.re.RightMemberList;
import edu.uclm.esi.regexpbuilder.model.re.State;

public class StatesRemovalSolver extends Solver {
	
	public StatesRemovalSolver(Automata automata, SessionManager sessionManager, Reloj reloj) {
		super(automata, sessionManager, reloj);
	}

	public void solve() {
		reloj.start(Reloj.PROCESSING_INITIAL_STATE);
		sessionManager.sendProgress("Processing initial state");		
		if (!automata.getInitial().getInputTransitionsList().isEmpty()) {
			State state = new State(-1);
			state.addOutputTransition(new Lambda(), automata.getInitial());
			automata.setInitialState(state);
			automata.add(state);
		}
		reloj.stop(Reloj.PROCESSING_INITIAL_STATE);
		
		reloj.start(Reloj.PROCESSING_FINAL_STATES);
		sessionManager.sendProgress("Processing final states");
		List<State> finalStates = automata.getFinalStates();
		if (finalStates.size()>1 || (finalStates.size()==1 && !finalStates.get(0).getOutputTransitionsList().isEmpty())) {
			State newFinalState = new State(automata.getNumberOfStates()+1);
			newFinalState.setEnd(true);
			automata.add(newFinalState);
			for (State state : finalStates) {
				state.setEnd(false);
				state.addOutputTransition(new Lambda(), newFinalState);
			}
		}
		reloj.stop(Reloj.PROCESSING_FINAL_STATES);
		
		reloj.start(Reloj.REMOVING_STATES);
		State[] states = automata.getStates().values().toArray(new State[automata.getNumberOfStates()]);
		State state;
		List<Transition> inputs, outputs;
		int cont=1;
		for (int i=states.length-1; i>=2; i--) {
			state=states[i];
			inputs = state.getInputTransitionsList();
			outputs = state.getOutputTransitionsList();
			sessionManager.sendProgress("Processing " + state + ": " + cont++ + "/" + states.length);
			if (!inputs.isEmpty() && !outputs.isEmpty()) {
				group(automata, state, inputs, outputs);
				automata.remove(state);
				sessionManager.sendAutomata(automata, "Removal of " + state.toString());
				states[i] = null;
			}
		}
		finalGrouping(automata);
		sessionManager.sendAutomata(automata, "Final automata");
		reloj.stop(Reloj.REMOVING_STATES);
		System.out.println("ACABÉ");
	}

	private void finalGrouping(Automata automata) {
		State[] states = automata.getStates().values().toArray(new State[automata.getNumberOfStates()]);
		State initial = null, state = null, finalState = null;
		for (int i=0; i<states.length; i++) {
			if (states[i].isInitial())
				initial = states[i];
			else if (states[i].isFinal())
				finalState = states[i];
			else
				state = states[i];
		}
		List<Transition> inputs = state.getInputTransitionsList();
		ArrayList<Transition> selfTransitions = loadSelfTransitions(state);
		RightMember inputSymbol, repeatedSymbol, concatSymbol;
		for (Transition input : inputs) {
			input.markToRemove();
			if (input.isSelfTransition())
				continue;
			inputSymbol = (RightMember) input.getSymbol();
			for (Transition selfTransition : selfTransitions) {
				repeatedSymbol = new Closure((RightMember) selfTransition.getSymbol());
				concatSymbol = buildConcat(inputSymbol, repeatedSymbol);
				initial.addOutputTransition((ILabel) concatSymbol, finalState);
			}			
		}
		finalState.removeInputLambdas();
		automata.remove(state);
		removeRemovableTransitions(finalState, inputs, null);
	}

	private void group(Automata automata, State state, List<Transition> inputs, List<Transition> outputs) {
		Transition input, output, selfTransition;
		State source, target;
		RightMember inputSymbol, outputSymbol;
		RightMember concatSymbol = null;
		
		ArrayList<Transition> selfTransitions = this.loadSelfTransitions(state);

		int outputsSize = outputs.size();
		for (int i=0; i<inputs.size(); i++) {
			input = inputs.get(i);
			if (input.isSelfTransition())
				continue;
			input.markToRemove();
			inputSymbol = (RightMember) input.getSymbol();
			source = input.getSource();
			
			for (int j=0; j<outputsSize; j++) {
				output = outputs.get(j);
				output.markToRemove();
				sessionManager.sendSubprogress(-1, (i+1) + "/" + inputs.size() + " inputs, " + (j+1) + "/" + outputs.size() + " ouputs", 1);
				outputSymbol = (RightMember) output.getSymbol();
				target = output.getTarget();
				//System.out.print("(" + source + " + " + left(input.getSymbol()) + "...) = " + state + " ^ ");
				//System.out.print("(" + state + " + ..." + right(output.getSymbol()) + ") = " + target + " ==> ");
				
				if (target.isFinal()) {
					concatSymbol = buildConcat(inputSymbol, outputSymbol);
					source.replaceOutputTransition((ILabel) concatSymbol, target);
					//source.addOutputTransition(new Lambda(), target);
				} else if (selfTransitions.isEmpty()) {
					concatSymbol = buildConcat(inputSymbol, outputSymbol);
					source.replaceOutputTransition((ILabel) concatSymbol, target);
				} else if (!output.isSelfTransition()) {
					RightMemberList rml = new RightMemberList();
					for (int k=0; k<selfTransitions.size(); k++) {
						selfTransition = selfTransitions.get(k);
						rml.add((RightMember) selfTransition.getSymbol());
					}
					RightMember repeatedSymbol = new Closure(rml);
					concatSymbol = buildConcat(inputSymbol, repeatedSymbol);
					concatSymbol = buildConcat((RightMember) concatSymbol, outputSymbol);
					source.replaceOutputTransition((ILabel) concatSymbol, target);
				}
				/*if (concatSymbol!=null) {
					System.out.print("(" + source + " + " + left((ILabel) concatSymbol) + "..." + right((ILabel) concatSymbol) + ")");
					System.out.println(" = " + target);
				}*/
			}
			
		}
		removeRemovableTransitions(state, inputs, outputs);
	}

	private String right(ILabel symbol) {
		String s = symbol.toString();
		int i = s.length()-1;
		while (s.charAt(i)==')' || s.charAt(i)=='+')
			i--;
		s = "" + s.charAt(i);
		return s;
	}

	private String left(ILabel symbol) {
		String s = symbol.toString();
		int i = 0;
		while (s.charAt(i)=='(')
			i++;
		return "" + s.charAt(i);
	}

	private void removeRemovableTransitions(State state, List<Transition> inputs, List<Transition> outputs) {
		Transition input;
		Transition output;
		State source;
		int size = inputs.size();
		for (int i=size-1; i>=0; i--) {
			input = inputs.get(i);
			if (input.isRemovable()) {
				source = input.getSource();
				source.removeOutputTransition(input);
				if (source.getOutputTransitionsList().isEmpty()) {
					System.out.println(source + " se ha quedado sin salidas");
				}
			}
		}
		if (outputs!=null) {
			size = outputs.size();
			for (int i=size-1; i>=0; i--) {
				output = outputs.get(i);
				if (output.isRemovable())
					state.removeOutputTransition(output);
				if (output.getTarget().getInputTransitionsList().isEmpty())
					System.out.println(output.getTarget() + " se ha quedado sin entradas");
			}
		}
	}
	
	private RightMember buildConcat(RightMember inputSymbol, RightMember outputSymbol) {
		if (inputSymbol instanceof Lambda)
			return outputSymbol;
		else if (outputSymbol instanceof Lambda)
			return inputSymbol;
		else
			return new Concat(inputSymbol, outputSymbol);
	}

	private ArrayList<Transition> loadSelfTransitions(State state) {
		ArrayList<Transition> selfTransitions = new ArrayList<>();
		for (Transition t : state.getOutputTransitionsList())
			if (t.isSelfTransition())
				selfTransitions.add(t);
		return selfTransitions;
	}

	@Override
	public String getRegularExpression() {
		reloj.start(Reloj.BUILDING_REGULAR_EXPRESSION);
		StringBuilder sb=new StringBuilder();
		List<Transition> transitions = this.automata.getInitial().getOutputTransitionsList();
		for (Transition t : transitions)
			sb.append(t.getSymbol() + " | ");
		String r = sb.toString();
		if (r.length()>1)
			r = r.substring(0, r.length()-3);
		reloj.stop(Reloj.BUILDING_REGULAR_EXPRESSION);
		return r;
	}
}