package edu.uclm.esi.regexpbuilder.model;

import java.util.Hashtable;

import edu.uclm.esi.regexpbuilder.model.re.State;

public class MacoZippingStrategy2 extends ZippingStrategy {
	private Hashtable<String, StringBuilder> preclausuras;

	public MacoZippingStrategy2(ManagedTransitionZipper zipper) {
		super(zipper, zipper.getRepeticiones());
		this.preclausuras = new Hashtable<>();
	}

	@Override
	public StringBuilder zip(State state, CharSequence s, int depthLevel) {
		String expresion = s.toString();
		int length = expresion.length();
		if (length<repeticiones)
			return new StringBuilder(expresion);
		int patternLength = length/repeticiones;
		
		String masCorta = expresion, candidata;
		int remainder, parentesisMasCorta=0;
		String r0, interior, rF;
		String r0Clausurada, interiorClausurada, rFClausurada;
		for (int currentLength=3; currentLength>=1; currentLength--) {
			remainder = length % currentLength;
			int offset = 0;
			do {
				r0=expresion.substring(0, offset);	
				interior=expresion.substring(offset, length-offset);
				rF=expresion.substring(length-offset, expresion.length());

				r0Clausurada = zip(state, r0, depthLevel).toString();
				interiorClausurada = clausura(state, interior, currentLength).toString();
				rFClausurada = zip(state, rF, depthLevel).toString();
				
				candidata=r0Clausurada + interiorClausurada + rFClausurada;
				
				if (candidata.length()<masCorta.length()) {
					masCorta = candidata;
					parentesisMasCorta = getParentesis(masCorta);
				} else if (candidata.length()==masCorta.length()) {
					int parentesisCandidata = getParentesis(candidata);
					if (parentesisCandidata>parentesisMasCorta) {
						masCorta = candidata;
						parentesisMasCorta = parentesisCandidata;
					}
				}
				offset++;
			} while (interior.length()>currentLength*patternLength);
		}
		return new StringBuilder(masCorta);
	}

	private StringBuilder clausura(State state, String cadena, int currentLength) {
		int length = cadena.length();
		int numeroDeTrozos = length/currentLength;
		
		if (numeroDeTrozos<this.repeticiones)
			return new StringBuilder(cadena);
		
		String nueva="";
		int inicioCoincidencia = 0, finCoincidencia = 0;
		String trozo, lastTrozo=null;
		int start = 0;
		for (int indexTrozo=0; indexTrozo<numeroDeTrozos; indexTrozo++) {
			trozo=cadena.substring(start, start+currentLength);
			if (indexTrozo>0) {
				if (trozo.equals(lastTrozo)) {
					if (finCoincidencia-inicioCoincidencia == 0) {
						inicioCoincidencia = indexTrozo-1;
						finCoincidencia = indexTrozo;
					} else { 
						finCoincidencia = indexTrozo;
					}
				} else {
					if (finCoincidencia-inicioCoincidencia>=this.repeticiones-1) {
						if (lastTrozo.length()*currentLength>=this.repeticiones) {
							StringBuilder clausuraLastTrozo = zip(state, lastTrozo, 1);
							nueva = nueva + "{" + clausuraLastTrozo + "}";
						} else {
							nueva = nueva + "{" + lastTrozo + "}";
						}
					} else if (finCoincidencia-inicioCoincidencia==1) {
						nueva = nueva + lastTrozo + lastTrozo;
					} else {
						nueva = nueva + lastTrozo;
					}
					inicioCoincidencia = 0;
					finCoincidencia = 0;
				}
			}
			lastTrozo = trozo;
			start = start+currentLength;
		}
		
		if (finCoincidencia-inicioCoincidencia>1) {
			StringBuilder clausuraLastTrozo = zip(state, lastTrozo, 1);
			nueva = nueva + "{" + clausuraLastTrozo + "}";
		} else if (finCoincidencia-inicioCoincidencia==1) {
			start = currentLength*inicioCoincidencia;
			nueva = nueva + cadena.substring(start, cadena.length());
		} else {
			nueva = nueva + lastTrozo;
		}

		return new StringBuilder(nueva);
	}

	private int getParentesis(String masCorta) {
		int r = 0;
		for (int i=0; i<masCorta.length(); i++)
			if (masCorta.charAt(i)=='{')
				r++;
		return r;
	}
}
