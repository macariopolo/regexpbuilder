package edu.uclm.esi.regexpbuilder.model.equations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import edu.uclm.esi.regexpbuilder.model.Automata;
import edu.uclm.esi.regexpbuilder.model.ManagedTransitionZipper;
import edu.uclm.esi.regexpbuilder.model.Reloj;
import edu.uclm.esi.regexpbuilder.model.SessionManager;
import edu.uclm.esi.regexpbuilder.model.Solver;
import edu.uclm.esi.regexpbuilder.model.re.ILabel;
import edu.uclm.esi.regexpbuilder.model.re.RightMember;

public class HeuristicEquationsSolver extends Solver {
	protected EquationSystem equationSystem;
	protected Map<Integer, Equation> equationsMap;
	private String regularExpression;
	
	public HeuristicEquationsSolver(Automata automata, SessionManager sessionManager, Reloj reloj) {
		super(automata, sessionManager, reloj);
		automata.buildEquations();
		this.equationSystem = automata.getEquationSystem();
	}

	public void solve() {
		sessionManager.sendProgress("Sorting equations");
		reloj.start(Reloj.SORTING);
		List<Equation> sortedEquations = getSortedEquations(EquationsSorter.STATES_USES_LESS_FIRST);
		reloj.stop(Reloj.SORTING);
		
		reloj.start(Reloj.REMOVING_EMPTY_STATES);
		sessionManager.sendProgress("Removing empty states");
		Equation initialEquation = this.equationSystem.getInitialEquation();
		List<Integer> terminalStates = new ArrayList<>();
		if (initialEquation.getRightMembers().isEmpty()) {
			this.equationSystem.remove(initialEquation);
			for (Equation eq : sortedEquations) {
				sessionManager.sendSubprogress(-1, "Removing empty states in equation " + eq.getLeftMember().toString(), 0.50);
				if (eq.getStatesUsed().contains(initialEquation.getLeftMember().getId())) {
					eq.removeState(initialEquation.getLeftMember());
					if (eq.getNumberOfStatesUsed()==0)
						terminalStates.add(eq.getLeftMember().getId());
				}
			}
		}
		reloj.stop(Reloj.REMOVING_EMPTY_STATES);
		
		reloj.start(Reloj.REMOVING_LAMBDAS);
		sessionManager.sendProgress("Removing lambdas");
		boolean solved;
		do {
			solved = true;
			int terminalState;
			for (Equation eq : sortedEquations) {
				int size = terminalStates.size();
				sessionManager.sendSubprogress(-1, "Removing lambdas in equation " + eq.getLeftMember().toString(), 0.50);
				for (int i=size-1; i>=0; i--) {
					terminalState = terminalStates.get(i);
					if (eq.getStatesUsed().contains(terminalState)) {
						eq.replaceStateInOtherEquation(terminalState);
						if (eq.getNumberOfStatesUsed()==0 && !terminalStates.contains(eq.getLeftMember().getId())) {
							terminalStates.add(eq.getLeftMember().getId());
							solved = false;
						}
					}
				}
			}
		} while (!solved);
		reloj.stop(Reloj.REMOVING_LAMBDAS);
		
		reloj.start(Reloj.SOLVING);
		List<Equation> finalEquations = this.equationSystem.getFinalEquations();
		for (Equation eq : finalEquations)
			this.develop(eq);
		reloj.stop(Reloj.SOLVING);
		
		this.setRegularExpression(finalEquations);
	}

	private void develop(Equation eq) {
		eq.replaceStatesUsed(3);
		eq.removeStates();
		removeRepeated(eq);
		int size = eq.getRightMembers().size();
		RightMember rm;
		ManagedTransitionZipper mtz = new ManagedTransitionZipper();
		for (int i=size-1; i>=0; i--) {
			rm = eq.getRightMembers().get(i);
			String srm = rm.toString();
			String zipped = mtz.zip(null, srm).toString();
			ILabel label = mtz.buildTransition(zipped);
			eq.getRightMembers().set(i, (RightMember) label);
		}
		removeProductions(eq.getRightMembers().getRightMembers());
		removeRepeated(eq);
	}

	private void removeRepeated(Equation eq) {
		List<RightMember> newRightMembers = new ArrayList<>();
		RightMember rm;
		for (int i=0; i<eq.getRightMembers().size(); i++) {
			rm = eq.getRightMembers().get(i);
			if (!newRightMembers.contains(rm))
				newRightMembers.add(rm);
		}
		eq.getRightMembers().clear();
		for (int i=0; i<newRightMembers.size(); i++)
			eq.addRightMember(newRightMembers.get(i));
	}

	private void removeProductions(List<RightMember> rightMembers) {
		RightMember left, right;
		for (int i=0; i<rightMembers.size(); i++) {
			left = rightMembers.get(i);
			for (int j=i+1; j<rightMembers.size(); j++) {
				right = rightMembers.get(j);
				System.out.println(left + " x " + right);
				if (left.equals(right) || left.produces(right)) {
					rightMembers.remove(j);
					j=j-1;
				}
			}
		}
 	}

	public void setEquationSystem(EquationSystem equationSystem) {
		this.equationSystem = equationSystem;
		this.equationsMap = equationSystem.getEquationsMap();
	}
	
	protected List<Equation> getSortedEquations(int mode) {
		List<Equation> sortedEquations = this.equationSystem.getEquations();
		EquationsSorter equationSorter=new EquationsSorter(mode);
		Collections.sort(sortedEquations, equationSorter);
		return sortedEquations;
	}

	public void printEquations() {
		System.out.println(equationSystem.toString());
	}

	protected void printEquations(String title, List<Equation> sortedEquations) {
		System.out.println("\n" + title);
		for (int i=0; i<title.length()-1; i++)
			System.out.print("--");
		System.out.println("-");
		for (Equation eq : sortedEquations)
			System.out.println(eq.toString());
	}
	
	@Override
	public String getRegularExpression() {
		return this.regularExpression;
	}

	private void setRegularExpression(List<Equation> finalEquations) {
		this.regularExpression = "";
		StringBuilder sb = new StringBuilder();
		for (Equation eq : finalEquations) {
			eq.simplify();
			String s = eq.getRightMembers().toString();
			sb.append(this.regularExpression + s + " | ");
		}
		this.regularExpression = sb.toString();
		if (this.regularExpression.length()>3)
			this.regularExpression = this.regularExpression.substring(0, this.regularExpression.length()-3);
	}
}