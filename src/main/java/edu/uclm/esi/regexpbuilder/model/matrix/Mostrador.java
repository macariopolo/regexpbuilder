package edu.uclm.esi.regexpbuilder.model.matrix;

public class Mostrador {

	public static void main(String[] args) {
		for (int i=1; i<=5; i++)
			for (int j=1; j<=5; j++)
				for (int k=1; k<=5; k++) 
					System.out.println(print(i, j, k));
	}

	public static String print(int i, int j, int k) {
		String r = r(i, j, k) + "=" + r(i,j,k-1) + "+" + r(i,k,k-1) + "·(" + r(k, k, k-1) + ")*·" + r(k,j,k-1);
		return r;		
	}

	public static String r(int i, int j, int k) {
		return "R[" + i + "," + j + "," + k + "]";
	}

}
