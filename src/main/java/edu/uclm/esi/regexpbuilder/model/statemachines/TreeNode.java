package edu.uclm.esi.regexpbuilder.model.statemachines;

import java.util.ArrayList;
import java.util.List;

import edu.uclm.esi.regexpbuilder.model.Transition;
import edu.uclm.esi.regexpbuilder.model.re.ILabel;
import edu.uclm.esi.regexpbuilder.model.re.State;

public class TreeNode {
	private static int ID = 0;
	private int id;
	private TreeNode parent;
	private State state;
	private ILabel arrivalSymbol;
	private List<TreeNode> children;
	private List<TreeNode> leaves;

	public TreeNode(State state) {
		this.state = state;
		this.id = ID++;
		this.children = new ArrayList<>();
	}
	
	public TreeNode(ILabel symbol, State childState) {
		this(childState);
		this.arrivalSymbol = symbol;
	}

	public TreeNode addChild(ILabel symbol, State childState) {
		TreeNode child = new TreeNode(symbol, childState);
		child.parent = this;
		this.children.add(child);
		return child;
	}
	
	public void addChildren(List<IOPairTable> tables) {
		List<Transition> transitions = this.state.getOutputTransitionsList();
		for (Transition t : transitions) {
			ILabel symbol = t.getSymbol();
			State target = t.getTarget();
			TreeNode child = this.addChild(symbol, target);
			child.addChildren(this.state, symbol, tables);
		}
	}

	public void addChildren(State preState, ILabel preSymbol, List<IOPairTable> tables) {
		List<Transition> transitions = this.state.getOutputTransitionsList();
		for (Transition t : transitions) {
			ILabel symbol = t.getSymbol();
			State target = t.getTarget();
			TreeNode child = null;
			IOPairTable table = findTable(tables, t.getSource());
			IOPair pair = table.findPair(preState, target, preSymbol, symbol);
			if (pair!=null) {
				pair.visit();
				child = this.addChild(symbol, target);
			}
			if (child!=null) 
				child.addChildren(this.state, symbol, tables);
		}	
		if (this.children.isEmpty())
			this.getRoot().addLeaf(this);
	}

	private void addLeaf(TreeNode leaf) {
		if (this.leaves==null)
			this.leaves = new ArrayList<>();
		this.leaves.add(leaf);		
	}
	
	public List<TreeNode> getLeaves() {
		return leaves;
	}

	private TreeNode getRoot() {
		if (this.parent==null)
			return this;
		return this.parent.getRoot();
	}

	private IOPairTable findTable(List<IOPairTable> tables, State state) {
		for (int i=0; i<tables.size(); i++)
			if (tables.get(i).getState()==state)
				return tables.get(i);
		return null;
	}

	@Override
	public String toString() {
		return (this.arrivalSymbol!=null ? this.arrivalSymbol : "") + this.state.toString() + " (" + this.id + ")";
	}
	
	public State getState() {
		return state;
	}

	public String getSequence() {
		List<ILabel> symbols = new ArrayList<>();
		symbols.add(arrivalSymbol);
		TreeNode parent = this.parent;
		while (parent.arrivalSymbol!=null) {
			symbols.add(0, parent.arrivalSymbol);
			parent = parent.parent; 
		}
		String r = "";
		for (ILabel s : symbols)
			r+= s.toString();
		return r;
	}
}
