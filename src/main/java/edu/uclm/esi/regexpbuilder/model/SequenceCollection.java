package edu.uclm.esi.regexpbuilder.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SequenceCollection implements Iterable<Sequence>, IThreadManager {
	private List<Sequence> sequences;
	private Map<Integer, SubsequenceMap> maps;
	private boolean separated;
	private Automata automata;
	private SessionManager sessionManager;

	public SequenceCollection(int length) {
		this.sequences=new Vector<>(length);
		this.maps = new HashMap<>(); 
	}
	
	public SequenceCollection(String[] cadenas, boolean separated, SessionManager sessionManager) {
		this(cadenas.length);
		this.sessionManager = sessionManager;
		this.separated = separated;
		REThread[] threads = sessionManager.createThreads(SequenceBuilder.class, cadenas);
		sessionManager.sendProgress("Loading sequences");
		for (int i=0; i<threads.length; i++) {
			SequenceBuilder sequenceBuilder = (SequenceBuilder) threads[i];
			sequenceBuilder.setSequences(this.sequences);
			sequenceBuilder.setSeparated(this.separated);
			sequenceBuilder.setSession(sessionManager.getSession());
			sequenceBuilder.start();
		}
		
		for (int i=0; i<threads.length; i++)
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	}
	
	@Override
	public Iterator<Sequence> iterator() {
		return this.sequences.iterator();
	}
	
	public void buildAutomata(String numberOfStates, SessionManager manager, Reloj reloj) {
		reloj.start(Reloj.ADD_SEQUENCES_TO_AUTOMATON);
		this.automata = numberOfStates.equals("one") ? new Automata1(this.separated, sessionManager, reloj) : new AutomataN(separated, sessionManager, reloj);
		REThread[] threads = manager.createThreads(SequenceAdder.class, sequences.toArray());
		for (int i=0; i<threads.length; i++) {
			SequenceAdder sequenceAdder = (SequenceAdder) threads[i];
			sequenceAdder.setAutomata(automata);
			sequenceAdder.setSession(manager.getSession());
			sequenceAdder.start();
		}
		for (int i=0; i<threads.length; i++)
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		reloj.stop(Reloj.ADD_SEQUENCES_TO_AUTOMATON);
		automata.groupTransitions();
		automata.zipTransitions();
	}
	
	public Automata getAutomata() {
		return automata;
	}
	
	public void getFrequencies() {
		Collections.sort(this.sequences, new SequencesSorterByLength(SequencesSorterByLength.LONGEST_FIRST));
		int maxLength = this.sequences.get(0).length() / 3;
		for (Sequence sequence : this.sequences) {
			for (int currentLength=2; currentLength<=maxLength; currentLength++) {
				SubsequenceMap map = this.maps.get(currentLength);
				if (map == null) {
					map = new SubsequenceMap(currentLength);
					this.maps.put(currentLength, map);
				}
				sequence.analyze(map);
			}
		}
	}
	
	public JSONObject toJSON() throws JSONException {
		JSONObject jso = new JSONObject();
		JSONArray jsa = new JSONArray();
		Collection<SubsequenceMap> cMaps = this.maps.values();
		for (SubsequenceMap map : cMaps) {
			JSONObject jsoMap = new JSONObject();
			jsoMap.put("length", map.getLength());
			jsoMap.put("frequencies", map.toJSON());
			jsa.put(jsoMap);
		}
		jso.put("frequencies", jsa);
		return jso;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		Collection<SubsequenceMap> cMaps = this.maps.values();
		for (SubsequenceMap map : cMaps) {
			sb.append("\nLength " + map.getLength() + "\n");
			sb.append(map.toString());
		}
		return sb.toString();
	}

	public List<Sequence> getSequences() {
		return this.sequences;
	}

	public void sortByEnd() {
		List<Sequence> reversed = new ArrayList<>();
		for (Sequence sequence : this.sequences)
			reversed.add(sequence.reverse());
		Collections.sort(reversed, new SequencesAlphabeticalSorter(SequencesAlphabeticalSorter.Z_TO_A));
		this.sequences.clear();
		for (Sequence sequence : reversed)
			this.sequences.add(sequence.reverse());
	}

	public void sortByStart() {
		Collections.sort(this.sequences, new SequencesAlphabeticalSorter(SequencesAlphabeticalSorter.A_TO_Z));
	}

	public void sortByLength(boolean shortestFirst) {
		if (shortestFirst)
			Collections.sort(this.sequences, new SequencesSorterByLength(SequencesSorterByLength.SORTHEST_FIRST));
		else
			Collections.sort(this.sequences, new SequencesSorterByLength(SequencesSorterByLength.LONGEST_FIRST));
	}
	
	public boolean contains(Sequence seq) {
		return this.sequences.contains(seq);
	}
	
	public void add(Sequence seq) {
		this.sequences.add(seq);
	}
	
	private int currentIndex;
	
	@Override
	public synchronized void reset() {
		this.currentIndex=0;
	}

	@Override
	public synchronized Sequence giveMeNext() {
		if (this.currentIndex<this.sequences.size()) {
			Sequence sequence = this.sequences.get(currentIndex++);
			sequence.setIndex(currentIndex);
			return sequence;
		}
		return null;
	}

	@Override
	public int size() {
		return this.sequences.size();
	}
}
