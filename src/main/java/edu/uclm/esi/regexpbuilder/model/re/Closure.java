package edu.uclm.esi.regexpbuilder.model.re;

import java.util.List;
import java.util.Map;

import edu.uclm.esi.regexpbuilder.model.equations.Equation;
import edu.uclm.esi.regexpbuilder.model.equations.RMCounter;

public class Closure extends RightMember implements ILabel {
	private char operator;
	private RightMember closured;

	public Closure(RightMember closured) {
		this.operator = '*';
		this.closured = closured;
		this.closured.setParent(this);
	}
	
	public Closure(ILabel closured, char operator) {
		this.operator = operator;
		this.closured = (RightMember) closured;
		this.closured.setParent(this);
	}

	public RightMember getClosured() {
		return closured;
	}

	@Override
	public String toString() {
		String s = closured.toString();
		if (s.startsWith("(") && s.endsWith(")") && s.indexOf('(', 1)==-1)
			return s + operator;
		if (s.length()==1)
			return s + this.operator;
		return "(" + s + ")" + this.operator;
	}

	@Override
	public int length() {
		return this.closured.length();
	}

	@Override
	public RightMember copy() {
		return new Closure(this.closured.copy());
	}

	@Override
	protected int depth() {
		return 1 + closured.depth();
	}
	
	@Override
	public void loadSubTrees(Map<String, RMCounter> subarboles, int weight) {
		int descendants = 1 + this.closured.descendants();
		if (descendants==weight) {
			String key = this.closured.toString();
			RMCounter rmc = subarboles.get(key);
			if (rmc==null) {
				rmc = new RMCounter(closured);
				subarboles.put(this.closured.toString(), rmc);
			}
			rmc.inc();
		} else if (descendants>weight) {
			this.closured.loadSubTrees(subarboles, weight);
		}
	}
	
	@Override
	protected int descendants() {
		return 1 + closured.descendants();
	}
	
	@Override
	public RightMember removeState(Equation equation, State state) {
		return new Closure(this.closured.removeState(equation, state));
	}

	@Override
	public RightMember replaceState(Equation equation, Equation replacementEq) {
		Closure result = new Closure(this.closured.replaceState(equation, replacementEq));
		return result.copy();
	}

	@Override
	public List<State> getStatesUsed() {
		return this.closured.getStatesUsed();
	}

	@Override
	public boolean contains(RightMember rightMember) {
		return this.closured.contains(rightMember);
	}
	
	@Override
	public RightMember getDistributivableNode() {
		return this.closured.getDistributivableNode();
	}

	@Override
	public RightMember trimUntil(Concat node) {
		return this;
	}
	
	@Override
	public void addToNull(RightMember rm, boolean byLeft) {
		this.closured.addToNull(rm, byLeft);
	}

	@Override
	public RightMember simplify() {
		if (closured instanceof Closure)
			return this.closured.simplify();
		Closure result = new Closure(this.closured.simplify());
		result.setOperator(this.operator);
		return result;
	}

	public void setOperator(char operator) {
		this.operator = operator;
	}
	
	@Override
	public RightMember sort() {
		Closure closure= new Closure(this.closured.sort());
		closure.operator=this.operator;
		return closure;
	}
	
	@Override
	public boolean completelyContains(RightMember rightMember) {
		return this.equals(rightMember) || this.closured.completelyContains(rightMember);
	}

	public char getOperator() {
		return this.operator;
	}

	@Override
	public String polish() {
		return this.operator + "(" + this.closured.polish() + ")";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((closured == null) ? 0 : closured.hashCode());
		result = prime * result + operator;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Closure other = (Closure) obj;
		if (closured == null) {
			if (other.closured != null)
				return false;
		} else if (!closured.equals(other.closured))
			return false;
		if (operator != other.operator)
			return false;
		return true;
	}

	@Override
	public RightMember loadSubTree(int currentLength) {
		return null;
	}

	@Override
	public boolean produces(RightMember rm) {
		if (rm instanceof Symbol || rm instanceof State)
			return this.closured.produces(rm);
		if (rm instanceof Concat) {
			int length = rm.length();
			String sRM = rm.toString();
			StringBuilder sb = new StringBuilder();
			for (int i=1; i<length; i++) {
				sb.append(this.closured.toString());
				if (sb.toString().equals(sRM))
					return true;
			}
		}
		return false;
	}
}
