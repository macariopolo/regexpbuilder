package edu.uclm.esi.regexpbuilder.model.statemachines;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import edu.uclm.esi.regexpbuilder.model.Transition;
import edu.uclm.esi.regexpbuilder.model.re.ILabel;
import edu.uclm.esi.regexpbuilder.model.re.State;

public class IOPairTable {
	private State state;
	private List<IOPair> pairs;
	
	public IOPairTable(State state) {
		this.state = state;
		this.pairs = new ArrayList<>();
	}

	public void add(IOPair pair) {
		this.pairs.add(pair);
	}
	
	public State getState() {
		return state;
	}
	
	@Override
	public String toString() {
		int cont = 1;
		String r = state.toString() + "\n---\n";
		for (IOPair pair : this.pairs)
			r = r + cont++ + ") " + pair.toString() + "\n";
		return r;
	}

	public IOPair findUnvisitedPair() {
		for (IOPair pair : this.pairs)
			if (pair.getVisits()==0)
				return pair;
		return null;
	}

	public IOPair findPair(State a, State b, ILabel preSymbol, ILabel symbol) {
		IOPair pair;
		for (int i=0; i<this.pairs.size(); i++) {
			pair = this.pairs.get(i);
			Transition input = pair.getInput();
			Transition output = pair.getOutput();
			if (input.getSource()==a && input.getSymbol()==preSymbol && output.getTarget()==b && output.getSymbol()==symbol && pair.getVisits()==0)
				return pair;
		}
		return null;
	}

	public JSONObject toJSON() throws JSONException {
		JSONObject jso = new JSONObject();
		jso.put("state", this.state.toString());
		JSONArray pairs = new JSONArray();
		for (IOPair pair : this.pairs)
			pairs.put(pair.toJSON());
		jso.put("pairs", pairs);
		return jso;
	}

	public List<IOPair> getPairs() {
		return this.pairs;
	}

	public boolean isEmpty() {
		return this.pairs.isEmpty();
	}
}
