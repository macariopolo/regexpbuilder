package edu.uclm.esi.regexpbuilder.model.equations;

public class Trio {
	public int posParIzdo, posComa, posParDcho;
	
	@Override
	public String toString() {
		return posParIzdo + ", " + posComa + ", " + posParDcho;
	}

	public void substract(int n) {
		posParIzdo-=2;
		posComa-=2;
		posParDcho-=2;
	}
}
