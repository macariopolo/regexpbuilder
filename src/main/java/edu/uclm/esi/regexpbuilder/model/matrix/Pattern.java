package edu.uclm.esi.regexpbuilder.model.matrix;

public class Pattern {
	private String pattern;
	private int n;
	
	public Pattern(String pattern) {
		this.pattern = pattern;
	}
	
	public void inc() {
		this.n++;
	}
	
	@Override
	public String toString() {
		return this.pattern;
	}
	
	public int getN() {
		return n;
	}
}
