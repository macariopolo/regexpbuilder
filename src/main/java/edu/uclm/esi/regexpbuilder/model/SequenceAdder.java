package edu.uclm.esi.regexpbuilder.model;

import edu.uclm.esi.regexpbuilder.web.ws.WSServerRegExp;

public class SequenceAdder extends REThread {
	private Automata automata;
	
	public SequenceAdder(int id, int start, int end, Object[] objectsToProcess) {
		super(id, start, end, objectsToProcess);
	}

	@Override
	public synchronized void start() {
		for (int i=start; i<end; i++) {
			Sequence sequence = (Sequence) objectsToProcess[i];
			automata.add(this.id, sequence, i, end);
		}
		WSServerRegExp.sendSubprogress(session, id, "idle", 1);
	}

	public void setAutomata(Automata automata) {
		this.automata = automata;
	}
}
