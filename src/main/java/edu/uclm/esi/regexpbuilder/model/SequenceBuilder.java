package edu.uclm.esi.regexpbuilder.model;

import java.util.List;

import edu.uclm.esi.regexpbuilder.web.ws.WSServerRegExp;

public class SequenceBuilder extends REThread {
	private List<Sequence> sequenceList;
	private boolean separated;
	
	public SequenceBuilder(int id, int start, int end, Object[] objectsToProcess) {
		super(id, start, end, objectsToProcess);
	}

	public void setSequences(List<Sequence> sequenceList) {
		this.sequenceList = sequenceList;
	}

	@Override
	public synchronized void start() {
		Sequence sequence;
		String cadena;
		for (int i=start; i<end; i++) {
			WSServerRegExp.sendSubprogress(session, id, "Processing sequence " + i + "/" + (end-1), 0.10);
			cadena = (String) objectsToProcess[i];
			cadena=cadena.trim();
			if (cadena.length()>0) {
				sequence = new Sequence(cadena, separated);
				if (!this.sequenceList.contains(sequence)) {
					this.sequenceList.add(sequence);
				}
			}
		}
		WSServerRegExp.sendSubprogress(session, id, "idle", 1);
	}

	public void setSeparated(boolean separated) {
		this.separated = separated;
	}
}
