package edu.uclm.esi.regexpbuilder.model.re;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import edu.uclm.esi.regexpbuilder.model.equations.Equation;
import edu.uclm.esi.regexpbuilder.model.equations.RMCounter;

public class Lambda extends RightMember implements ILabel {

	@Override
	public RightMember copy() {
		return new Lambda();
	}

	@Override
	public String toString() {
		return "€";
	}

	@Override
	public int length() {
		return 0;
	}

	@Override
	protected int depth() {
		return 0;
	}
	
	@Override
	public void loadSubTrees(Map<String, RMCounter> subarboles, int weight) {
		if (weight<=1) {
			String key = this.toString();
			RMCounter rmc = subarboles.get(key);
			if (rmc==null)
				rmc = new RMCounter(this);
			subarboles.put(this.toString(), rmc);
			rmc.inc();
		}
	}

	@Override
	public RightMember removeState(Equation equation, State state) {
		return this;
	}

	@Override
	public RightMember replaceState(Equation equation, Equation replacementEq) {
		return this.copy();
	}

	@Override
	public List<State> getStatesUsed() {
		return new ArrayList<>();
	}

	@Override
	public boolean contains(RightMember rightMember) {
		return rightMember.toString().equals(this.toString());
	}

	@Override
	public RightMember trimUntil(Concat node) {
		return this;
	}

	@Override
	public RightMember simplify() {
		return this;
	}
	
	@Override
	public boolean completelyContains(RightMember rightMember) {
		return rightMember instanceof Lambda;
	}

	@Override
	public String polish() {
		return "";
	}
	
	@Override
	public boolean equals(Object obj) {
		return obj instanceof Lambda;
	}

	@Override
	public int hashCode() {
		return 0;
	}

	@Override
	public RightMember loadSubTree(int currentLength) {
		return null;
	}

	@Override
	public boolean produces(RightMember rm) {
		return rm instanceof Lambda;
	}
}
