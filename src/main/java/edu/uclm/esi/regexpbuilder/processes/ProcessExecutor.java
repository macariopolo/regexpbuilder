package edu.uclm.esi.regexpbuilder.processes;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.json.JSONObject;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import edu.uclm.esi.regexpbuilder.model.Manager;

public class ProcessExecutor {
	private String workingFolder;
	private File outputFile;
	private File errorsFile;

	public ProcessExecutor(String projectName, boolean original) throws IOException {
		String projectFolder = Manager.get().getWorkingFolder() + projectName + File.separator;
		this.workingFolder = projectFolder + (original ? "SUA" : "instrumented") + File.separator + projectName + File.separator;
		Files.createDirectories(Paths.get(projectFolder + "logs"));
		this.outputFile = new File(projectFolder + "logs" + File.separator + "output.txt");
		this.errorsFile = new File(projectFolder + "logs" + File.separator + "errors.txt");
	}

	public void execute(WebSocketSession session, String command) throws Exception {
		String[] comandos = command.split(" ");
		
		ProcessBuilder pb=new ProcessBuilder(comandos);
		File workingDirectory=new File(this.workingFolder);
		pb.directory(workingDirectory);
		pb.redirectOutput(outputFile);
		pb.redirectError(errorsFile);
		
		Process process = pb.start();
		process.waitFor();
		
		FileReader fr=new FileReader(outputFile);
		String line=null;
		JSONObject jsoLine = new JSONObject();
		try (BufferedReader br=new BufferedReader(fr)) {
			jsoLine.put("type", "line");
			while ((line=br.readLine())!=null) {
				jsoLine.put("line", line);
				session.sendMessage(new TextMessage(jsoLine.toString()));
			}
		}

		fr=new FileReader(errorsFile);
		try(BufferedReader br=new BufferedReader(fr)) {
			line=null;
			while ((line=br.readLine())!=null)
				jsoLine.put("line", line);
			session.sendMessage(new TextMessage(jsoLine.toString()));
		}
	}

}
