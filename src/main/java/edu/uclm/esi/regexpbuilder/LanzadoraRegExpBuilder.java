package edu.uclm.esi.regexpbuilder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LanzadoraRegExpBuilder {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(LanzadoraRegExpBuilder.class, args);
	}

}
