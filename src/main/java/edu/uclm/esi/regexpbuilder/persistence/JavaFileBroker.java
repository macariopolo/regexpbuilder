package edu.uclm.esi.regexpbuilder.persistence;

import java.util.List;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

import edu.uclm.esi.regexpbuilder.model.Manager;

public class JavaFileBroker {
	List<String> imageBuffer = new ArrayList<String>();
	String destinationPath;
	File file;
	PrintWriter printWriter;
	
    private static JavaFileBroker singleInstance = new JavaFileBroker();
 
    private JavaFileBroker() {
		
    }
 
    public static JavaFileBroker getInstance() {
        return singleInstance;
    }
    
    public void addToBuffer(String image){
		imageBuffer.add(image);
	}

	public void addToBuffersStart(String image){
		imageBuffer.add(0, image);
	}
	
	public void flushBuffer() {
		Iterator<String> iterator = imageBuffer.listIterator();
	    while (iterator.hasNext()) {
	      String element = iterator.next();
	      try {
		      printWriter.print(formatForPrintWriter(element));
		      System.out.print(formatForPrintWriter(element));
	      }catch(Exception e) {
	    	  System.out.println("Error al imprimir en el archivo");
	      }
	    }
	    imageBuffer.clear();
	}
	
	public void printLines(String lines) {
		printWriter.print(formatForPrintWriter(lines));
		System.out.print(formatForPrintWriter(lines));
	}
	
	public void printMethodPreLine(String line) {
		flushBuffer();
		printWriter.println(formatForPrintWriter(line));
		System.out.print(formatForPrintWriter(line));
	}
	
	public void printMethodPostLine(String line, boolean closeTheBlock) {
		cutBufferTail();
		flushBuffer();
		printWriter.print(formatForPrintWriter(line /*+ "}"*/));
		System.out.print(formatForPrintWriter(line /*+ "}"*/));
		if(closeTheBlock) {
			printWriter.print(formatForPrintWriter("}"));
			System.out.print(formatForPrintWriter( "}"));
		}
	}
   
	public void printVariableLines(String lines) {
		printWriter.print(formatForPrintWriter(lines));
		System.out.print(formatForPrintWriter(lines));
		flushBuffer();
	}
	
	public void printLoopPreLines(String lines) {
		List<String> auxImageBuffer = new ArrayList<String>();
		auxImageBuffer.addAll(imageBuffer);
		imageBuffer = new ArrayList<String>();
		printWriter.print(formatForPrintWriter(lines));
		System.out.print(formatForPrintWriter(lines));
		flushBuffer();
		imageBuffer.addAll(auxImageBuffer);
		flushBuffer();
		
	}
	
	public void printLoopPostLines(String lines) {
		printWriter.print(formatForPrintWriter(lines));
		System.out.print(formatForPrintWriter(lines));
	}
	
	public void flushAndPrint(String line) {
		flushBuffer();
		System.out.print(formatForPrintWriter(line));
		printWriter.print(formatForPrintWriter(line));
	}
	
	public void printAndFlush(String line) {
		printWriter.print(formatForPrintWriter(line));
		System.out.print(formatForPrintWriter(line));
		flushBuffer();
	}
	
	private void cutBufferTail(){
		
		int cutIndex = getCutIndex();

		imageBuffer = imageBuffer.subList(0, cutIndex);

	}
	
	private int getCutIndex() {
		int index = 0, counter = 0;
		ListIterator<String> iterator = imageBuffer.listIterator();
	    while (iterator.hasNext()) {
	      String element = iterator.next();
	      if(element.equals("}")){
	    	  index = counter;
	      }
	      counter++;
	    }	    
	    return index;
	}

	public void setDestinationPath(String destinationPath) {
		flushBuffer();
		String auxi = destinationPath.replace('\\', File.separatorChar);
		auxi = auxi.substring(0, auxi.lastIndexOf(File.separator));
		new File(auxi).delete();
		new File(auxi).mkdirs();
		this.destinationPath = destinationPath;
		try {
			printWriter.close();
		}catch(Exception e) {
			// Siempre se pasa por aqu� en la primera llamada.
		}
		file = new File(destinationPath);
		try {
			file.createNewFile();
		}catch(Exception e) {
			System.out.println("Ha ocurrido un error al crear en archivo.java");
		}
		try {
			printWriter = new PrintWriter(file);
		}catch(FileNotFoundException e) {
			System.out.println("Ha habido un problema al crear el PrintWriter");
		}	
	}
	
	public void closeCurrentPrintWriter() {
		try {
			printWriter.close();
		}catch(Exception e) {
			System.out.println("Error al cerrar el PrintWriter");
		}
	}

	public String formatForPrintWriter(String oldString) {
		return oldString.replaceFirst("\\n", "\r\n");
	}
	
	public String getDestinationPath() {
		return destinationPath;
	}
	
	public List<String> getImageBuffer() {
		return imageBuffer;
	}

	public void setImageBuffer(List<String> imageBuffer) {
		this.imageBuffer = imageBuffer;
	}
	
	public static void copyTraceWriterFile(String target) {
		DataInputStream dis = null;
		DataOutputStream dos = null;
		String fileName = "";
		if(Manager.get().getInstrumentationKind().equals("inserta")) {
			fileName = "TraceWriterInsertion";
		}else {
			fileName = "TraceWriterAspects";
		}
		try{
			String outPutFile =  target + File.separator + "src" + File.separator + 
					"traceWriterSource" + File.separator + "TraceWriter" + File.separator + "TraceWriter.java";
			new File (outPutFile).createNewFile();
			dis = new DataInputStream(new FileInputStream(new File("resources" + File.separator + 
					"templates" + File.separator + "traceWriter" + File.separator + fileName + ".java.txt").getAbsolutePath()));
			dos = new DataOutputStream(new FileOutputStream(outPutFile));

			while(true){
				dos.writeByte(dis.readByte());
			}
		}catch(EOFException e){
			System.out.println("\n\n**********************TraceWriter file Succesfuly created**********************");
			try {
				dis.close();
				dos.close();
			}catch(Exception exception){
				System.out.println("Error in copyTraceWriterFile ==> "+e.toString());
			}
		}catch(Exception e){
			System.out.println("Error in copyTraceWriterFile ==> "+e.toString());
		}
	}
	
	public static void createAspectFile(String target, String fileName, String content) {
		//String outPut = aspectSerpar.build();

		DataInputStream dis = null;
		DataOutputStream dos = null;
		try{
			InputStream targetStream = new ByteArrayInputStream(content.getBytes());
			dis = new DataInputStream(targetStream);
			dos = new DataOutputStream(new FileOutputStream(target+"" + File.separator + "src" + File.separator 
					+ "traceWriterSource" + File.separator + fileName));

			while(true){
				dos.writeByte(dis.readByte());
			}
		}catch(EOFException e){
			System.out.println("\n\n**********************Aspects file Succesfuly created: " + fileName + "**********************");
			try {
				dis.close();
				dos.close();
			}catch(Exception exception){
				System.out.println("Error in createAspectFile ==> "+e.toString());
			}
		}catch(Exception e){
			System.out.println("Error in createAspectFile ==> "+e.toString());
		}
	}
}
