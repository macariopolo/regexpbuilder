package edu.uclm.esi.regexpbuilder.persistence;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;

public class PackageBroker {

    private static PackageBroker singleInstance = new PackageBroker();
    
    private PackageBroker() {
		
    }
 
    public static PackageBroker getInstance() {
        return singleInstance;
    }
	
	public void write(String outPutPath) {
		//System.out.println("Estamos en PackageBroker, Path: " + outPutPath);
		new File(outPutPath).mkdirs();
	}

	public static  void copyFolder(Path src, Path dest) throws IOException {
		Files.createDirectories(dest.getParent());
		Files.walk(src)
		.forEach(source -> copy(source, dest.resolve(src.relativize(source))));
	}

	private static void copy(Path source, Path dest) {
		try {
			Files.copy(source, dest);
		} catch (FileAlreadyExistsException faee) {
			// El directorio raíz ya existe
		} catch (DirectoryNotEmptyException dnee) {
			// No ha pasado nada
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage(), e);
		}
	}
	
	public static void createTraceWriterPackage(String target){
		try{

			File tPackage = new File(target + File.separator + "src" + File.separator + "traceWriterSource");
			if(!tPackage.exists()){
				Files.createDirectories(tPackage.toPath());
				System.out.println("\r\n\r\n**********************traceWriterSource Package Succesfuly created**********************");
			}

			tPackage = new File(target + File.separator + "src" + File.separator + "traceWriterSource" + File.separator + "serialization");
			if(!tPackage.exists()){
				Files.createDirectories(tPackage.toPath());
				System.out.println("\r\n\r\n**********************serialization Package Succesfuly created**********************");
			}

			tPackage = new File(target + File.separator + "src" + File.separator + "traceWriterSource" + File.separator + "traceWriter");
			if(!tPackage.exists()){
				Files.createDirectories(tPackage.toPath());
				System.out.println("\r\n\r\n**********************traceWriter Package Succesfuly created**********************");
			}
		}catch(Exception e){
			System.out.println("Error in createInstrumentationPackage ==> "+e.toString() + "\r\n");
			e.printStackTrace();
		}
	}
}
