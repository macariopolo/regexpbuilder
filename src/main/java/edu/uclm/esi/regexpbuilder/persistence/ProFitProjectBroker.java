package edu.uclm.esi.regexpbuilder.persistence;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import edu.uclm.esi.regexpbuilder.instrumentation.ProFitProject;
import edu.uclm.esi.regexpbuilder.model.Manager;

public class ProFitProjectBroker {

	static FileOutputStream serializedOutPutStream;
	static ObjectOutputStream objectOutPutStream;
	
    public static void writeSerializedInstance(ProFitProject pfProject){
		// Write objects to file
    	try {
    		serializedOutPutStream = new FileOutputStream(Manager.get().getWorkingFolder() + File.separator + "ProfitProject.txt");
    		objectOutPutStream = new ObjectOutputStream(serializedOutPutStream);
    		objectOutPutStream.writeObject(pfProject);
    		objectOutPutStream.flush();
    	}catch(IOException ioe) {
    		System.err.print("ProFitProjectBroker: IOException!");
    	}catch(Exception e) {
    		System.err.print("ProFitProjectBroker: Exception!");
    	}
    }
}
