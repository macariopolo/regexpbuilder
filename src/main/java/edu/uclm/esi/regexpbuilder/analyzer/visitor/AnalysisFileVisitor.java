package edu.uclm.esi.regexpbuilder.analyzer.visitor;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.StringTokenizer;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.tree.ClassNode;

import edu.uclm.esi.regexpbuilder.analyzer.parser.InstrumentationManager;
import edu.uclm.esi.regexpbuilder.analyzer.parser.ProFitParser;
import edu.uclm.esi.regexpbuilder.instrumentation.Clazz;
import edu.uclm.esi.regexpbuilder.instrumentation.ProFitAttribute;
import edu.uclm.esi.regexpbuilder.instrumentation.ProFitClass;
import edu.uclm.esi.regexpbuilder.instrumentation.ProFitMethod;
import edu.uclm.esi.regexpbuilder.instrumentation.ProFitPackage;
import edu.uclm.esi.regexpbuilder.instrumentation.ProFitProject;
import edu.uclm.esi.regexpbuilder.persistence.ProFitProjectBroker;

public class AnalysisFileVisitor {
	Path rootPath;
	InstrumentationManager manager = InstrumentationManager.getInstance();

	public AnalysisFileVisitor(String rootPath) {
		super();
		this.rootPath = Paths.get(rootPath);
	}
	
	public ProFitProject walkProject() {
		ProFitProject project = new ProFitProject();
		String projectFolder = rootPath.toString();
		File file = new File(projectFolder);
		File[] dirs = file.listFiles();
		try {
			for (int i=0; i<dirs.length; i++) {
				if(dirs[i].getName().equals("SUA")) {
					file = dirs[i].listFiles()[0];
					project.setName(file.getName());
				}
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		//project.setName(rootPath.getFileName().toString());
		IOFileFilter fileFilter = FileFilterUtils.suffixFileFilter("java", IOCase.INSENSITIVE);
		IOFileFilter dirFilter = TrueFileFilter.INSTANCE;
		Collection<File> classFiles = FileUtils.listFiles(new File(projectFolder), fileFilter, dirFilter);
		for (File classFile : classFiles) {
			System.out.println("Procesando " + classFile.getAbsolutePath());
			String[] args = new String[1];
    			args[0]=classFile.getPath();
    			manager.cleanBuffers();
    			ProFitParser.main(args);
            	ProFitClass myClass = new ProFitClass(manager.getParameterBuffer(), manager.getMethodBuffer(), classFile.getPath());
            	project.add(myClass);
		}
		return project;
	}
	
	public ProFitProject oldWalkProject() {
		ArrayList<ProFitPackage> packages = new ArrayList<ProFitPackage>();
		ArrayList<ProFitClass> classes = new ArrayList<ProFitClass>();
		
		File folder = new File(rootPath.toString());
		listFilesForProject(folder, packages);
		
		ProFitProject project = new ProFitProject(packages, classes, rootPath.toString());
		ProFitProjectBroker.writeSerializedInstance(project); 
		
		return project;
	}
	
	public void listFilesForProject(final File folder, ArrayList<ProFitPackage> packages) {
		for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
		        	ArrayList<ProFitPackage> myPackages = new ArrayList<ProFitPackage>();
		        	ArrayList<ProFitClass> myClasses = new ArrayList<ProFitClass>();
	            listFilesForFolder(fileEntry, myPackages, myClasses);
	            ProFitPackage myPackage = new ProFitPackage(myPackages, myClasses, fileEntry.getPath());
	            packages.add(myPackage);
	        }
	    }
	}
	
	public void listFilesForFolder(final File folder, ArrayList<ProFitPackage> packages, ArrayList<ProFitClass>classes) {
		for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	        		ArrayList<ProFitPackage> myPackages = new ArrayList<ProFitPackage>();
		        	ArrayList<ProFitClass> myClasses = new ArrayList<ProFitClass>();
	            listFilesForFolder(fileEntry, myPackages, myClasses);
	            ProFitPackage myPackage = new ProFitPackage(myPackages, myClasses, fileEntry.getPath());
	            packages.add(myPackage);
	        } else {
	            if (fileEntry.isFile()) {
	               if(isJavaFile(fileEntry.getPath())) {
	                 String[] args = new String[1];
		        		args[0]=fileEntry.getPath();
		        		manager.cleanBuffers();
		            	ProFitParser.main(args);
		            	System.out.println("Procesando " + fileEntry.getAbsolutePath());
		            	ProFitClass myClass = new ProFitClass(manager.getParameterBuffer(), manager.getMethodBuffer(), fileEntry.getPath());
		            	classes.add(myClass);
		            	System.out.println("\t" + classes.size() + " en classes");
		            	manager.setParameterBuffer(new ArrayList<ProFitAttribute>());
		            	manager.setMethodBuffer(new ArrayList<ProFitMethod>());
	              }
	            }
	        }
	    }
	}
	private static  boolean isJavaFile(String path){
	    String lastItem = "";
	    StringTokenizer stz= new StringTokenizer(path,".");
	    while(stz.hasMoreTokens()) lastItem = stz.nextToken();
	    return (lastItem.equalsIgnoreCase("java"));
	}
}
