package edu.uclm.esi.regexpbuilder.analyzer.parser;
import java.nio.file.Path;
import java.util.ArrayList;

import edu.uclm.esi.regexpbuilder.instrumentation.ProFitAttribute;
import edu.uclm.esi.regexpbuilder.instrumentation.ProFitClass;
import edu.uclm.esi.regexpbuilder.instrumentation.ProFitMethod;
import edu.uclm.esi.regexpbuilder.instrumentation.ProFitProject;

public class InstrumentationManager {
    ArrayList<ProFitAttribute> parameterBuffer;
    ArrayList<ProFitMethod> methodBuffer;
    ProFitProject project;
    Path OutPutPath;
    Path OutPutRootPath;
    String locale = "es";
    ProFitClass classUnderAnalysis;
    ArrayList<ProFitAttribute> projectParameters = new ArrayList<ProFitAttribute>();
    int classAcount = 0;
	int classToInstrumentAcount = 0;
    int logFileCount = 1;
    boolean areWeInsideAMethod = false;
    ArrayList<ProFitClass> instrumentableClassesList = new ArrayList<ProFitClass>();
    

	private static InstrumentationManager singleInstance = new InstrumentationManager();
 
    private InstrumentationManager() {
    	parameterBuffer = new ArrayList<ProFitAttribute>();
    	methodBuffer = new ArrayList<ProFitMethod>();
    }
 
    public static InstrumentationManager getInstance() {
        return singleInstance;
    }
    
	public void cleanBuffers() {
		parameterBuffer.clear();
    	methodBuffer.clear();
	}
	
	public ArrayList<ProFitAttribute> getProjectParameters() {
		return projectParameters;
	}

	public int getClassToInstrumentCount() {
		return classToInstrumentAcount;
	}

	public void setClassToInstrumentCount(int classToInstrumentCount) {
		this.classToInstrumentAcount = classToInstrumentCount;
	}

	public ProFitClass getClassUnderAnalysis() {
		return classUnderAnalysis;
	}

	public void setClassUnderAnalysis(ProFitClass classUnderAnalysis) {
		this.classUnderAnalysis = classUnderAnalysis;
	}

	public ArrayList<ProFitAttribute> getParameterBuffer() {
		return parameterBuffer;
	}

	public void setParameterBuffer(ArrayList<ProFitAttribute> parameterBuffer) {
		this.parameterBuffer = parameterBuffer;
	}

	public ArrayList<ProFitMethod> getMethodBuffer() {
		return methodBuffer;
	}

	public void setMethodBuffer(ArrayList<ProFitMethod> methodBuffer) {
		this.methodBuffer = methodBuffer;
	}

	public ProFitProject getProject() {
		return project;
	}

	public void setProject(ProFitProject project) {
		this.project = project;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public int getClassAcount() {
		return classAcount;
	}

	public void setClassAcount(int classAcount) {
		this.classAcount = classAcount;
	}

	public boolean isAreWeInsideAMethod() {
		return areWeInsideAMethod;
	}

	public void setAreWeInsideAMethod(boolean areWeInsideAClass) {
		this.areWeInsideAMethod = areWeInsideAClass;
	}

	public int getLogFileCount() {
		return logFileCount;
	}

	public void setLogFileCount(int logFileCount) {
		this.logFileCount = logFileCount;
	}

	public Path getOutPutRootPath() {
		return OutPutRootPath;
	}

	public void setOutPutRootPath(Path outPutRootPath) {
		OutPutRootPath = outPutRootPath;
	}

	public Path getOutPutPath() {
		return OutPutPath;
	}

	public void setOutPutPath(Path outPutPath) {
		OutPutPath = outPutPath;
	}

	public ArrayList<ProFitClass> getInstrumentableClassesList() {
		return instrumentableClassesList;
	}

	public void setInstrumentableClassesList(ArrayList<ProFitClass> instrumentableClassesList) {
		this.instrumentableClassesList = instrumentableClassesList;
	}
	
} 

