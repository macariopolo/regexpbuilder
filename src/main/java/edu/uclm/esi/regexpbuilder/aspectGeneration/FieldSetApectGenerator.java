package edu.uclm.esi.regexpbuilder.aspectGeneration;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import edu.uclm.esi.regexpbuilder.analyzer.parser.InstrumentationManager;
import edu.uclm.esi.regexpbuilder.instrumentation.ProFitAttribute;
import edu.uclm.esi.regexpbuilder.instrumentation.ProFitClass;
import edu.uclm.esi.regexpbuilder.instrumentation.ProFitProject;
import edu.uclm.esi.regexpbuilder.model.Manager;

public class FieldSetApectGenerator extends AbstractAspectBroker{

	AspectsUtil aspectsUtil = AspectsUtil.getInstance();
	String buffer = "";
	
	public FieldSetApectGenerator(JSONArray classes) {
		super(classes);
	}
	
	String pointCut() throws JSONException {
		buffer = "";
		String outPut = "";
		boolean isFirst = true;
		for (int i=0; i<classes.length(); i++) {
			JSONObject jsElement = classes.getJSONObject(i);
			ProFitClass element = new ProFitClass(jsElement.getJSONArray("methods"), jsElement.getString("name"));
			if (jsElement.getBoolean("checked") && !attributePointCutPart(element, isFirst)) {
				isFirst = false;
			}
		}
		outPut += buffer;
		return outPut;
	}

	String getAspectType() {
		return "FieldSetAspect";
	}
	
	String actionBefore() {
		return aspectsUtil.outPutOptions("fieldSet", "before", "");
	}

	String actionAfter() {
		return aspectsUtil.outPutOptions("fieldSet", "after", "");
	}
	
	private void attributesPointcut() {
		if(manager.getProject().getConfiguration().getParameterConfiguration().isInstrumentParameters()) {
			Iterator<ProFitClass> iterator = manager.getInstrumentableClassesList().iterator();
			buffer += System.getProperty("line.separator");
			buffer += "\tpublic pointcut AttributeOperation() : (";
			boolean isFirst = true;
			while(iterator.hasNext()) {
				ProFitClass element = (ProFitClass) iterator.next();
				if(element.isInstrumentationTarget()) {
					if(!attributePointCutPart(element, isFirst)) {
						isFirst = false;
					}
				}
			}
			//buffer += System.getProperty("line.separator");
			buffer += "\t);";
		}
	}

	private boolean attributePointCutPart(ProFitClass pfClass, boolean isFirst) {
		float percentage = pfClass.getInstrumentableChildrenFieldPercentage();
		if(percentage == 100) {
			if(!attributeFullPointCutPart(pfClass, isFirst)) {
				isFirst = false;
			}
		}else if(percentage == 0) {
			// No instrumentamos
		}else if(percentage > 50) {
			if(!attributeSubstractivePointCutPart(pfClass, isFirst)) {
				isFirst = false;
			}
		}else {
			if(!attributeAditivePointCutPart(pfClass, isFirst)) {
				isFirst = false;
			}
		}
		return isFirst;
	}

	private boolean attributeSubstractivePointCutPart(ProFitClass pfClass, boolean isFirst) {
		Iterator iterator = pfClass.getAttributes().iterator();
		if(isFirst) {
			//buffer += System.getProperty("line.separator");
			buffer += "\t\t   ";
			isFirst = false;
		}else {
			//buffer += System.getProperty("line.separator");
			buffer += "\t\t|| ";
		}
		
		
		buffer += "( (set(* "
				+ pfClass.getName()
				+ ".*))";
		
		while(iterator.hasNext()) {
			ProFitAttribute pfAttr = (ProFitAttribute) iterator.next();
			if(!pfAttr.isInstrumentationTarget()) {
				//buffer += System.getProperty("line.separator");
				buffer += "\t\t&& ";
				
				buffer += "( !set(* "
						+ aspectsUtil.splitExtension(pfClass.getName())
						+ "."
						+ pfAttr.getName()
						+ "(..)) )";
			}
		}
		buffer += ")";
		return isFirst;
	}
	
	private boolean attributeFullPointCutPart(ProFitClass pfClass, boolean isFirst) {
		if(isFirst) {
			isFirst = false;
			//buffer += System.getProperty("line.separator");
			buffer += "\t\t   ";
		}else {
			//buffer += System.getProperty("line.separator");
			buffer += "\t\t|| ";
		}
		buffer += 
				  "( set(* "
				+ aspectsUtil.splitExtension(pfClass.getName())
				+ ".*))";
		return isFirst;
	}
	
	
	
	private boolean attributeAditivePointCutPart(ProFitClass pfClass, boolean isFirst){
		Iterator iterator = pfClass.getAttributes().iterator();
		while(iterator.hasNext()) {
			ProFitAttribute pfAttribute = (ProFitAttribute) iterator.next();
			if(pfAttribute.isInstrumentationTarget()) {
				if(isFirst) {
					isFirst = false;
					//buffer += System.getProperty("line.separator");
					buffer += "\t\t   ";
				}else {
					//buffer += System.getProperty("line.separator");
					buffer += "\t\t|| ";
				}
				buffer += 
						  "( set(* "
						+ aspectsUtil.splitExtension(pfClass.getName())
						+ "."
						+ pfAttribute.getName()
						+ "))";	
			}
		}	
		return isFirst;
	}
	
}
