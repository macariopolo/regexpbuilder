package edu.uclm.esi.regexpbuilder.aspectGeneration;

import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import edu.uclm.esi.regexpbuilder.instrumentation.ProFitClass;
import edu.uclm.esi.regexpbuilder.instrumentation.ProFitMethod;

public class DynamicMethodAspectBroker extends AbstractAspectBroker{
	AspectsUtil aspectsUtil = AspectsUtil.getInstance();
	String buffer = "";
	
	public DynamicMethodAspectBroker(JSONArray classes) {
		super(classes);
	}

	String pointCut() throws JSONException {
		buffer = "";
		String outPut = "";
		boolean isFirst = true;
		
		for (int i=0; i<classes.length(); i++) {
			JSONObject jsElement = classes.getJSONObject(i);
			ProFitClass element = new ProFitClass(jsElement.getJSONArray("methods"), jsElement.getString("name"));
			if (jsElement.getBoolean("checked")) 
				isFirst = methodPointCutPart(element, isFirst);
		}
		outPut += buffer;
		return outPut;
	}

	String getAspectType() {
		return "DynamicMethodAspect";
	}

	public String actionBefore() {
		return aspectsUtil.outPutOptions("methodCall", "before", "dynamic");
	}

	public String actionAfter() {
		return aspectsUtil.outPutOptions("methodCall", "after", "dynamic");
	}

	private boolean methodPointCutPart(ProFitClass pfClass, boolean isFirst ) {
		float percentage = pfClass.getInstrumentableChildrenMethodPercentage();
		if(percentage == 100) {
			if(!methodFullPointCutPart(pfClass, isFirst)) {
				isFirst = false;
			}
		}else if(percentage == 0) {
			// No instrumentamos
			return isFirst;
		}else if(percentage > 50) {
			if(!methodSubtractivePointCutPart(pfClass, isFirst)) {
				isFirst = false;				
			}
		}else {
			if(!methodAditivePointCutPart(pfClass, isFirst)) {
				isFirst = false;				
			}
		}
		//buffer += System.getProperty("line.separator");
		buffer += "\t\t|| ( execution("
				+ aspectsUtil.splitExtension(pfClass.getName())
				+ ".new(..)) && target(object))";
		return isFirst;
	}
	
	private boolean methodSubtractivePointCutPart(ProFitClass pfClass, boolean isFirst) {
		Iterator<ProFitMethod> iterator = pfClass.getMethods().iterator();
		if (isFirst) {
			buffer += "\t\t   ";
			isFirst = false;
		} else {
			buffer += "\t\t|| ";
		}
		
		buffer += "( (execution(* "
				+ aspectsUtil.splitExtension(pfClass.getName())
				+ ".*(..)) && target(object))";
		
		while (iterator.hasNext()) {
			ProFitMethod pfMethod = iterator.next();
			if (!pfMethod.isInstrumentationTarget()) {
				buffer += "\t\t&& ";
				buffer += "( !execution(* "
						+ aspectsUtil.splitExtension(pfClass.getName())
						+ "."
						+ pfMethod.getName()
						+ "(..)) && target(object))";
			}
		}
		buffer += ")";
		return isFirst;
	}
	
	private boolean methodFullPointCutPart(ProFitClass pfClass, boolean isFirst) {
		if(isFirst) {
			buffer += "\t\t   ";
			isFirst = false;
		}else {
			buffer += "\t\t|| ";
		}
		buffer += "( execution(* "
				+ aspectsUtil.splitExtension(pfClass.getName())
				+ ".*(..)) && target(object))";		
		return isFirst;
	}

	private boolean methodAditivePointCutPart(ProFitClass pfClass, boolean isFirst) {
		Iterator<ProFitMethod> iterator = pfClass.getMethods().iterator();
		
		while(iterator.hasNext()) {
			ProFitMethod pfMethod = iterator.next();
			if (pfMethod.isInstrumentationTarget()) {

				if (isFirst) {
					buffer += "\t\t   ";
					isFirst = false;
				} else {
					buffer += "\t\t|| ";
				}	

				buffer += "( execution(* "
						+ aspectsUtil.splitExtension(pfClass.getName())
						+ "."
						+ pfMethod.getName()
						+ "(..)) && target(object))";
			}
		}
		return isFirst;
	}
}
