package edu.uclm.esi.regexpbuilder.aspectGeneration;

import org.json.JSONArray;
import org.json.JSONException;

import edu.uclm.esi.regexpbuilder.analyzer.parser.InstrumentationManager;

public abstract class AbstractAspectBroker {
	
	InstrumentationManager manager;
	JSONArray classes;
	
	public AbstractAspectBroker(JSONArray classes) {
		super();
		manager = InstrumentationManager.getInstance();
		this.classes = classes;
	}

	String aspectFileContents;

	abstract String pointCut() throws JSONException;

	abstract String getAspectType();

	abstract String actionBefore();

	abstract String actionAfter();
	
	public JSONArray getClasses() {
		return classes;
	}

	public void setClasses(JSONArray classes) {
		this.classes = classes;
	}
}


