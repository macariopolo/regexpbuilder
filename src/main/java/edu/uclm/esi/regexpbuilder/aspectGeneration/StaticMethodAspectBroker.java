package edu.uclm.esi.regexpbuilder.aspectGeneration;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;

import edu.uclm.esi.regexpbuilder.analyzer.parser.InstrumentationManager;
import edu.uclm.esi.regexpbuilder.instrumentation.ProFitProject;


public class StaticMethodAspectBroker extends AbstractAspectBroker {
	AspectsUtil aspectsUtil = AspectsUtil.getInstance();
	
	public StaticMethodAspectBroker(JSONArray classes) {
		super(classes);
	}

	String pointCut() {
		return "( execution(public static * *.*(..)) "
				+ "|| execution(private static * *.*(..)) "
				+ "|| execution(protected static * *.*(..)) ) "
				+ "&& !execution(* traceWriter.*.*(..))"
				+ "&& !execution(* traceWriter.*.*.*(..))"
				+ "&& !execution(* traceWriter.*.*.*.*(..))"
				+ ")";
	}

	String getAspectType() {
		return "StaticMethodAspect";
	}

	String actionBefore() {
		return aspectsUtil.outPutOptions("methodCall", "before", "static");
	}

	String actionAfter() {
		return aspectsUtil.outPutOptions("methodCall", "after", "static");
	}
}
