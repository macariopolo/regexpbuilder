package edu.uclm.esi.regexpbuilder.aspectGeneration;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import edu.uclm.esi.regexpbuilder.analyzer.parser.InstrumentationManager;
import edu.uclm.esi.regexpbuilder.instrumentation.ProFitClass;
import edu.uclm.esi.regexpbuilder.instrumentation.ProFitPackage;
import edu.uclm.esi.regexpbuilder.instrumentation.ProFitProject;

public final class AspectsUtil {
	
	private static AspectsUtil singleInstance = new AspectsUtil();
	InstrumentationManager manager;
	 
    private AspectsUtil() {
    		manager = InstrumentationManager.getInstance();
    }
 
    public static AspectsUtil getInstance() {
        return singleInstance;
    }
	
	public String packageStatement() {
		return "package traceWriterSource;"+ System.getProperty("line.separator");
	}
	
	public String importStatements(JSONArray classes) throws JSONException {
		String outPut = "";
		outPut += System.getProperty("line.separator") + System.getProperty("line.separator");

		outPut += "import traceWriterSource.serialization.FieldSetTrace;" + System.getProperty("line.separator")
				+ "import traceWriterSource.serialization.MethodCallTrace;" + System.getProperty("line.separator")
				+ "import traceWriterSource.traceWriter.TraceWriter;" + System.getProperty("line.separator") + System.getProperty("line.separator")
				+ "import org.aspectj.lang.JoinPoint;" + System.getProperty("line.separator")
				+ "import org.aspectj.lang.ProceedingJoinPoint;" + System.getProperty("line.separator")
				+ "import org.aspectj.lang.annotation.After;" + System.getProperty("line.separator")
				+ "import org.aspectj.lang.annotation.Aspect;" + System.getProperty("line.separator")
				+ "import org.aspectj.lang.annotation.Before;" + System.getProperty("line.separator")
				+ "import org.aspectj.lang.annotation.Pointcut;" + System.getProperty("line.separator")
				+ "import org.json.JSONObject;\r\n" + System.getProperty("line.separator");
		
		outPut += importStatementsIterator(classes);	
		return outPut;
	}
		
	private String importStatementsIterator(JSONArray classes) throws JSONException {
		String outPut = "";
		JSONObject jsElement;
		for (int i=0; i<classes.length(); i++) {
			jsElement = classes.getJSONObject(i);
			if ((boolean) jsElement.get("checked")) {
				outPut += "import " + formatPathForImport(splitExtension(jsElement.getString("name"))) + ";"+ System.getProperty("line.separator");
				ProFitClass element = new ProFitClass(jsElement.getJSONArray("methods"), jsElement.getString("name"));
				manager.getInstrumentableClassesList().add(element);
			}
		}
		outPut += System.getProperty("line.separator");
		return outPut;
	}
	
	private String formatPathForImport(String splitExtension) {
		String outPut = "";
		InstrumentationManager manager = InstrumentationManager.getInstance();
		outPut = splitExtension.replace(File.separator, ".");
		String[] outArray = outPut.split("src.");
		return outArray[1];
	}

	public String outPutOptions(String kind, String moment, String context) {
		String outPut = "";
		outPut += serialization(kind, moment, context);
		if(true/*manager.getProject().getConfiguration().getBrokerConfiguration().isMustPrintInJSON()*/) {
			outPut +=  "\t\t"
					+ "TraceWriter.writeJSON(trace);" + System.getProperty("line.separator");
		}
		return outPut;
	}
	
	public String serialization(String kind, String moment, String context) {
		String outPut = "";
		if(kind.equals("methodCall")) {
			outPut += "\r\n\t\t"
					+ "long timeStamp = System.nanoTime();"
					+ "\r\n\r\n\t\t"
					+ "MethodCallTrace trace = new MethodCallTrace(\""+moment+"\", jp.getKind(),"
					+ "\r\n\t\t"
					+ "jp.getSourceLocation().toString(), jp.getSignature().toString(),"
					+ "\r\n\t\t"
					+ "Long.toString(timeStamp), \""+context+"\",jp.getArgs());";
		}else {
			outPut += "\r\n\t\t"
					+ "String value;"
					+ "\r\n\t\t"
					+ "if ( jp.getArgs()[0]!=null ) { value = jp.getArgs()[0].toString(); } else { value = \"\"; }"
					+ "\r\n\t\t"
					+ "long timeStamp = System.nanoTime();"
					+ "\r\n\r\n\t\t\t"
					+ "FieldSetTrace trace = new FieldSetTrace(\""+moment+"\", jp.getKind(),"
					+ "\r\n\t\t\t"
					+ "jp.getSourceLocation().toString(), jp.getSignature().toString(),"
					+ "\r\n\t\t\t"
					+ "Long.toString(timeStamp), value);";
		}
		return outPut;
	}
	
	// Para formatear una ruta para packages e imports, primero se sustituyen los "\"
	// de la ruta por puntos, a continuaci�n se extrae de la cadena la ruta del proyecto,
	// adem�s de 5 caracteres adicionales que se corresponden con ".src."
	public String formatPath(String path) {
		String outPut = path.replaceAll(File.separator, ".");
		outPut = outPut.substring(manager.getProject().getPath().toString().length()+5);
		return outPut;
	}
	
	public String splitExtension(String className) {
		return className.substring(0, className.length()-5);
	}
}

