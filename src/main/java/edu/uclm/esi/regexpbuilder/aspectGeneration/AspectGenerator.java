package edu.uclm.esi.regexpbuilder.aspectGeneration;

import org.json.JSONException;

import edu.uclm.esi.regexpbuilder.analyzer.parser.InstrumentationManager;
import edu.uclm.esi.regexpbuilder.instrumentation.ProFitProject;

public class AspectGenerator {

	ProFitProject pfProject;
	String aspectFileContents;
	InstrumentationManager manager = InstrumentationManager.getInstance();
	String thisPackage = "";
	AspectsUtil aspectsUtil = AspectsUtil.getInstance();
	AbstractAspectBroker aspectBroker;
	
	public AspectGenerator() {
		super();
		this.pfProject = null;
		this.aspectFileContents = "";
	}
	
	public String build() throws JSONException {
		aspectFileContents = "";
		manager.getInstrumentableClassesList().clear();
		aspectFileContents += aspectsUtil.packageStatement();
		aspectFileContents += aspectsUtil.importStatements(aspectBroker.getClasses());
		instrumentationAspect();
		return aspectFileContents;
	}
	
	private void instrumentationAspect() throws JSONException {
		aspectFileContents += "@Aspect" + System.getProperty("line.separator") 
		+ "public class "+ aspectBroker.getAspectType() +" {" + System.getProperty("line.separator");
		Before();
		After();
		aspectFileContents += System.getProperty("line.separator");
		aspectFileContents += "}";
	}

	private void Before() throws JSONException {
		aspectFileContents += "\t@Before(";
		aspectFileContents += "\"" + aspectBroker.pointCut() + "\"";
		aspectFileContents += ")" + System.getProperty("line.separator")
		+ "\tpublic void beforeAction(JoinPoint jp) {" + System.getProperty("line.separator");
		aspectFileContents += aspectBroker.actionBefore();
		aspectFileContents += "}"+ System.getProperty("line.separator");;
	}

	private void After() throws JSONException {
		aspectFileContents += "\t@After(";
		aspectFileContents += "\"" + aspectBroker.pointCut() + "\"";
		aspectFileContents += ")" + System.getProperty("line.separator")
		+ "\tpublic void afterAction(JoinPoint jp) {" + System.getProperty("line.separator");
		aspectFileContents += aspectBroker.actionAfter();
		aspectFileContents += "}"+ System.getProperty("line.separator");; 
		
	}

	public ProFitProject getPfProject() {
		return pfProject;
	}

	public void setPfProject(ProFitProject pfProject) {
		this.pfProject = pfProject;
	}
	
	public String getAspectFileContents() {
		return aspectFileContents;
	}

	public void setAspectFileContents(String aspectFileContents) {
		this.aspectFileContents = aspectFileContents;
	}

	public AbstractAspectBroker getAspectBroker() {
		return aspectBroker;
	}

	public void setAspectBroker(AbstractAspectBroker aspectBroker) {
		this.aspectBroker = aspectBroker;
	}
}
