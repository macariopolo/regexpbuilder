package edu.uclm.esi.regexpbuilder.web.ws;


import java.io.File;

import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import edu.uclm.esi.regexpbuilder.analyzer.parser.InstrumentationManager;
import edu.uclm.esi.regexpbuilder.analyzer.visitor.AnalysisFileVisitor;
import edu.uclm.esi.regexpbuilder.instrumentation.ProFitProject;
import edu.uclm.esi.regexpbuilder.instrumenter.Instrumenter;
import edu.uclm.esi.regexpbuilder.model.Manager;
import edu.uclm.esi.regexpbuilder.processes.ProcessExecutor;

@Component
public class WSServerProjects extends TextWebSocketHandler {
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
	}

	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		JSONObject jso=new JSONObject(message.getPayload());
		if (jso.getString("type").equals("openProject")) {
			String projectName = jso.getString("projectName");
			String projectFolder = Manager.get().getWorkingFolder() + projectName + File.separator;
			AnalysisFileVisitor visitor = new AnalysisFileVisitor(projectFolder);
			ProFitProject project = visitor.walkProject();
			InstrumentationManager.getInstance().setProject(project);
			JSONObject resp = new JSONObject();
			resp.put("type", "project");
			resp.put("project", project.toJSONArray());
			session.sendMessage(new TextMessage(resp.toString()));
		} else if (jso.getString("type").equals("instrument")) {
			Instrumenter instrumenter = new Instrumenter();
			instrumenter.instrument(jso);
		} else if (jso.getString("type").startsWith("execute")) {
			String projectName = jso.getString("projectName");
			ProcessExecutor executor = new ProcessExecutor(projectName, jso.getString("type").equals("executeOriginal"));
			String command = jso.getString("command");
			executor.execute(session, command);
		}
	}

	



	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		Manager.get().remove(session);
	}
	
	
}
