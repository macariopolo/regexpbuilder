package edu.uclm.esi.regexpbuilder.web;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mkyong.zip.UnzipUtility;

import edu.uclm.esi.regexpbuilder.model.Manager;

@RestController
public class Controller {
	
	
	@PostMapping("/uploadFile")
    public void uploadFile(@RequestParam String projectName, @RequestParam("file") MultipartFile file) throws IOException {
		String projectFolder = Manager.get().getWorkingFolder() + projectName + File.separator;
		Manager.get().setProjectFolder(projectFolder);
		new File(projectFolder).mkdirs();
		String suaFolder = projectFolder + "SUA" + File.separator;
		new File(suaFolder).mkdir();
		String instrumentedFolder = projectFolder + "instrumented" + File.separator;
		new File(instrumentedFolder).mkdir();
		Manager.get().setInstrumentedFolder(instrumentedFolder);
		new File(projectFolder + "zip" + File.separator).mkdir();
		byte[] bytes = file.getBytes();
		try(FileOutputStream fos = new FileOutputStream(projectFolder + "zip" + File.separator + file.getOriginalFilename())) {
			fos.write(bytes);
		}
		
		Manager.get().setSuaFolder(suaFolder);
		suaFolder = suaFolder.replace('\\', File.separatorChar);
		if (!suaFolder.endsWith(File.separator))
			suaFolder+=File.separator;
		//outputFolder = outputFolder + projectName + File.separator;
		UnzipUtility unzip = new UnzipUtility();
		unzip.unzip(projectFolder + "zip" + File.separator + file.getOriginalFilename(), suaFolder);
		new File(projectFolder + "zip" + File.separator + file.getOriginalFilename()).delete();
		new File(projectFolder + "zip").delete();
	}
}
