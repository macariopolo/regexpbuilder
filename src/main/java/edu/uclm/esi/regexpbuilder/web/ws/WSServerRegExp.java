package edu.uclm.esi.regexpbuilder.web.ws;

import java.security.SecureRandom;

import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import edu.uclm.esi.regexpbuilder.model.Manager;
import edu.uclm.esi.regexpbuilder.model.SessionManager;

@Component
public class WSServerRegExp extends TextWebSocketHandler {
	private static SecureRandom dado = new SecureRandom();
	
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		SessionManager manager = new SessionManager(session);
		Manager.get().add(manager);
	}
	
	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		SessionManager manager = Manager.get().getSessionManager(session.getId());
		
		JSONObject jso=new JSONObject(message.getPayload());
		if (jso.getString("type").equals("setNumberOfThreads")) {
			manager.setNumberOfThreads(jso.getInt("numberOfThreads"));
		} else if (jso.getString("type").equals("getThreads")) {
			int threads = manager.getNumberOfProcessors();
			JSONObject resp = new JSONObject();
			resp.put("type", "threads");
			resp.put("threads", threads);
			session.sendMessage(new TextMessage(resp.toString()));
		} else { 
			boolean separated = jso.optBoolean("separated");
			String texto = jso.optString("sequences");
			String[] cadenas = texto.split("\n");
			manager.setNumberOfThreads(jso.getInt("numberOfThreads"));
			manager.loadSequenceCollection(cadenas, separated);
	
			if (jso.getString("type").equals("getRegularExpression")) {
				String numberOfFinalStates = jso.getString("numberOfFinalStates");
				String order = jso.getString("order");
				boolean minimize = jso.getBoolean("minimize");
				String algorithm = jso.getString("algorithm");
				
				JSONObject resp = manager.getRegularExpression(numberOfFinalStates, order, minimize, algorithm);
				session.sendMessage(new TextMessage(resp.toString()));
			} 
		}
	}
	
/*	private JSONObject getRegularExpression(SessionManager manager, JSONObject jso, SequenceCollection sequences, WebSocketSession session) throws JSONException, IOException {
		String numberOfFinalStates = jso.getString("numberOfFinalStates");
		String order = jso.getString("order");
		sendProgress(session, "Sorting sequences");
		Reloj reloj = new Reloj(session);
		reloj.start(Reloj.SORTING);
		if (order.equals("shortest"))
			sequences.sortByLength(true);
		else if (order.equals("longest"))
			sequences.sortByLength(false);
		else if (order.equals("byStart"))
			sequences.sortByStart();
		else 
			sequences.sortByEnd();
		reloj.stop(Reloj.SORTING);
		sendProgress(session, "Building original automata");
		sequences.buildAutomata(numberOfFinalStates, session, reloj);
		Automata originalAutomata = sequences.getAutomata();
		sendProgress(session, "Calculating minimized automata");
		Automata minimizedAutomata = originalAutomata.minimize();
		sendAutomata(session, originalAutomata, minimizedAutomata);
		Automata automata = jso.getBoolean("minimize") ? minimizedAutomata : originalAutomata;
		Solver solver;
		String algorithm = jso.getString("algorithm");
		reloj.start(Reloj.BUILD_EQUATIONS);
		if (algorithm.equals("equations"))
			solver = new EquationsSolver(automata, session, reloj);
		else if (algorithm.equals("transitionPairs"))
			solver = new TransitionsPairsSolver(automata, session, reloj);
		else if (algorithm.equals("matrices")) 
			solver = new TransitiveClosureMatrixSolver(automata, session, reloj);
		else 
			solver = new HeuristicMatrixSolver(automata, session, reloj);
		reloj.stop(Reloj.BUILD_EQUATIONS);
		reloj.start(Reloj.SOLVING);
		solver.solve();
		reloj.stop(Reloj.SOLVING);
		JSONObject resp = new JSONObject();
		resp.put("type", "regularExpression");
		resp.put("regularExpression", solver.getRegularExpression());
		session.sendMessage(new TextMessage(resp.toString()));
		return resp;
	}*/
	
	public static synchronized void sendProgress(WebSocketSession session, String text) {
		if (session!=null) {
			try {
				JSONObject jso = new JSONObject();
				jso.put("type", "progress");
				jso.put("message", text);
				WebSocketMessage<?> wsMessage=new TextMessage(jso.toString());
				session.sendMessage(wsMessage);
			} catch (Exception e) {
				System.err.println(e.toString());
			}
		}
	}
	
	public static synchronized void sendSubprogress(WebSocketSession session, int threadId, String text, double chance) {
		if (session==null)
			return;
		boolean send = chance==1;
		if (!send)
			send = dado.nextDouble()<0.05;
		if (send && session!=null) {
			try {
				JSONObject jso = new JSONObject();
				jso.put("type", "subprogress");
				jso.put("threadId", threadId);
				jso.put("message", text);
				WebSocketMessage<?> wsMessage=new TextMessage(jso.toString());
				session.sendMessage(wsMessage);
			} catch (Exception e) {
				System.err.println(e.toString());
			}
		}
	}

	@SuppressWarnings("unused")
	private void sendError(WebSocketSession session, String message) throws Exception {
		if (session==null)
			return;
		JSONObject jso = new JSONObject();
		jso.put("type", "ERROR");
		jso.put("message", message);
		WebSocketMessage<?> wsMessage=new TextMessage(jso.toString());
		session.sendMessage(wsMessage);
	}

	public static void sendTime(WebSocketSession session, String taskName, Long taskTime) {
		if (session==null)
			return;
		try {
			JSONObject jso = new JSONObject();
			jso.put("type", "time");
			jso.put("task", taskName);
			jso.put("time", taskTime);
			WebSocketMessage<?> wsMessage=new TextMessage(jso.toString());
			session.sendMessage(wsMessage);
		} catch (Exception e) {
			System.err.println(e.toString());
		}
	}
	
	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		Manager.get().remove(session);
	}
}
