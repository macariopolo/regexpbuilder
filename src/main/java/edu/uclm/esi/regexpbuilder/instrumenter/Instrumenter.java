package edu.uclm.esi.regexpbuilder.instrumenter;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.file.Paths;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import edu.uclm.esi.regexpbuilder.analyzer.parser.InstrumentationManager;
import edu.uclm.esi.regexpbuilder.aspectGeneration.AspectGenerator;
import edu.uclm.esi.regexpbuilder.aspectGeneration.DynamicMethodAspectBroker;
import edu.uclm.esi.regexpbuilder.aspectGeneration.StaticMethodAspectBroker;
import edu.uclm.esi.regexpbuilder.instrumentation.ProFitClass;
import edu.uclm.esi.regexpbuilder.instrumentation.ProFitProject;
import edu.uclm.esi.regexpbuilder.instrumenter.parser.ProFitInstrumenter;
import edu.uclm.esi.regexpbuilder.model.Manager;
import edu.uclm.esi.regexpbuilder.persistence.JavaFileBroker;
import edu.uclm.esi.regexpbuilder.persistence.PackageBroker;

public class Instrumenter {

	public Instrumenter() {
		super();
	}

	public void instrument(JSONObject jso) throws JSONException {
		Manager.get().setInstrumentationKind(jso.getString("instrumentationKind"));
		boolean instrumentAttributes = jso.getBoolean("instrumentAttributes");
		boolean instrumentMethods = jso.getBoolean("instrumentMethods");
		String inputFolder =  Manager.get().getSuaFolder();
		String outputFolder = Manager.get().getInstrumentedFolder();
		String outPutProjectFolder = outputFolder +InstrumentationManager.getInstance().getProject().getName();
		JSONArray classes = jso.getJSONArray("classes");
		JSONObject clazz;
		try {
			PackageBroker.copyFolder(Paths.get(inputFolder), Paths.get(outputFolder));
		}catch(Exception e){
			e.printStackTrace();
		}
		PackageBroker.createTraceWriterPackage(outPutProjectFolder);
		JavaFileBroker.copyTraceWriterFile(outPutProjectFolder);
		copySerializationDataStructure(outPutProjectFolder);
		InstrumentationManager manager = InstrumentationManager.getInstance();

		if(!jso.getString("instrumentationKind").contentEquals("aspectiza")) {
			for (int i=0; i<classes.length(); i++) {
				clazz = classes.getJSONObject(i);
				if (clazz.getBoolean("checked")) {
					String outPutPath = calculateOutPutPath(clazz.getString("name"), inputFolder, outputFolder);
					String[] args = new String[4];
					args[0] = clazz.getString("name");
					args[1] = outPutPath;
					args[2] = "" + instrumentAttributes;
					args[3] = "" + instrumentMethods;
					manager.setClassUnderAnalysis(new ProFitClass(clazz.getJSONArray("methods"), clazz.getString("name")));
					new File(outPutPath).delete();
					ProFitInstrumenter.main(args);
				}
			}
			
		}
		if(!jso.getString("instrumentationKind").contentEquals("inserta")) {
			createAspectFiles(outPutProjectFolder, classes);
		}
	}
		
	private String calculateOutPutPath(String clazz, String inputPathRoot, String outPutPathRoot) {
		String fixedInPut = inputPathRoot.replace("/", File.separator).replace("\\", File.separator);
		String fixedOutPut = outPutPathRoot.replace("/", File.separator).replace("\\", File.separator);
		String tail = clazz.replace(fixedInPut, fixedOutPut);
		return tail;
	}
	
	private static void copySerializationDataStructure(String target) {
		copyTrace(target);
		copyFieldSetTraceFile(target);
		copyMethodCallTraceFile(target);
		copyLoopTraceFile(target);
	}

	private static void copyTrace(String target) {
		DataInputStream dis = null;
		DataOutputStream dos = null;
		try{
			String outPutFile =  target + File.separator + "src" + File.separator + "traceWriterSource"
					+ File.separator + "serialization" + File.separator + "Trace.java";
			new File (outPutFile).createNewFile();
			dis = new DataInputStream(new FileInputStream(new File("resources" + File.separator + "templates" + File.separator 
					+ "serialization" + File.separator + "Trace.java.txt").getAbsolutePath()));
			dos = new DataOutputStream(new FileOutputStream(outPutFile));
			while(true){
				dos.writeByte(dis.readByte());
			}
		}catch(EOFException e){
			System.out.println("\n\n**********************Trace file Succesfuly created**********************");
			try {
				dis.close();
				dos.close();
			}catch(Exception exception){
				System.out.println("Error in copyTrace ==> "+e.toString());
			}
		}catch(Exception e){
			System.out.println("Error in copyTrace ==> "+e.toString());
		}
	}

	private static void copyFieldSetTraceFile(String target) {
		DataInputStream dis = null;
		DataOutputStream dos = null;
		try{
			String outPutFile =  target + "" + File.separator + "src" + File.separator 
					+ "traceWriterSource" + File.separator + "serialization" + File.separator + "FieldSetTrace.java";
			new File (outPutFile).createNewFile();
			dis = new DataInputStream(new FileInputStream(new File("resources" + File.separator + "templates" 
					+ File.separator + "serialization" + File.separator + "FieldSetTrace.java.txt").getAbsolutePath()));
			dos = new DataOutputStream(new FileOutputStream(outPutFile));

			while(true){
				dos.writeByte(dis.readByte());
			}
		}catch(EOFException e){
			System.out.println("\n\n**********************FieldSetTrace file Succesfuly created**********************");
			try {
				dis.close();
				dos.close();
			}catch(Exception exception){
				e.printStackTrace();
				System.out.println("Error in copyFieldSet ==> "+e.toString());
			}
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Error in copyFieldSet ==> "+e.toString());
		}
	}
	
	private static void copyMethodCallTraceFile(String target) {
		DataInputStream dis = null;
		DataOutputStream dos = null;
		try{
			String outPutFile =  target+ "" + File.separator + "src" + File.separator
					+ "traceWriterSource" + File.separator + "serialization" + File.separator + "MethodCallTrace.java";
			new File (outPutFile).createNewFile();
			dis = new DataInputStream(new FileInputStream(new File("resources" + File.separator + "templates"
					+ File.separator + "serialization" + File.separator 
					+ "MethodCallTrace.java.txt").getAbsolutePath()));
			dos = new DataOutputStream(new FileOutputStream(outPutFile));

			while(true){
				dos.writeByte(dis.readByte());
			}
		}catch(EOFException e){
			System.out.println("\n\n**********************MethodCallTrace file Succesfuly created**********************");
			try {
				dis.close();
				dos.close();
			}catch(Exception exception){
				System.out.println("Error in copyMethodCall ==> "+e.toString());
			}
		}catch(Exception e){
			System.out.println("Error in copyMethodCall ==> "+e.toString());
		}
	}

	private static void copyLoopTraceFile(String target) {
		DataInputStream dis = null;
		DataOutputStream dos = null;
		try{
			String outPutFile =  target+ "" + File.separator + "src" + File.separator
					+ "traceWriterSource" + File.separator + "serialization" + File.separator + "LoopTrace.java";
			new File (outPutFile).createNewFile();
			dis = new DataInputStream(new FileInputStream(new File("resources" + File.separator + "templates"
					+ File.separator + "serialization" + File.separator 
					+ "LoopTrace.java.txt").getAbsolutePath()));
			dos = new DataOutputStream(new FileOutputStream(outPutFile));

			while(true){
				dos.writeByte(dis.readByte());
			}
		}catch(EOFException e){
			System.out.println("\n\n**********************LoopTrace file Succesfuly created**********************");
			try {
				dis.close();
				dos.close();
			}catch(Exception exception){
				System.out.println("Error in copyLoopTrace ==> "+e.toString());
			}
		}catch(Exception e){
			System.out.println("Error in copyLoopTrace ==> "+e.toString());
		}
	}

	private static void createAspectFiles(String target, JSONArray classes) throws JSONException {

		InstrumentationManager manager = InstrumentationManager.getInstance();
		ProFitProject pfProject = manager.getProject();
		AspectGenerator aspectGenerator = new AspectGenerator();
		
		aspectGenerator.setPfProject(pfProject);
		
		aspectGenerator.setAspectBroker(new StaticMethodAspectBroker(classes));
		String content = aspectGenerator.build();
		JavaFileBroker.createAspectFile(target, "StaticMethodAspect.java", content);

		aspectGenerator.setAspectBroker(new DynamicMethodAspectBroker(classes));
		content = aspectGenerator.build();
		JavaFileBroker.createAspectFile(target, "DynamicMethodAspect.java", content);

		//////////////////////////////////////////////////////////
		//    Para cuando se metan los atributos
		//////////////////////////////////////////////////////////
		
		/*aspectGenerator.setAspectBroker(new FieldSetApectGenerator(classes));
		content = aspectGenerator.build();
		JavaFileBroker.createAspectFile(target, "FieldSetAspect.java", content);*/

	}
}
