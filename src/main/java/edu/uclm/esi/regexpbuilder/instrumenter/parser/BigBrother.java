package edu.uclm.esi.regexpbuilder.instrumenter.parser;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import edu.uclm.esi.regexpbuilder.analyzer.parser.InstrumentationManager;
import edu.uclm.esi.regexpbuilder.instrumentation.ProFitAttribute;
import edu.uclm.esi.regexpbuilder.instrumentation.ProFitClass;
import edu.uclm.esi.regexpbuilder.instrumentation.ProFitMethod;
import edu.uclm.esi.regexpbuilder.model.Manager;

public class BigBrother {
	ProFitClass proFitClass;
	int state = 0;
	String line;
	InstrumentationManager manager = InstrumentationManager.getInstance();
	List<String> variableBuffer;
	long startTime = 0;
	long endTime = 0;
	static int loopTraceNumber = 0;

	ArrayList<String> methodNames = new ArrayList<String>();
	ArrayList<Boolean> hasMethodReturn = new ArrayList<Boolean>();
	
	List<Pair<Integer, String>> loopBuffer = new ArrayList<Pair<Integer, String>>(); // Contiene las trazas de bucle que han sido abiertas pero no cerradas.
	Boolean loopEarlyExitControl = false;  // Para las anteriores, true si ha cerrado antes en un return/throw y false de lo contrario.
	private boolean instrumentAttributes;
	private boolean instrumentMethods;
	private boolean instrumentLoops = true;
	
	public BigBrother() {
		super();
		variableBuffer = new ArrayList<String>();
		proFitClass = manager.getClassUnderAnalysis();
		proFitClass.setInstrumentationTarget(true);
	}
	
	public String whatToInsertBeforeLine(List<String> line, boolean generateNewTraces) {
		String preLine = "";
		String postLine = "";
		
		// La existencia de líneas posteriores al atributo depende de si se han escrito líneas anteriores.
		Iterator iterator;
		if (!variableBuffer.isEmpty()) {
			iterator = variableBuffer.iterator();
			while (iterator.hasNext()) {
				String varToPostInstrument = (String) iterator.next();
				if (this.instrumentAttributes) {
					postLine += "\r\n " + wrapLine("( new FieldSetTrace(\"after\", \"field-set\","
							+ "\"" + proFitClass.getName() + "." +  varToPostInstrument+":\"+Thread.currentThread().getStackTrace()[0].getLineNumber(), " 
							+ "\"" + varToPostInstrument + "\"" + ", "
							+ " Long.toString(System.nanoTime())" + ", "
							+ "new String() + " + varToPostInstrument
							+ "));");
				}
			}
			variableBuffer = new ArrayList<String>();
		}
	
		//La inserción de líneas de entrada depende de las opciones que haya seleccionado el usuario.
		if (proFitClass.isInstrumentationTarget() && generateNewTraces) {
			if (manager.isAreWeInsideAMethod()) {
				iterator = proFitClass.getAttributes().iterator();
				while (iterator.hasNext()) {
					ProFitAttribute pfAttribute = (ProFitAttribute) iterator.next();
					if (pfAttribute.isInstrumentationTarget()) {
						String elemento = pfAttribute.getName();										
						if (line.contains(elemento) && this.instrumentAttributes) {
								preLine += "\r\n " + wrapLine("( new FieldSetTrace(\"before\", \"field-set\","
									+ "\"" + proFitClass.getName() + "." +  pfAttribute.getName()+":\"+Thread.currentThread().getStackTrace()[0].getLineNumber(), " 
									+ "\"" + pfAttribute.getName() + "\"" + ", "
									+ " Long.toString(System.nanoTime()), "
									+ "\"\""
									+ "));");									
								if (!line.contains("return") && !line.contains("throw"))
									variableBuffer.add(elemento);
						 }	
					}	
				}
			}	
		}
	
		String outPut = "";
		outPut = postLine + preLine;

		if (line.contains("return")) {  
			outPut += getMethodOutLine(true, false);
		} else if (line.contains("throw")) {
			outPut += getMethodOutLine(false, false);
		}
		return outPut;
	}
	
	public void checkMethod(String methodName) {

		boolean methodInList = false;

		////////////////////////////////////////////////////////////
		/////////   Aquí la comprobación individual del método   ///
		////////////////////////////////////////////////////////////
		if(proFitClass.isInstrumentationTarget()) {
			Iterator iterator = proFitClass.getMethods().iterator();
			while (iterator.hasNext()) {
				ProFitMethod pfMethod = (ProFitMethod) iterator.next();
				//if(pfMethod.isInstrumentationTarget()) {
					if(pfMethod.getName()/*.substring(0, pfMethod.getName().length()-5)*/.equals(methodName)) {
						methodInList = true;	
						System.out.println("Estamos en BigBrother: ---------------------------------------> Bookmark!");
					}
				//}
			}
		}
        ////////////////////////////////////////////////////////////
		
		if(methodInList) {
			state = 1;
			this.methodNames.add(methodName);
			this.hasMethodReturn.add((Boolean)false);
		}
	}
	
	public String getMethodInLine() {
		String line = "";
		if (state == 1) {
			if (this.instrumentMethods) {
				startTime = System.nanoTime();
				line += "\r\n " + wrapLine("( new MethodCallTrace(\"before\", \"method-call\","
						+ "\"" + proFitClass.getName() + "." +  methodNames.get(methodNames.size()-1)+":\"+Thread.currentThread().getStackTrace()[0].getLineNumber(), "
						+ "\"" + methodNames.get(methodNames.size()-1) + "\"" + ", "
						+ " Long.toString(System.nanoTime()), "
						+ "\"\"," // falta el contexto 
						+ " new String[0]" // faltan los parámetros
						+ "));");					
				}
			state = 2;
		}
		return line;
	}
	
	public String getMethodOutLine(boolean isReturn, boolean isEndOfBlock) {
		String line = "";

		// isReturn contiene si gramaticalmente estamos analizando un punto previo
		// a una sentencia return [X]; , hasMethodReturn es true si el método analizado
		// contiene alguna sentencia de ese tipo, para no instrumentar al final del 
		// bloque (código muerto).
		if (isReturn && !hasMethodReturn.isEmpty())
			hasMethodReturn.set(hasMethodReturn.size()-1, true);
		if( !(hasMethodReturn.size() == 0) ) {
			if((hasMethodReturn.get(hasMethodReturn.size()-1) && isReturn) || (!hasMethodReturn.get(hasMethodReturn.size()-1) && !isReturn)) {
				if(this.instrumentMethods){
						endTime = System.nanoTime();
												
						line += "\r\n " + wrapLine("( new MethodCallTrace(\"after\", \"method-call\","
								+ "\"" + proFitClass.getName() + "." +  methodNames.get(methodNames.size()-1)+":\"+Thread.currentThread().getStackTrace()[0].getLineNumber(), " 
								+ "\"" + methodNames.get(methodNames.size()-1) + "\"" + ", "
								+ " Long.toString(System.nanoTime()) " + ", "
								+ "\"\"," // falta el contexto 
								+ " new String[0]" // faltan los parámetros
								+ "));");
					
				}
				state = 0;
				if(isEndOfBlock) {
					methodNames.remove(methodNames.size()-1);
					hasMethodReturn.remove(hasMethodReturn.size()-1);
				}
		    }
		}
		String value = "";
		if(line.equals("")) {
			value = line;
		}else {
			value =  line;
		}
		return value;
	}
	
	public void popMethodFromStacks(String methodName) {
		if(methodNames.size() != 0) {
			if(methodNames.get(methodNames.size()-1).equals(methodName)) {
				methodNames.remove(methodNames.size()-1);
				hasMethodReturn.remove(hasMethodReturn.size()-1);
			}	
		}
	}

	public String wrapLine(String line) {
		String wrappedLine = "";
		String lineSeparators = "";
		lineSeparators = lineSeparations(wrappedLine);
		wrappedLine = wrappedLine + lineSeparators + "TraceWriter.writeJSON" + line;
		if(!wrappedLine.equals("")) {
			wrappedLine = System.getProperty("line.separator") + wrappedLine + System.getProperty("line.separator");
		}
		return wrappedLine;
	}
	
	private String lineSeparations(String wrappedLine) {
		String separator = "";
		if(!wrappedLine.equals("")) {
			separator = "\r\n";
		}
		return separator;
	}
	
	public String getImportSentence(){
		return "\r\nimport traceWriterSource.traceWriter.TraceWriter;"
				+ "\r\nimport traceWriterSource.serialization.MethodCallTrace;"
				+ "\r\nimport traceWriterSource.serialization.FieldSetTrace;"
				+ "\r\nimport traceWriterSource.serialization.LoopTrace;";
	}
	
	public String getStaticInstanceSentence() {
		//return "\r\nstatic TraceWriter traceWriter = TraceWriter.getInstance();";
		return "";
	}
	
	public String getNonStaticInstanceSentence() {
		//return "\r\nTraceWriter traceWriter = TraceWriter.getInstance();";
		return "";
	}
	
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public String getLine() {
		return line;
	}
	public void setLine(String line) {
		this.line = line;
	}
	
	public List<Pair<Integer, String>> getLoopBuffer() {
		return loopBuffer;
	}

	public void setLoopBuffer(List<Pair<Integer, String>> loopBuffer) {
		this.loopBuffer = loopBuffer;
	}

	public Boolean isLoopEarlyExitControl() {
		return loopEarlyExitControl;
	}

	public void setLoopEarlyExitControl(Boolean loopEarlyExitControl) {
		this.loopEarlyExitControl = loopEarlyExitControl;
	}

	public static int getLoopTraceNumber() {
		return loopTraceNumber;
	}

	public static void setLoopTraceNumber(int loopTraceNumber) {
		BigBrother.loopTraceNumber = loopTraceNumber;
	}

	public String loopTrace(String moment, String loopKind, int id) {
		String line = "";
		
		if(manager.getProject().getConfiguration().getLoopConfiguration().isInstrumentLoops()) {
			if( (manager.getProject().getConfiguration().getLoopConfiguration().isInstrumentIf() && loopKind.equals("if"))
					|| (manager.getProject().getConfiguration().getLoopConfiguration().isInstrumentDo() && loopKind.equals("do"))
					|| (manager.getProject().getConfiguration().getLoopConfiguration().isInstrumentWhile() && loopKind.equals("while"))
					|| (manager.getProject().getConfiguration().getLoopConfiguration().isInstrumentFor() && loopKind.equals("for"))
					|| (manager.getProject().getConfiguration().getLoopConfiguration().isInstrumentSwitch() && loopKind.equals("switch"))
					) {		
				if(moment.equals("before")) {
					loopTraceNumber++;
					line += "\r\n " + wrapLine("( new LoopTrace( \"" + moment + "\", \"loop\","
							+ "\"" + proFitClass.getName() + ":\"+Thread.currentThread().getStackTrace()[0].getLineNumber(), "
							+ "\"noName\"" + ", "
							+ " Long.toString(System.nanoTime()) " + ", "
							+ "\"" + loopKind + "\", "
							+ loopTraceNumber 
							+ "));");
					loopBuffer.add(new Pair<Integer, String>(loopTraceNumber, loopKind));
					loopEarlyExitControl = false;
				}else {
					line += "\r\n " + wrapLine("( new LoopTrace( \"" + moment + "\", \"loop\","
							+ "\"" + proFitClass.getName() + ":\"+Thread.currentThread().getStackTrace()[0].getLineNumber(), "
							+ "\"noName\"" + ", "
							+ " Long.toString(System.nanoTime()) " + ", "
							+ "\"" + loopKind + "\", "
							+ id 
							+ "));");
				}
			}	
		}
		return line;
	}

	public void removeFromList(int id) {
		Pair selectedPair = null;
		
		Iterator iterator = loopBuffer.iterator();
		while(iterator.hasNext()) {
			Pair pair = (Pair) iterator.next();
			if ((Integer) pair.getLeft() == id) {
				selectedPair = pair;
			}
		}
		if(!(selectedPair == null) ) {
			loopBuffer.remove(selectedPair);
		}
	}

	public void instrumentAttributes(boolean b) {
		this.instrumentAttributes = b;
	}
	
	public void instrumentMethods(boolean b) {
		this.instrumentMethods = b;
	}
}