package edu.uclm.esi.regexpbuilder.instrumentation;

import java.io.Serializable;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

public class ProFitAttribute  implements Serializable{
	
	private static final long serialVersionUID = -5932487455825640639L;
	String name;
	boolean inspect, checkValues, isInstrumentationTarget;
	ArrayList<Integer> lines;
	
	public ProFitAttribute(String name, boolean inspect, boolean checkValues, ArrayList<Integer> lines) {
		super();
		this.name = name;
		this.inspect = inspect;
		this.checkValues = checkValues;
		this.lines = lines;
		isInstrumentationTarget = false;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isInspect() {
		return inspect;
	}

	public void setInspect(boolean inspect) {
		this.inspect = inspect;
	}

	public boolean isCheckValues() {
		return checkValues;
	}

	public void setCheckValues(boolean checkValues) {
		this.checkValues = checkValues;
	}

	public ArrayList<Integer> getLines() {
		return lines;
	}

	public void setLines(ArrayList<Integer> lines) {
		this.lines = lines;
	}

	public void addLine(int line) {
		this.lines.add(line);
	}

	public boolean isInstrumentationTarget() {
		return isInstrumentationTarget;
	}

	public void setInstrumentationTarget(boolean isInstrumentationTarget) {
		this.isInstrumentationTarget = isInstrumentationTarget;
	}
	
	public String toString() {
		return name;
	}

	public JSONObject toJSON() throws JSONException {
		return new JSONObject().put("name", this.name);
	}
}
