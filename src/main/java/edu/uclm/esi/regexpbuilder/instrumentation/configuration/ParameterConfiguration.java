package edu.uclm.esi.regexpbuilder.instrumentation.configuration;

import java.io.Serializable;

public class ParameterConfiguration implements Serializable{
	boolean instrumentParameters = false;
	boolean addParameterValues = false;
	boolean insertPersonalCode = false;
	String personalCodeBefore = "";
	String personalCodeAfter = "";
	public ParameterConfiguration() {
		super();
	}
	
	public boolean isInsertPersonalCode() {
		return insertPersonalCode;
	}

	public void setInsertPersonalCode(boolean insertPersonalCode) {
		this.insertPersonalCode = insertPersonalCode;
	}

	public boolean isInstrumentParameters() {
		return instrumentParameters;
	}
	public void setInstrumentParameters(boolean instrumentParameters) {
		this.instrumentParameters = instrumentParameters;
	}
	public boolean isAddParameterValues() {
		return addParameterValues;
	}
	public void setAddParameterValues(boolean addParameterValues) {
		this.addParameterValues = addParameterValues;
	}
	public String getPersonalCodeBefore() {
		return personalCodeBefore;
	}
	public void setPersonalCodeBefore(String personalCodeBefore) {
		this.personalCodeBefore = personalCodeBefore;
	}
	public String getPersonalCodeAfter() {
		return personalCodeAfter;
	}
	public void setPersonalCodeAfter(String personalCodeAfter) {
		this.personalCodeAfter = personalCodeAfter;
	}	
}
