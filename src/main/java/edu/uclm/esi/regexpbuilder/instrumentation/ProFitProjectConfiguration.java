package edu.uclm.esi.regexpbuilder.instrumentation;

import java.io.Serializable;

import edu.uclm.esi.regexpbuilder.instrumentation.configuration.*;

public class ProFitProjectConfiguration implements Serializable{
	
	MethodConfiguration methodConfiguration = new MethodConfiguration();
	ParameterConfiguration parameterConfiguration = new ParameterConfiguration();
	BrokerConfiguration brokerConfiguration = new BrokerConfiguration();
	LoopConfiguration loopConfiguration = new LoopConfiguration();
	
	public ProFitProjectConfiguration() {
		super();
	}
		
	public BrokerConfiguration getBrokerConfiguration() {
		return brokerConfiguration;
	}
	public void setBrokerConfiguration(BrokerConfiguration brokerConfiguration) {
		this.brokerConfiguration = brokerConfiguration;
	}
	public MethodConfiguration getMethodConfiguration() {
		return methodConfiguration;
	}
	public void setMethodConfiguration(MethodConfiguration methodConfiguration) {
		this.methodConfiguration = methodConfiguration;
	}
	public ParameterConfiguration getParameterConfiguration() {
		return parameterConfiguration;
	}
	public void setParameterConfiguration(ParameterConfiguration parameterConfiguration) {
		this.parameterConfiguration = parameterConfiguration;
	}

	public LoopConfiguration getLoopConfiguration() {
		return loopConfiguration;
	}

	public void setLoopConfiguration(LoopConfiguration loopConfiguration) {
		this.loopConfiguration = loopConfiguration;
	}

}
