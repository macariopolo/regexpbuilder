package edu.uclm.esi.regexpbuilder.instrumentation;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;

public class Clazz {
	private String name;
	private List<CConstructor> constructors;
	private List<CMethod> methods;

	public Clazz(ClassNode classNode) {
		this.name = classNode.name;
		this.constructors = new ArrayList<>();
		this.methods = new ArrayList<>();
		System.out.println(classNode.name);
		for (int i=0; i<classNode.methods.size(); i++) {
			MethodNode mn = (MethodNode) classNode.methods.get(i);
			if (mn.name.equals("<clinit>") || mn.name.equals("<cinit>"))
				continue;
			if (mn.name.equals("<init>")) {
				CConstructor cons = new CConstructor(this.name, mn);
				this.constructors.add(cons);
			} else {
				CMethod method = new CMethod(mn);
				this.methods.add(method);
			}
		}
	}

	public JSONObject toJSON() throws JSONException {
		JSONObject jso = new JSONObject();
		jso.put("name", name);
		JSONArray jsaConstructors = new JSONArray();
		for (CConstructor constructor : this.constructors)
			jsaConstructors.put(constructor.toJSON());
		jso.put("constructors", jsaConstructors);
		JSONArray jsaMethods = new JSONArray();
		for (CMethod method : this.methods)
			jsaMethods.put(method.toJSON());
		jso.put("methods", jsaMethods);
		return jso;
	}

	public boolean hasOperations() {
		return !this.constructors.isEmpty() || !this.methods.isEmpty();
	}
}
