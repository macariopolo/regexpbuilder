package edu.uclm.esi.regexpbuilder.instrumentation;

import org.json.JSONException;
import org.json.JSONObject;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.MethodNode;

public class CConstructor {
	private String name;
	private String visibility;

	public CConstructor(String name, MethodNode mn) {
		this.name = name;
		if ((mn.access & Opcodes.ACC_PUBLIC) == 1)
			this.visibility = "+";
		else if ((mn.access & Opcodes.ACC_PROTECTED) == 1)
			this.visibility = "#";
		else if ((mn.access & Opcodes.ACC_PRIVATE) == 1)
			this.visibility = "-";
	}

	public String getName() {
		return name;
	}

	public JSONObject toJSON() throws JSONException {
		return new JSONObject().put("name", name).put("visibility", this.visibility);
	}
}
