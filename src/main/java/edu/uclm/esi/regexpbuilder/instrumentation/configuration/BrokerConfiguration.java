package edu.uclm.esi.regexpbuilder.instrumentation.configuration;

import java.io.Serializable;

public class BrokerConfiguration implements Serializable{
	
	private boolean mustPrintInConsole, mustPrintInTxt, mustPrintInCsv, mustSerialize, mustPrintInJSON;
	
	public BrokerConfiguration() {
		super();
	}

	public boolean isMustPrintInConsole() {
		return mustPrintInConsole;
	}

	public void setMustPrintInConsole(boolean mustPrintInConsole) {
		this.mustPrintInConsole = mustPrintInConsole;
	}

	public boolean isMustPrintInTxt() {
		return mustPrintInTxt;
	}

	public void setMustPrintInTxt(boolean mustPrintInTxt) {
		this.mustPrintInTxt = mustPrintInTxt;
	}

	public boolean isMustPrintInCsv() {
		return mustPrintInCsv;
	}

	public void setMustPrintInCsv(boolean mustPrintInCsv) {
		this.mustPrintInCsv = mustPrintInCsv;
	}

	public boolean isMustSerialize() {
		return mustSerialize;
	}

	public void setMustSerialize(boolean mustSerialize) {
		this.mustSerialize = mustSerialize;
	}

	public void setMustPrintJSONTree(boolean mustPrintInJSON) {
		this.mustPrintInJSON = mustPrintInJSON;
		
	}
	
	public boolean isMustPrintInJSON() {
		return mustPrintInJSON;
	}
}
