package edu.uclm.esi.regexpbuilder.instrumentation;

import java.io.Serializable;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

public class ProFitPackage  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3673103625629504484L;
	String name;
	List<ProFitPackage> packages;
	List<ProFitClass> classes;
	String path; // path se almacena como String en la estructura de datos porque sun.nio.fs.WindosPath no es serializable
	float instrumentableChildrenClassPercentage = 0;
	float instrumentableChildrenPackagePercentage = 0;
	boolean isInstrumentationTarget = false;

	public ProFitPackage(List<ProFitPackage> packages, List<ProFitClass> classes, String path) {
		super();
		this.packages = packages;
		this.classes = classes;
		this.path = path; 
		this.name = Paths.get(path).getFileName().toString();
	}
	
	// 	   Este m�todo recorre recursivamente el proyecto con el objetivo de marcar
	// como objetivo de la instrumentaci�n los m�todos cuyo nombre contenga alguna
	// de las cadenas incluidas en el ArrayList nameArray.
	public boolean selectMethodsByString(ArrayList<String> nameArray) {
		boolean outPut = false;
		System.out.println("\r\nProFitPackage; selecting methods by String  " + path);
		ListIterator<ProFitPackage> iteratorP = packages.listIterator(0);
		while(iteratorP.hasNext()) {
			ProFitPackage element = iteratorP.next();
			if( element.selectMethodsByString(nameArray) ) {
				outPut = true;
			}
		}
		ListIterator<ProFitClass> iteratorC = classes.listIterator();
		while(iteratorC.hasNext()) {
			ProFitClass element = iteratorC.next();
			if( element.selectMethodsByString(nameArray) ) {
				outPut = true;
			}
		}	
		System.out.println("\t" + outPut);
		return outPut;
	}
	
	// 	   Este m�todo recorre recursivamente el proyecto con el objetivo de marcar,
	// en los paquetes(respecto a las clases que contienen) y en las clases (respecto
	// a m�todos y atributos), el porcentaje de sus hijos que est�n seleccionados 
	// para instrumentar. 
	// 	   Para tal efecto los ProFitClass y ProFitPackage tienen, respectivamente,
	// los campos: instrumentableChildrenMethodPercentage, 
	// instrumentableChildrenFieldPercentage, instrumentableChildrenClassPercentage.
	public void setInstrumentablePercentages() {
		setInstrumentablePackagePercentage();
		setInstrumentableClassPercentage();
	}

	
	//De momento la optimizaci�n se lleva a cabo s�lo a nivel de clase
	private void setInstrumentablePackagePercentage() {
		int instrumetablePackageNumber = 0;
		ListIterator<ProFitPackage> iterator = packages.listIterator();
		while(iterator.hasNext()) {
			ProFitPackage element = iterator.next();
			
			
			//if( element.isInstrumentationTarget) {
				element.setInstrumentablePercentages();
				instrumetablePackageNumber++;
			//}
		}
		instrumentableChildrenPackagePercentage = instrumetablePackageNumber / (packages.size() / 100.f);
	}
	
	//De momento la optimizaci�n se lleva a cabo s�lo a nivel de clase
	private void setInstrumentableClassPercentage() {
		int instrumetableClassdNumber = 0;
		ListIterator<ProFitClass> iterator = classes.listIterator();
		while(iterator.hasNext()) {
			ProFitClass element = iterator.next();
			//if( element.isInstrumentationTarget) {
				element.setInstrumentablePercentages();
				instrumetableClassdNumber++;
			//}
		}
		instrumentableChildrenClassPercentage = instrumetableClassdNumber / (classes.size() / 100.f);
	}
	
	public String toString() {
		return name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ProFitPackage> getPackages() {
		return packages;
	}

	public void setPackages(List<ProFitPackage> dirs) {
		this.packages = dirs;
	}

	public List<ProFitClass> getClasses() {
		return classes;
	}

	public void setClasses(List<ProFitClass> classes) {
		this.classes = classes;
	}

	public String getPath() {
		return path.toString();
	}

	public void setPath(String path) {
		this.path = path;
	}
	
	public float getInstrumentableChildrenClassPercentage() {
		return instrumentableChildrenClassPercentage;
	}

	public void setInstrumentableChildrenClassPercentage(float instrumentableChildrenClassPercentage) {
		this.instrumentableChildrenClassPercentage = instrumentableChildrenClassPercentage;
	}
	
	public boolean isInstrumentationTarget() {
		return isInstrumentationTarget;
	}

	public void setInstrumentationTarget(boolean isInstrumentationTarget) {
		this.isInstrumentationTarget = isInstrumentationTarget;
	}
}
