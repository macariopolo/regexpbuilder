package edu.uclm.esi.regexpbuilder.instrumentation;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.json.JSONArray;
import org.json.JSONException;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.tree.ClassNode;

import edu.uclm.esi.regexpbuilder.model.Manager;

public class Project {
	private List<Clazz> classes;
	
	private Project() {
		this.classes = new ArrayList<>();
	}

	public static Project load(String projectName) throws IOException {
		Project project = new Project();
		String projectFolder = Manager.get().getWorkingFolder() + projectName + "/";
		IOFileFilter fileFilter = FileFilterUtils.suffixFileFilter("java", IOCase.INSENSITIVE);
		IOFileFilter dirFilter = TrueFileFilter.INSTANCE;
		Collection<File> classFiles = FileUtils.listFiles(new File(projectFolder), fileFilter, dirFilter);
		for (File classFile : classFiles) {
			try(InputStream is=new FileInputStream(classFile)) {
				ClassReader classReader = new ClassReader(is);
				ClassNode classNode=new ClassNode();
				classReader.accept(classNode, ClassReader.SKIP_FRAMES | ClassReader.SKIP_DEBUG);
				Clazz clazz = new Clazz(classNode);
				if (clazz.hasOperations())
					project.classes.add(clazz);
			}			
		}
		return project;
	}

	public JSONArray toJSONArray() throws JSONException {
		JSONArray jsa = new JSONArray();
		for (Clazz clazz : this.classes)
			jsa.put(clazz.toJSON());
		return jsa;
	}

}
