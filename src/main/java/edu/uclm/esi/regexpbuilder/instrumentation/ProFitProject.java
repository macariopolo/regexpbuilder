package edu.uclm.esi.regexpbuilder.instrumentation;

import java.io.Serializable;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.json.JSONArray;
import org.json.JSONException;

public class ProFitProject implements Serializable{
	private static final long serialVersionUID = -434964223115950307L;
	String name;
	List<ProFitPackage> packages;
	List<ProFitClass> classes;
	String path;  // path se almacena como String en la estructura de datos porque sun.nio.fs.WindosPath no es serializable
	ProFitProjectConfiguration configuration = new ProFitProjectConfiguration();

	public ProFitProject() {
		this.packages = new ArrayList<>();
		this.classes = new ArrayList<>();
	}
	
	public ProFitProject(List<ProFitPackage> packages, List<ProFitClass> classes, String path) {
		super();
		this.packages = packages;
		this.classes = classes;
		this.path = path; 
		this.name = Paths.get(path).getFileName().toString();
	}
	
	public void add(ProFitClass clazz) {
		this.classes.add(clazz);
	}

	// Este metodo recorre recursivamente el proyecto con el objetivo de marcar
	// como objetivo de la instrumentacion los metodos cuyo nombre contenga alguna
	// de las cadenas incluidas en el ArrayList nameArray.
	public void selectMethodsByString(ArrayList<String> nameArray) {
		System.out.println("\r\nProFitProject; selecting methods by String  " + path);
		ListIterator<ProFitPackage> iteratorP = packages.listIterator();
		while(iteratorP.hasNext()) {
			ProFitPackage element = iteratorP.next();
			element.selectMethodsByString(nameArray);
		}
		ListIterator<ProFitClass> iteratorC = classes.listIterator();
		while(iteratorC.hasNext()) {
			ProFitClass element = iteratorC.next();
			element.selectMethodsByString(nameArray);
		}
		System.out.println("\t" + true);
	}
	
	// 	   Este m�todo recorre recursivamente el proyecto con el objetivo de marcar,
	// en los paquetes(respecto a las clases que contienen) y en las clases (respecto
	// a m�todos y atributos), el porcentaje de sus hijos que est�n seleccionados 
	// para instrumentar. 
	// 	   Para tal efecto los ProFitClass y ProFitPackage tienen, respectivamente,
	// los campos: instrumentableChildrenMethodPercentage, 
	// instrumentableChildrenFieldPercentage, instrumentableChildrenClassPercentage.
	public void setInstrumentablePercentages() {
		ListIterator<ProFitPackage> iteratorP = packages.listIterator();
		while(iteratorP.hasNext()) {
			ProFitPackage element = iteratorP.next();
			element.setInstrumentablePercentages();
		}
		ListIterator<ProFitClass> iteratorC = classes.listIterator();
		while(iteratorC.hasNext()) {
			ProFitClass element = iteratorC.next();
			element.setInstrumentablePercentages();
		}
	}
	
	public String getName() {
		return name;
	}
	
	public String toString() {
		return name;
	}

	public ProFitProjectConfiguration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(ProFitProjectConfiguration configuration) {
		this.configuration = configuration;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ProFitPackage> getPackages() {
		return packages;
	}

	public void setPackages(List<ProFitPackage> packages) {
		this.packages = packages;
	}

	public List<ProFitClass> getClasses() {
		return classes;
	}

	public void setClasses(List<ProFitClass> classes) {
		this.classes = classes;
	}

	public String getPath() {
		return path.toString();
	}
	
	public void setPath(String path) {
		this.path = path;
	}

	public JSONArray toJSONArray() throws JSONException {
		JSONArray jsa = new JSONArray();
		for (ProFitClass clazz : this.classes)
			jsa.put(clazz.toJSON());
		return jsa;
	}
}