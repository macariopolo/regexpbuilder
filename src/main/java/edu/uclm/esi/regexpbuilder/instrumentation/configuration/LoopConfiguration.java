package edu.uclm.esi.regexpbuilder.instrumentation.configuration;

public class LoopConfiguration {
	boolean instrumentLoops, instrumentFor, instrumentIf, instrumentWhile, instrumentDo, instrumentSwitch;
	
	public LoopConfiguration(boolean instrumentFor, boolean instrumentIf, boolean instrumentWhile, boolean instrumentDo,
			boolean instrumentSwitch) {
		super();
		this.instrumentFor = instrumentFor;
		this.instrumentIf = instrumentIf;
		this.instrumentWhile = instrumentWhile;
		this.instrumentDo = instrumentDo;
		this.instrumentSwitch = instrumentSwitch;
	}

	public LoopConfiguration() {
		super();
	}

	public boolean isInstrumentFor() {
		return instrumentFor;
	}

	public void setInstrumentFor(boolean instrumentFor) {
		this.instrumentFor = instrumentFor;
	}

	public boolean isInstrumentIf() {
		return instrumentIf;
	}

	public void setInstrumentIf(boolean instrumentIf) {
		this.instrumentIf = instrumentIf;
	}

	public boolean isInstrumentWhile() {
		return instrumentWhile;
	}

	public void setInstrumentWhile(boolean instrumentWhile) {
		this.instrumentWhile = instrumentWhile;
	}

	public boolean isInstrumentDo() {
		return instrumentDo;
	}

	public void setInstrumentDo(boolean instrumentDo) {
		this.instrumentDo = instrumentDo;
	}

	public boolean isInstrumentSwitch() {
		return instrumentSwitch;
	}

	public void setInstrumentSwitch(boolean instrumentSwitch) {
		this.instrumentSwitch = instrumentSwitch;
	}

	public boolean isInstrumentLoops() {
		return instrumentLoops;
	}

	public void setInstrumentLoops(boolean instrumentLoops) {
		this.instrumentLoops = instrumentLoops;
	}
	
	
}
