package edu.uclm.esi.regexpbuilder.instrumentation.configuration;

import java.io.Serializable;

public class MethodConfiguration implements Serializable{
	boolean instrumentMethods = false;
	boolean addTimestamp = false;
	boolean insertPersonalCode = false;
	String personalCodeBefore = "";
	String personalCodeAfter = "";
	public MethodConfiguration() {
		super();
	}
	
	public boolean isInsertPersonalCode() {
		return insertPersonalCode;
	}

	public void setInsertPersonalCode(boolean insertPersonalCode) {
		this.insertPersonalCode = insertPersonalCode;
	}

	public boolean isInstrumentMethods() {
		return instrumentMethods;
	}
	public void setInstrumentMethods(boolean instrumentMethods) {
		this.instrumentMethods = instrumentMethods;
	}
	public boolean isAddTimestamp() {
		return addTimestamp;
	}
	public void setAddTimestamp(boolean addTimestamp) {
		this.addTimestamp = addTimestamp;
	}
	public String getPersonalCodeBefore() {
		return personalCodeBefore;
	}
	public void setPersonalCodeBefore(String personalCodeBefore) {
		this.personalCodeBefore = personalCodeBefore;
	}
	public String getPersonalCodeAfter() {
		return personalCodeAfter;
	}
	public void setPersonalCodeAfter(String personalCodeAfter) {
		this.personalCodeAfter = personalCodeAfter;
	}
	
}
