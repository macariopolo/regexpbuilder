package edu.uclm.esi.regexpbuilder.instrumentation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.ListIterator;

import org.json.JSONException;
import org.json.JSONObject;

public class ProFitMethod  implements Serializable{
	
	private static final long serialVersionUID = 5048691580113927230L;
	String name;
	ArrayList<Integer> lines;
	boolean inspect, checkTime, checkParameterValues, isInstrumentationTarget;
	
	public ProFitMethod(String name, /*List<ProFitAttribute> parameters,*/ ArrayList<Integer> lines, boolean inspect,
			boolean checkTime, boolean checkParameterValues) {
		super();
		this.name = name;
		this.lines = lines;
		this.inspect = inspect;
		this.checkTime = checkTime;
		this.checkParameterValues = checkParameterValues;
		isInstrumentationTarget = false;
	}

	public ProFitMethod(JSONObject jso) throws JSONException {
		this.name = jso.getString("name");
		this.inspect = true;
	}

	// 	   Este m�todo recorre recursivamente el proyecto con el objetivo de marcar
	// como objetivo de la instrumentaci�n los m�todos cuyo nombre contenga alguna
	// de las cadenas incluidas en el ArrayList nameArray.
	public boolean selectMethodsByString(ArrayList<String> nameArray) {
		System.out.println("\r\nProFitProject; selecting methods by String  " + name);
		ListIterator<String> iteratorN = nameArray.listIterator();
		while (iteratorN.hasNext()) {
			String subName = iteratorN.next();
			if(name.contains(subName)) {
				isInstrumentationTarget = true;
				return true;
			}
		}
		System.out.println("\t" + isInstrumentationTarget);
		return false;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Integer> getLines() {
		return lines;
	}

	public void setLines(ArrayList<Integer> lines) {
		this.lines = lines;
	}
	
	public void addLine(int line) {
		this.lines.add(line);
	}

	public boolean isInspect() {
		return inspect;
	}

	public void setInspect(boolean inspect) {
		this.inspect = inspect;
	}

	public boolean isCheckTime() {
		return checkTime;
	}

	public void setCheckTime(boolean checkTime) {
		this.checkTime = checkTime;
	}

	public boolean isCheckParameterValues() {
		return checkParameterValues;
	}

	public void setCheckParameterValues(boolean checkParameterValues) {
		this.checkParameterValues = checkParameterValues;
	}
	
	public boolean isInstrumentationTarget() {
		return isInstrumentationTarget;
	}

	public void setInstrumentationTarget(boolean isInstrumentationTarget) {
		this.isInstrumentationTarget = isInstrumentationTarget;
	}
	
	public String toString() {
		return name+"(..)";
	}

	public JSONObject toJSON() throws JSONException {
		JSONObject jso = new JSONObject();
		jso.put("name", name);
		return jso;
	}
}
