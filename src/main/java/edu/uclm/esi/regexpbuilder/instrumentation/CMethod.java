package edu.uclm.esi.regexpbuilder.instrumentation;

import org.json.JSONException;
import org.json.JSONObject;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.MethodNode;

public class CMethod {
	private String name;
	private String visibility;
	private boolean isStatic;
	private boolean isSynchronized;
	private boolean isAbstract;

	public CMethod(MethodNode mn) {
		this.name = mn.name;
		if ((mn.access & Opcodes.ACC_PUBLIC) == 1)
			this.visibility = "+";
		else if ((mn.access & Opcodes.ACC_PROTECTED) == 1)
			this.visibility = "#";
		else if ((mn.access & Opcodes.ACC_PRIVATE) == 1)
			this.visibility = "-";
		if ((mn.access & Opcodes.ACC_STATIC) ==1)
			this.isStatic = true;
		if ((mn.access & Opcodes.ACC_SYNCHRONIZED) ==1)
			this.isStatic = true;
		if ((mn.access & Opcodes.ACC_ABSTRACT) ==1)
			this.isAbstract = true;
		
	}

	public String getName() {
		return name;
	}

	public JSONObject toJSON() throws JSONException {
		return new JSONObject().put("name", name).put("visibility", this.visibility).put("static", this.isStatic).put("abstract", this.isAbstract).put("synchronized", this.isSynchronized);
	}
}
