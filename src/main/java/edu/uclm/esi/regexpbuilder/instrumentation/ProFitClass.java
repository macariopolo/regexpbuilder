package edu.uclm.esi.regexpbuilder.instrumentation;

import java.io.Serializable;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ProFitClass  implements Serializable{
	
	private static final long serialVersionUID = -247762953665784153L;
	String name;
	List<ProFitAttribute> attributes;
	List<ProFitMethod> methods;
	String path; // path se almacena como String en la estructura de datos porque sun.nio.fs.WindosPath no es serializable
	boolean isInstrumentationTarget;
	float instrumentableChildrenMethodPercentage = 0;
	float instrumentableChildrenFieldPercentage = 0;
	
	public ProFitClass(JSONArray methods, String path) throws JSONException {
		super();
		this.attributes = new ArrayList<>();
		this.methods = new ArrayList<>();

		for (int i=0; i<methods.length(); i++) {
			JSONObject jso = methods.getJSONObject(i);
			if (jso.getBoolean("checked"))
				this.methods.add(new ProFitMethod(jso));
		}
		
		this.path = path; 
		this.name = Paths.get(path).getFileName().toString();
		isInstrumentationTarget = false;
		setInstrumentablePercentages();
	}
	
	public ProFitClass(List<ProFitAttribute> attributes, List<ProFitMethod> methods, String path) {
		super();
		this.attributes = new ArrayList<>();
		this.methods = new ArrayList<>();

		for (ProFitAttribute attr : attributes)
			this.attributes.add(attr);
		
		for (ProFitMethod method : methods)
			this.methods.add(method);
		
		this.path = path; 
		this.name = Paths.get(path).getFileName().toString();
		isInstrumentationTarget = false;
	}
	
	// 	   Este m�todo recorre recursivamente el proyecto con el objetivo de marcar
	// como objetivo de la instrumentaci�n los m�todos cuyo nombre contenga alguna
	// de las cadenas incluidas en el ArrayList nameArray.
	public boolean selectMethodsByString(ArrayList<String> nameArray) {
		boolean outPut = false;
		System.out.println("\r\nProFitClass; selecting methods by String  " + path);
		ListIterator<ProFitMethod> iteratorM = methods.listIterator();
		while(iteratorM.hasNext()) {
			ProFitMethod element = iteratorM.next();
			if( element.selectMethodsByString(nameArray) ) {
				isInstrumentationTarget = true;
				outPut = true;
			}
		}
		System.out.println("\t" + outPut);
		return outPut;
	}

	// 	   Este m�todo recorre recursivamente el proyecto con el objetivo de marcar,
	// en los paquetes(respecto a las clases que contienen) y en las clases (respecto
	// a m�todos y atributos), el porcentaje de sus hijos que est�n seleccionados 
	// para instrumentar. 
	// 	   Para tal efecto los ProFitClass y ProFitPackage tienen, respectivamente,
	// los campos: instrumentableChildrenMethodPercentage, 
	// instrumentableChildrenFieldPercentage, instrumentableChildrenClassPercentage.
	public void setInstrumentablePercentages() {
		setInstrumentableMethodPercentage();
		setInstrumentableFieldPercentage();
	}

	// Calculamos qu� porcentaje de los m�todos locales est�n marcados como objeto de instrumentaci�n.
	private void setInstrumentableMethodPercentage() {
		instrumentableChildrenMethodPercentage = 0;
		if(methods.size()!=0) {
			instrumentableChildrenMethodPercentage = this.methods.size()/methods.size()*100;
		}
		
		/*ListIterator<ProFitMethod> iterator = methods.listIterator();
		while(iterator.hasNext()) {
			ProFitMethod element = iterator.next();
			if( element.isInstrumentationTarget) {
				instrumetableMethodNumber++;
			}
		}
		instrumentableChildrenMethodPercentage = instrumetableMethodNumber / (methods.size() / 100.f);
		*/
		
	}
	
	// Calculamos qu� porcentaje de los atributos est�n marcados como objeto de instrumentaci�n.
	private void setInstrumentableFieldPercentage() {
		int instrumetableFieldNumber = 0;
		ListIterator<ProFitAttribute> iterator = attributes.listIterator();
		while(iterator.hasNext()) {
			ProFitAttribute element = iterator.next();
			if( element.isInstrumentationTarget) {
				instrumetableFieldNumber++;
			}
		}
		instrumentableChildrenFieldPercentage = instrumetableFieldNumber / (attributes.size() / 100.f);
	}
	

	public String toString() {
		return name;
	}

	public boolean isInstrumentationTarget() {
		return isInstrumentationTarget;
	}

	public void setInstrumentationTarget(boolean isInstrumentationTarget) {
		this.isInstrumentationTarget = isInstrumentationTarget;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ProFitAttribute> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<ProFitAttribute> attributes) {
		this.attributes = attributes;
	}

	public List<ProFitMethod> getMethods() {
		return methods;
	}

	public void setMethods(List<ProFitMethod> methods) {
		this.methods = methods;
	}

	public String getPath() {
		return path.toString();
	}

	public void setPath(String path) {
		this.path = path;
	}
	
	public float getInstrumentableChildrenMethodPercentage() {
		return instrumentableChildrenMethodPercentage;
	}

	public void setInstrumentableChildrenMethodPercentage(int instrumentableChildrenMethodPercentage) {
		this.instrumentableChildrenMethodPercentage = instrumentableChildrenMethodPercentage;
	}

	public float getInstrumentableChildrenFieldPercentage() {
		return instrumentableChildrenFieldPercentage;
	}

	public void setInstrumentableChildrenFieldPercentage(int instrumentableChildrenFieldPercentage) {
		this.instrumentableChildrenFieldPercentage = instrumentableChildrenFieldPercentage;
	}

	public JSONObject toJSON() throws JSONException {
		JSONObject jso = new JSONObject();
		jso.put("name", path);
		jso.put("isInstrumentationTarget", isInstrumentationTarget);
		jso.put("instrumentableChildrenMethodPercentage", instrumentableChildrenMethodPercentage);
		jso.put("instrumentableChildrenFieldPercentage", instrumentableChildrenFieldPercentage);
		JSONArray jsaAttributes = new JSONArray();
		for (ProFitAttribute attribute : this.attributes)
			jsaAttributes.put(attribute.toJSON());
		jso.put("attributes", jsaAttributes);
		JSONArray jsaMethods = new JSONArray();
		for (ProFitMethod method : this.methods)
			jsaMethods.put(method.toJSON());
		jso.put("methods", jsaMethods);
		return jso;
	}
}


