package com.mkyong.zip;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.json.JSONArray;
 
/**
 * This utility extracts files and directories of a standard zip file to
 * a destination directory.
 * @author www.codejava.net
 *
 */
public class UnzipUtility {
    private static final int BUFFER_SIZE = 4096;
    
    public JSONArray unzip(String zipFilePath, String destDirectory) throws IOException {
    		JSONArray files=new JSONArray();
        File destDir = new File(destDirectory);
        if (!destDir.exists()) {
            destDir.mkdir();
        }
        try(FileInputStream zipFis=new FileInputStream(zipFilePath)) {
	        ZipInputStream zipIn = new ZipInputStream(zipFis);
	        ZipEntry entry = zipIn.getNextEntry();
	        if(entry==null) 
	        		throw new IOException("Not valid file.");
	        while (entry != null) {
	            String filePath = destDirectory + entry.getName();
	            if (!entry.isDirectory()) {
	                extractFile(zipIn, filePath, files);
	            } else {
	                File dir = new File(filePath);
	                dir.mkdirs();
	            }
	            zipIn.closeEntry();
	            entry = zipIn.getNextEntry();
	        }
        }
        return files;
    }

    private void extractFile(ZipInputStream zipIn, String filePath, JSONArray files) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read = 0;
        while ((read = zipIn.read(bytesIn)) != -1) {
            bos.write(bytesIn, 0, read);
        }
        bos.close();
        files.put(filePath);
    }
}