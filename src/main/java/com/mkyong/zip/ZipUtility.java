package com.mkyong.zip;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipUtility {
	
    /**
     * Zip it
     * @param zipFile output ZIP file location
     */
    private String OUTPUT_ZIP_FILE;
    private String SOURCE_FOLDER;
    private List<String> fileList;
    public ZipUtility(){
    	fileList=new ArrayList<String>();
    }
    public String zip(String path,String name){
    	OUTPUT_ZIP_FILE=name+".zip";
    	SOURCE_FOLDER=path;
    	generateFileList(new File(path));
    	zipIt(OUTPUT_ZIP_FILE,fileList);
    	return OUTPUT_ZIP_FILE;
    }
    
    private void zipIt(String zipFile,List<String> fileList){

     byte[] buffer = new byte[1024];

     try{

    	FileOutputStream fos = new FileOutputStream(zipFile);
    	ZipOutputStream zos = new ZipOutputStream(fos);

    	for(String file : fileList){
    		ZipEntry ze= new ZipEntry(file);
        	zos.putNextEntry(ze);

        	FileInputStream in = new FileInputStream(SOURCE_FOLDER + File.separator + file);

        	int len;
        	while ((len = in.read(buffer)) > 0) {
        		zos.write(buffer, 0, len);
        	}

        	in.close();
    	}

    	zos.closeEntry();
    	zos.close();

    }catch(IOException ex){
       ex.printStackTrace();
    }
   }

    /**
     * Traverse a directory and get all files,
     * and add the file into fileList
     * @param node file or directory
     * @return 
     */
    private void generateFileList(File node){
    	//add file only
		if(node.isFile()){
			fileList.add(generateZipEntry(node.getAbsoluteFile().toString()));
		}
	
		if(node.isDirectory()){
			String[] subNote = node.list();
			for(String filename : subNote){
				generateFileList(new File(node, filename));
			}
		}

    }

    /**
     * Format the file path for zip
     * @param file file path
     * @return Formatted file path
     */
    private String generateZipEntry(String file){
    	return file.substring(SOURCE_FOLDER.length()+1, file.length());
    }
}
